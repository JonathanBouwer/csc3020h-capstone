﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class ErrorText : MonoBehaviour {

        private TextMeshProUGUI errorText;
        private bool fading;

        public void Start() {
            errorText = GetComponent<TextMeshProUGUI>();
            fading = false;
        }

        public void FadeInOut(string newText) {
            if (fading) return;
            errorText.text = newText;
            StartCoroutine(Fade());
        }

        private IEnumerator Fade() {
            fading = true;
            for (var i = 0; i < 30; i++) {
                var value = Mathf.Lerp(0, 1, i / 30.0f);
                errorText.color = new Color(1, 0, 0, value);
                yield return null;
            }
            yield return new WaitForSeconds(1);
            for (var i = 0; i < 30; i++) {
                var value = Mathf.Lerp(1, 0, i / 30.0f);
                errorText.color = new Color(1, 0, 0, value);
                yield return null;
            }
            fading = false;
        }
    }
}

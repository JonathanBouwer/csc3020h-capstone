﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts {
    public class Room : MonoBehaviour {
        public IntVector2 Size;
        public IntVector2 Coordinates;
        public Corridor CorridorPrefab;
        public Tile TilePrefab;
		public float moveDecor;
        public GameObject WallPrefab;
		public GameObject[] DecorPillars;
		public GameObject LightCorridorBlock, MovingLight;
		public int lightCountPerRoom;
		public Color[] LightColours;

        [HideInInspector] public int PuzzleId;
        [HideInInspector] public int RoomId;
        [HideInInspector] public Dictionary<Room, Corridor> RoomCorridor = new Dictionary<Room, Corridor>();

        private GameObject tilesObject;
        private GameObject wallsObject;
		private GameObject decorObject;
        private Tile[,] tiles;
        private Map map;

        /// <summary>
        /// Initialization funtion for a room
        /// </summary>
        /// <param name="map">Map instance to use for room</param>
		public void Init(Map map) {
            this.map = map;
        }

        /// <summary>
        /// Method to generate the floor of a room
        /// </summary> 
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Generate() {
            // Create parent object
            tilesObject = new GameObject("Tiles");
            tilesObject.transform.parent = transform;
            tilesObject.transform.localPosition = Vector3.zero;

            tiles = new Tile[Size.x, Size.z];
            for (int x = 0; x < Size.x; x++) {
                for (int z = 0; z < Size.z; z++) {
                    tiles[x, z] = CreateTile(new IntVector2(Coordinates.x + x, Coordinates.z + z));
                }
            }
			CreateMovingLights ();
            yield return null;
        }

        /// <summary> 
        /// Internal methof to generate a tile game object
        /// </summary>
        /// <param name="coordinates">Coordinates of tile to be build</param>
        /// <returns>The tile game object generated</returns>
        private Tile CreateTile(IntVector2 coordinates) {
            if (map.GetTileType(coordinates) == TileType.Empty) {
                map.SetTileType(coordinates, TileType.Room);
            } else {
                Debug.LogError("Tile Conflict at: " + coordinates);
            }
            Tile newTile = Instantiate(TilePrefab);
            newTile.Coordinates = coordinates;
            newTile.name = "Tile " + coordinates.x + ", " + coordinates.z;
            newTile.transform.parent = tilesObject.transform;
            var tilePosition = new Vector3 {
                x = coordinates.x - Coordinates.x - Size.x * 0.5f + 0.5f,
                y = 0f,
                z = coordinates.z - Coordinates.z - Size.z * 0.5f + 0.5f
            };

            newTile.transform.localPosition = RoomMapManager.TileSize * tilePosition;
            return newTile;
        }

        /// <summary>
        /// Method to generate a corridor between rooms
        /// </summary>
        /// <param name="otherRoom">Other room to generate a corridor to</param>
        /// <returns>The corridor generated</returns>
        public Corridor CreateCorridor(Room otherRoom) {
            // Don't create if already connected
            if (RoomCorridor.ContainsKey(otherRoom)) {
                return RoomCorridor[otherRoom];
            }

            Corridor newCorridor = Instantiate(CorridorPrefab);
            newCorridor.name = "Corridor (" + otherRoom.RoomId + ", " + RoomId + ")";
            newCorridor.transform.parent = transform.parent;
            newCorridor.Coordinates = new IntVector2 {
                x = Coordinates.x + Size.x / 2,
                z = otherRoom.Coordinates.z + otherRoom.Size.z / 2
            };
            newCorridor.transform.localPosition = new Vector3 {
                x = newCorridor.Coordinates.x - map.MapSize.x / 2,
                y = 0,
                z = newCorridor.Coordinates.z - map.MapSize.z / 2
            };
            newCorridor.Rooms[0] = otherRoom;
            newCorridor.Rooms[1] = this;
            newCorridor.Length = Vector3.Distance(otherRoom.transform.localPosition, transform.localPosition);
            newCorridor.Init(map);
            otherRoom.RoomCorridor.Add(this, newCorridor);
            RoomCorridor.Add(otherRoom, newCorridor);

            return newCorridor;
        }

        /// <summary> 
        /// Generator function used to generate walls for the corridor
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator CreateWalls() {
            wallsObject = new GameObject("Walls");
            wallsObject.transform.parent = transform;
            wallsObject.transform.localPosition = Vector3.zero;
			decorObject = new GameObject("Decor");
			decorObject.transform.parent = transform;
			decorObject.transform.localPosition = Vector3.zero;
			bool host = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>().IsHost;

            IntVector2 leftBottom = new IntVector2(Coordinates.x - 1, Coordinates.z - 1);
            IntVector2 rightTop = new IntVector2(Coordinates.x + Size.x, Coordinates.z + Size.z);
            for (int x = leftBottom.x; x <= rightTop.x; x++) {
                for (int z = leftBottom.z; z <= rightTop.z; z++) {
                    var isLeft = x == leftBottom.x;
                    var isRight = x == rightTop.x;
                    var isBottom = z == leftBottom.z;
                    var isTop = z == rightTop.z;
                    var isCenter = !isLeft && !isRight && !isBottom && !isTop;
                    var isCorner = (isLeft || isRight) && (isBottom || isTop);
                    var isWall = map.GetTileType(new IntVector2(x, z)) == TileType.Wall;
                    // If it's center or corner or not wall
                    if (isCenter) continue;
			
					if (isCorner&&isWall) {//put up decor
						var decorPosition = new Vector3 {
							x = x - Coordinates.x-Size.x * 0.5f+ 0.5f,
							y = 0f,
							z = z - Coordinates.z- Size.z * 0.5f + 0.5f
						};
						int decorChosen;
						if (host) {
							decorChosen = Random.Range (0, 2);
						} else {
							decorChosen = 2;
						}
						Vector3 Dposition = decorPosition* (moveDecor*RoomMapManager.TileSize); 
						Dposition.y = -0.3f;
						GameObject Decor = Instantiate(DecorPillars[decorChosen]);
						Decor.name = "Pillar " + decorChosen;//USING THE INT FOR SERIALIZATION PURPOSES
						Decor.transform.parent = decorObject.transform;
						Decor.transform.localPosition = Dposition;
						continue;
					}


                    Quaternion rotation;
                    if (isLeft) {
                        rotation = MapDirection.West.ToRotation();
                    } else if (isRight) {
                        rotation = MapDirection.East.ToRotation();
                    } else if (isBottom) {
                        rotation = MapDirection.South.ToRotation();
                    } else /*if (isTop) // Always true */
                      {
                        rotation = MapDirection.North.ToRotation();
                    }
                    var tilePosition = new Vector3 {
                        x = x - Coordinates.x - Size.x * 0.5f + 0.5f,
                        y = 0f,
                        z = z - Coordinates.z - Size.z * 0.5f + 0.5f
                    };
                    Vector3 position = RoomMapManager.TileSize * tilePosition;
					if (!isWall) {
						position.y = 15;
                        GameObject stop = Instantiate(LightCorridorBlock, position,rotation);
						stop.name = "LightEntrance (" + x + ", " + z + ")";
                        stop.transform.parent = wallsObject.transform;
                        stop.transform.localPosition = position;
                        stop.transform.localRotation = rotation;
                        continue;

					}

                    GameObject newWall = Instantiate(WallPrefab, position, rotation);
                    newWall.name = "Wall (" + x + ", " + z + ")";
                    newWall.transform.parent = wallsObject.transform;
                    newWall.transform.localPosition = position;
                    newWall.transform.localRotation = rotation;

                }
            }
            yield return null;
        }
		public void CreateMovingLights() {

			GameObject lightObject = new GameObject("Moving Lights");
			lightObject.transform.parent = transform;
			lightObject.transform.localPosition = Vector3.zero;
			Vector3 positionLight = new Vector3 (0, 25, 0);
			for (int i = 0; i < lightCountPerRoom; i++) {
				GameObject mlight = Instantiate (MovingLight);
				mlight.transform.parent = lightObject.transform;
				mlight.transform.localPosition = positionLight;
				mlight.GetComponentInChildren<Light> ().color = LightColours [Random.Range (0, LightColours.Length)];
			}

		} 

    }
}
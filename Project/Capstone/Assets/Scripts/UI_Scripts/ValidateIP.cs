﻿using System.Text.RegularExpressions;
using Assets.Scripts.Menu_Scripts;
using UnityEngine;
namespace Assets.Scripts.UI_Scripts {
    public class ValidateIP : MonoBehaviour {
        [HideInInspector] public bool ContainsValidIp;
        public ConnectButton Button;

        private const string DigitGroup = @"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        private readonly Regex r = new Regex("^" + DigitGroup + @"\." + DigitGroup + @"\." 
                                             + DigitGroup + @"\." + DigitGroup + "$");
        /// <summary>
        /// Method to validate IP using regexes
        /// </summary>
        /// <param name="input">IP to validate</param>
        public void IsValidIp(string input) {
            ContainsValidIp = input.Equals("localhost") || r.IsMatch(input);
            Button.Enable();
        }
    }
}

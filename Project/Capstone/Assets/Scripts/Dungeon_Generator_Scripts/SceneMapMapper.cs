﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Dungeon_Generator_Scripts {
    /// <summary>
    /// Serializable class used to map between representations of the map
    /// </summary>
    [Serializable]
    public class SerializedMapData : SerializedSceneItem {
        public enum Type {
            Invalid,
            None,
            Tile,
            RoomTile,
            FinalRoomTile,
            Wall,
            FinalRoomWall,
            RoomWall,
            Room,
            FinalRoom,
            Corridor,
			LightBlock,
            Map,
			Pillar
        }

        public string Name;
        public Type BlockType;
        public Vector3 Position, Rotation, Scale;
		public string Extra;
    }

    public class SceneMapMapper : SceneItemMapper<SerializedMapData> {
        public Map MapPrefab;

        /// <summary>
        /// Constructor for SceneMapMapper
        /// </summary>
        /// <param name="mapPrefab">MapPrefab to use</param>
        public SceneMapMapper(Map mapPrefab) {
            MapPrefab = mapPrefab;
        }

        /// <summary>
        /// Customized mapping function specific to <code>SerializedMapData</code>
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <returns>Sererialized representation of item</returns>
        public override SerializedMapData MapItem(Transform item) {
            var result = new SerializedMapData {
                Name = item.name,
                BlockType = SerializedMapData.Type.Invalid,
                Position = item.position,
                Rotation = item.eulerAngles,
                Scale = item.localScale
            };

            var prefix = item.name.Split(' ')[0];
			if (prefix == "Map") {
				result.BlockType = SerializedMapData.Type.Map;
			} else if (prefix == "Room") {
				result.BlockType = SerializedMapData.Type.Room;
				result.Extra = item.GetComponent<ClientTriggerController>().Message + " " +item.GetComponent<Room>().PuzzleId;
			} else if (prefix == "Pillar") {
				result.BlockType = SerializedMapData.Type.Pillar;
			} else if (prefix == "FinalRoom") {
				result.BlockType = SerializedMapData.Type.FinalRoom;
			} else if (prefix == "Corridor") {
				result.BlockType = SerializedMapData.Type.Corridor;
			} else if (prefix == "Tile") {
				var parentName = item.transform.parent.parent.name.Split (' ') [0];
				if (parentName == "Room") {
					result.BlockType = SerializedMapData.Type.RoomTile;
				} else if (parentName == "FinalRoom") {
					result.BlockType = SerializedMapData.Type.FinalRoomTile;
				} else {
					result.BlockType = SerializedMapData.Type.Tile;
				}
			} else if (prefix == "Wall") {
				var parentName = item.transform.parent.parent.name.Split (' ') [0];
				if (parentName == "Room") {
					result.BlockType = SerializedMapData.Type.RoomWall;
				} else if (parentName == "FinalRoom") {
					result.BlockType = SerializedMapData.Type.FinalRoomWall;
				} else {
					result.BlockType = SerializedMapData.Type.Wall;
				}
			} else if (prefix == "LightEntrance") {
				result.BlockType = SerializedMapData.Type.LightBlock;
			}else if (prefix == "Walls" || prefix == "Tiles"
			                    || prefix == "Player1Spawn" || prefix == "Player2Spawn") {
				result.BlockType = SerializedMapData.Type.None;
			} else if (prefix == "LockedRoom"||prefix =="Fog"||prefix =="Spotlight"||prefix =="Moving"||prefix =="WallCube"||prefix =="Quad"||item.name =="Minimap Quad") {
				return null;
			}
            return result;
        }

        /// <summary>
        /// Customized unmapping function specific to <code>SerializedMapData</code>
        /// </summary>
        /// <param name="item">Serialized item to unmap</param>
        /// <returns>Game object generated based on item</returns>
        public override GameObject UnmapItem(SerializedMapData item) {
            GameObject result;
            switch (item.BlockType) {
                case SerializedMapData.Type.Tile:
                    result = Object.Instantiate(MapPrefab.RoomPrefab.CorridorPrefab.TilePrefab).gameObject;
                    break;
                case SerializedMapData.Type.RoomTile:
                    result = Object.Instantiate(MapPrefab.RoomPrefab.TilePrefab).gameObject;
                    break;
                case SerializedMapData.Type.FinalRoomTile:
                    result = Object.Instantiate(MapPrefab.FinalRoomPrefab.TilePrefab).gameObject;
                    break;
                case SerializedMapData.Type.Wall:
                    result = Object.Instantiate(MapPrefab.RoomPrefab.CorridorPrefab.WallPrefab).gameObject;
                    break;
                case SerializedMapData.Type.RoomWall:
                    result = Object.Instantiate(MapPrefab.RoomPrefab.WallPrefab).gameObject;
                    break;
				case SerializedMapData.Type.Room:
					result = Object.Instantiate (MapPrefab.RoomPrefab).gameObject;
                    var extras = item.Extra.Split(' ');
					result.GetComponent<ClientTriggerController>().Message = extras[0];
                    var room = result.GetComponent<Room>();
                    room.PuzzleId = int.Parse(extras[1]);
                    room.CreateMovingLights ();
                    break;
                case SerializedMapData.Type.FinalRoomWall:
                    result = Object.Instantiate(MapPrefab.FinalRoomPrefab.WallPrefab).gameObject;
                    break;
                case SerializedMapData.Type.FinalRoom:
                    result = Object.Instantiate(MapPrefab.FinalRoomPrefab).gameObject;
					result.GetComponent<Room>().CreateMovingLights ();
                    break;
				case SerializedMapData.Type.Corridor:
					result = Object.Instantiate (MapPrefab.RoomPrefab.CorridorPrefab).gameObject;
					result.GetComponent<Corridor>().CreateMovingLights ();
                    break;
                case SerializedMapData.Type.Map:
                    result = Object.Instantiate(MapPrefab).gameObject;
                    break;
				case SerializedMapData.Type.LightBlock:
					result = Object.Instantiate(MapPrefab.RoomPrefab.LightCorridorBlock).gameObject;
					break;
				case SerializedMapData.Type.Pillar:
					var DecorNumber = int.Parse(item.Name.Split(' ')[1]);
					result = Object.Instantiate(MapPrefab.RoomPrefab.DecorPillars[DecorNumber]).gameObject;
					break;
				default:
					result = new GameObject ();
                    break;
            }

            result.name = item.Name;
            result.transform.position = item.Position;
            result.transform.eulerAngles = item.Rotation;
            result.transform.localScale = item.Scale;
            return result;
        }
    }
}

﻿namespace Assets.Scripts {
    [System.Serializable]
    public enum NetworkMessageType {
        // Spawn Messages
        READY_FOR_SPAWN = 50,
        SPAWN_DONE = 51,
        CHANGE_SCENE_REQUEST = 52,
        SCENE_TRANSITION_START = 53,
        SCENE_TRANSITION_END = 54,
        SCENE_TRANSITION_CLEAR =55,
        // Networked Seeding Messages
        TRANSFER_NETWORK_SEED = 61,
        // Quit Messages
        QUIT = 70,
        RETURN_TO_MENU = 71,
        // Chat Messages
        SEND_CHAT_MESSAGE = 80,
        RECEIVE_CHAT_MESSAGE = 81,
        // Map Messages
        DO_KEY_TEST = 90,
        KEY_TEST_SUCESS = 91,
		//Tutorial
		TUTORIAL_WON=98,
		// Puzzle 0 messages
        PUZZLE_0_PLATE_TRIGGER = 100,
        PUZZLE_0_DESTROY_PLATE_PAIR = 101,
        PUZZLE_0_SOLVED = 102,
        // Puzzle 2 messages
        PUZZLE_2_TRIGGER = 200,
        PUZZLE_2_PATTERN = 201,
        PUZZLE_2_MISTAKE = 202,
        PUZZLE_2_SOLVED = 203,
        PUZZLE_2_SAVED_SOLVED = 204,
        PUZZLE_2_RESET = 205,
        // Puzzle 3 messages 
        PUZZLE_3_BLOCK_OPEN = 300,
		PUZZLE_3_MINE_MARK = 301,
		PUZZLE_3_SOLVED = 302,
		PUZZLE_3_LOST=303,
	    // Puzzle 4 Messages
	    PUZZLE_4_PREY_MOVE_REQUEST = 400,
	    PUZZLE_4_PREY_MOVE_CLIENT = 401,
	    PUZZLE_4_PREDATOR_MOVE_REQUEST = 402,
	    PUZZLE_4_PREDATOR_MOVE_CLIENT = 403,
	    PUZZLE_4_NEXT_TURN = 404,
	    PUZZLE_4_SOLVED = 405
    }
}

﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    /// <summary>
    /// Class to help serialize puzzle 0
    /// </summary>
    [Serializable]
    public class SerializedPuzzle0Data : SerializedSceneItem {
        public string Name;
        public Vector3 Position, Scale;
        public string[] Extra;
    }

    class ScenePuzzle0Mapper : SceneItemMapper<SerializedPuzzle0Data> {
        public PuzzleBuilder Builder;

        /// <summary>
        /// Constructor for serializer
        /// </summary>
        /// <param name="builder">Puzzle builder object containing references to the prefabs used to build</param>
        public ScenePuzzle0Mapper(PuzzleBuilder builder) {
            Builder = builder;
        }

        /// <summary>
        /// Customized map method for SerializedPuzzle0Data
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <returns>SerializedPuzzle0Data version of item</returns>
        public override SerializedPuzzle0Data MapItem(Transform item) {
            var result = new SerializedPuzzle0Data {
                Position = item.position,
                Scale = item.localScale,
                Name = item.name
            };

            switch (item.name) {
                case "MazeCameraController":
                    var boxCollider = item.GetComponent<BoxCollider>();
                    var center = boxCollider.center;
                    var size = boxCollider.size;
                    result.Extra = new string[2];
                    result.Extra[0] = center.x + "," + center.y + "," + center.z;
                    result.Extra[1] = size.x + "," + size.y + "," + size.z;
                    break;
                case "MazePlate":
                    var color = item.GetComponent<MeshRenderer>().material.color;
                    var id = item.GetComponent<ClientTriggerController>().Prefix;
                    result.Extra = new string[3];
                    result.Extra[0] = color.r + "," + color.g + "," + color.b;
                    result.Extra[1] = id;
                    result.Extra[2] = item.gameObject.layer + "";
                    break;
                case "PlatePairs":
                    var count = item.GetComponent<PlatePairController>().PairCount;
                    result.Extra = new string[1];
                    result.Extra[0] = count.ToString();
                    break;
                case "FakeWall":
                    result.Extra = new string[1];
                    result.Extra[0] = item.gameObject.layer.ToString();
                    break;
            }
            return result;
        }

        /// <summary>
        /// Method to unmap SerializedPuzzle0Data item
        /// </summary>
        /// <param name="item">Item to unmap</param>
        /// <returns>Newly created game object</returns>
        public override GameObject UnmapItem(SerializedPuzzle0Data item) {
            GameObject result;
            switch (item.Name) {
                case "MazeWall":
                    result = Object.Instantiate(Builder.MazeWall);
                    break;
                case "MazeFloor":
                    result = Object.Instantiate(Builder.MazeFloor);
                    break;
                case "MazePlate":
                    result = Object.Instantiate(Builder.MazePlate);
                    var cols = ParseCsv<float>(item.Extra[0]);
                    var meshRenderer = result.GetComponent<MeshRenderer>();
                    meshRenderer.material.color = new Color(cols[0], cols[1], cols[2]);
                    var trigger = result.GetComponent<ClientTriggerController>();
                    trigger.Prefix = item.Extra[1];
                    trigger.Message = " T";
                    result.layer = int.Parse(item.Extra[2]);
                    break;
                case "FakeWall":
                    result = Object.Instantiate(Builder.MazeWall);
                    Object.Destroy(result.GetComponent<Collider>());
                    result.layer = int.Parse(item.Extra[0]);
                    break;
                case "PlatePairs":
                    result = new GameObject();
                    var pairControl = result.AddComponent<PlatePairController>();
                    pairControl.PairCount = int.Parse(item.Extra[0]);
                    break;
                case "LeavePlate":
                    result = Object.Instantiate(Builder.LeavePlate);
                    break;
                case "MazeCameraController":
                    result = new GameObject {
                        layer = LayerMask.NameToLayer("Ignore Raycast")
                    };
                    result.AddComponent<CameraPerspectiveController>();
                    var boxCollider = result.AddComponent<BoxCollider>();
                    var vals = ParseCsv<float>(item.Extra[0]);
                    boxCollider.center = new Vector3(vals[0], vals[1], vals[2]);
                    vals = ParseCsv<float>(item.Extra[1]);
                    boxCollider.size = new Vector3(vals[0], vals[1], vals[2]);
                    boxCollider.isTrigger = true;
                    break;
                case "Decoration":
                    result = Object.Instantiate(Builder.RoomBuilder.DecorationPrefab);
                    break;
                default:
                    result = new GameObject();
                    break;
            }

            result.name = item.Name;
            result.transform.position = item.Position;
            result.transform.localScale = item.Scale;
            return result;
        }
    }
}

﻿using System;
using Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4;
using UnityEngine;

namespace Assets.Scripts.Player_Scripts{
    public class Raycast : MonoBehaviour{
        
        private void Update(){
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Puzzle4Scene"){
                var forward = transform.TransformDirection(Vector3.forward);
                //Debug.DrawRay(transform.position, forward, Color.green);
                if (Input.GetKeyDown("e")){
                    RaycastHit hit;
                    if (!Physics.Raycast(transform.position, forward, out hit)) return;
                    if (!hit.collider.CompareTag("PredatorPiece")) return;
                    if (!(hit.distance <= 5)) return;
                    var predator = hit.collider.gameObject.GetComponentInParent<PredatorController>();

                    if (!((transform.gameObject.layer == LayerMask.NameToLayer("Player2Ignore") 
                           & (predator.ID == 0 | predator.ID == 1)) |
                          (transform.gameObject.layer == LayerMask.NameToLayer("Player1Ignore")
                           & (predator.ID == 2 | predator.ID == 3)))) return;
                    //Calculate displacement here
                    var xDiff = transform.position.x - hit.transform.position.x;
                    var zDiff = transform.position.z - hit.transform.position.z;

                    int x = 0, y = 0;
                    if (Math.Abs(xDiff) > Math.Abs(zDiff)){
                        if (xDiff < 0) x = 1;
                        else x = -1;
                    }
                    else{
                        if (zDiff < 0) y = 1;
                        else y = -1;
                    }
                    //Debug.Log(x + " " + y);
                    predator.RequestMove(x,y);
                }
            }
        }
    }
}
﻿
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.UI_Scripts {
    public class PauseScreen : MonoBehaviour {

        private bool open, quit;
        void Start () {
            open = false;
            quit = false;
            const short type = (short) NetworkMessageType.RETURN_TO_MENU;
            NetworkManager.singleton.client.RegisterHandler(type, ReturnToMenu);
        }
	
        void Update () {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                open = !open;
                transform.localScale = new Vector3(1, 1 - transform.localScale.y, 1);
            }
        }

        public void ClosePauseScreen() {
            open = false;
            transform.localScale = new Vector3(1, 0, 1);
        }

        public void QuitDesktop() {
            quit = true;
            const short type = (short) NetworkMessageType.QUIT;
            NetworkManager.singleton.client.Send(type, new EmptyMessage());
            ReturnToMenu(null);
        }

        private IEnumerator WaitThenQuit() {
            yield return new WaitForSeconds(2);
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void ReturnToMenu(NetworkMessage message) {
            if (quit) {
                StartCoroutine(WaitThenQuit());
            } else {
                Cleanup.CleanupPersistObjectsGoToMenu();
            }
        }
    }
}

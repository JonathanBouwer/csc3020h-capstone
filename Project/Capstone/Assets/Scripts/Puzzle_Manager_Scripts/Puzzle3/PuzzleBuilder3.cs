﻿using System.Collections;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
    public class PuzzleBuilder3 : MonoBehaviour {
        public int  NumberOfMinesMin, NumberOfMinesMax, StartAreaLength;
		//width can only be 6, 10, 14, 18 in order to ensure the aracade placem
		public int[] widthvalues;
		public int[] breadthvalues;
        public MineBuilder MineBuilder;
        public RoomBuilder3 RoomBuilder;
        public Puzzle3Manager PuzzleManager;
        public GameObject MazeWall, MazeFloor, Block, LeavePlate, RestartPlate;
        public Material[] MatBlocks;
		public Material loose;
		public Material[] Win;
		private int Width, Breadth, NumberOfMines;
		public GameObject[] decor;

        [HideInInspector] public SavedLevelManager LevelManager;
        [HideInInspector] public SceneManager Manager;
        [HideInInspector] public SceneSerializer<SerializedPuzzle3Data> Serializer;

        private const string FileName = "Puzzle3Data.json";
        private bool host;
        
        /// <summary>
        /// Build Map only if previous data not saved
        /// </summary>
		public void BuildMap(int seed) {
            host = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>().IsHost;
            var fileData = LevelManager.LoadLevel(FileName);
            if (string.IsNullOrEmpty(fileData)) {
                StartCoroutine(Build(seed));
            } else {
                StartCoroutine(Build(fileData));
            }
        }

        /// <summary>
        /// Builds World, with seperate instructions for host and client
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
		private IEnumerator Build(int seed) {
			SetValues (seed);
            MineBuilder.Width = Width;
            MineBuilder.Breadth = Breadth;
            MineBuilder.Block = Block;
            if (host) {
                MineBuilder.NumMines = NumberOfMines;
                yield return StartCoroutine(MineBuilder.BuildHost());
                PuzzleManager.MineIds = MineBuilder.MinesId;
                PuzzleManager.Width = Width;
                PuzzleManager.Breadth = Breadth;
                PuzzleManager.HostGetListBricks();
            } else {
                yield return StartCoroutine(MineBuilder.BuildClient());
            }
            var mine = MineBuilder.MineObject;
			RoomBuilder.decor = decor;
            RoomBuilder.StartAreaLength = StartAreaLength;
			RoomBuilder.host = host;
            RoomBuilder.Width = Width;
            RoomBuilder.Breadth = Breadth;
            RoomBuilder.Mine = mine;
            RoomBuilder.Floor = MazeFloor;
            RoomBuilder.Wall = MazeWall;
            RoomBuilder.LeavingPlate = LeavePlate;
            RoomBuilder.RestartPlate = RestartPlate;
            yield return StartCoroutine(RoomBuilder.BuildRoom());

            // In this puzzle it only makes sense to save data if won, as we want the puzzle to regenerate everytime when restarts
            yield return StartCoroutine(Manager.WaitForPersistObjectsToLoad());
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }

        /// <summary>
        /// Helper method to save environment to file
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator SaveEnvironment() {
            var environment = GameObject.Find("Environment");
            yield return StartCoroutine(Serializer.SerializeSceneItem(environment.transform));
            LevelManager.SaveLevel(FileName, Serializer.SerializedResult);
        }

        /// <summary>
        /// Generate map fromn file  
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Build(string levelData) {
            yield return StartCoroutine(Serializer.DeserializeSceneItem(levelData));
            PuzzleManager.GameWon();
            yield return StartCoroutine(Manager.WaitForPersistObjectsToLoad());
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }
		/// <summary>
		/// Set Variables: width, breadth, number of Mines    
		/// </summary>
		public void SetValues(int seed) {
			Random.InitState(seed);
			Width = widthvalues [Random.Range (0, widthvalues.Length)];
			Breadth = breadthvalues [Random.Range (0, breadthvalues.Length)];
			NumberOfMines = Random.Range (NumberOfMinesMin, NumberOfMinesMax);
		}
		/// Changes layer of clue to it ignores the other player
		/// </summary>
		/// <param name="tagToIgnore">Either Player1Ignore or PLayer2Ignore</param>
		public void SetClueIgnore(string tagToIgnore) {
			var clue = GameObject.Find ("Clue");
			clue.transform.position = new Vector3 (Width*2+2.5f, 5, -StartAreaLength*10);//2,5 is for half block width
			var clueController =clue.GetComponentInChildren<ClueController>();
			clueController.IgnoreLayer = tagToIgnore;
		}
    }
}

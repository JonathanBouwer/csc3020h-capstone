﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.UI_Scripts{
    public class PlayerChat : MonoBehaviour{
	
	    private List<string> _messageList;

	    public TMP_InputField InputField;
	    public GameNetworkManager Manager;
	    public TMP_Text TextArea;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        private void Awake(){
		    _messageList = new List<string>();
		    const short typeRecieve = (short)NetworkMessageType.RECEIVE_CHAT_MESSAGE;
		    Manager = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>();
		    Manager.client.RegisterHandler(typeRecieve, RecieveMessage);
	    }

	    /// <summary>
        /// Method to send chat messages to the server to be forwarded.
        /// </summary>
	    public void SendMessage(){
		    var toSend = InputField.textComponent.text.Trim();

		    if (string.IsNullOrEmpty(toSend)) return;

		    if (Manager.IsHost) toSend = "N0v4 : " + toSend;
		    else toSend = "Bl00m: " + toSend;
		
		    var message = new StringMessage(toSend);
		    const short typeSend = (short)NetworkMessageType.SEND_CHAT_MESSAGE;
		    Manager.client.Send(typeSend, message);

		    InputField.text = "";
	    }

        /// <summary>
        /// Handler for chat messages. updates message list and UI.
        /// </summary>
        public void RecieveMessage(NetworkMessage message){
		    var data = message.ReadMessage<StringMessage>().value;
		
		    _messageList.Add(data);

		    UpdateText();
	    }
	
	    /// <summary>
        /// Helper method to update text
        /// </summary>
	    private void UpdateText(){
		    TextArea.text = "";
		    for (int i = 0; i < _messageList.Count; i++)
		    {
			    TextArea.text += _messageList[i];
			    if (i != _messageList.Count - 1) TextArea.text += "\n";
		    }
	    }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method handles when the user presses Enter and will send a message in this case
        /// </remarks>
        private void Update() {
		    if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) {
			    SendMessage();
			    InputField.DeactivateInputField();
		    }
	    }
    }
}
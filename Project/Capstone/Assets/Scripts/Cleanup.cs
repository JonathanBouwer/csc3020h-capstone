﻿
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts {
    public class Cleanup : MonoBehaviour {
        public TextMeshProUGUI Credits;

        private const float FadeDuration = 1.0f;
		public float wait;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            Destroy(GameObject.Find("Player1(Clone)"));
            Destroy(GameObject.Find("Player2(Clone)"));
            Time.timeScale = 1;
        }

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            CleanupPersistObjects();

            StartCoroutine(ShowCredits());
        }

        public static void CleanupPersistObjectsGoToMenu() {
            CleanupPersistObjects();
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
        }

        private static void CleanupPersistObjects() {
            Destroy(GameObject.Find("GameHUD"));
            var networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
            networkManager.StopClient();
            networkManager.StopHost();
            Destroy(networkManager.gameObject);

            var settings = GameObject.Find("GameSettings").GetComponent<Settings>();
            settings.LevelManager.CleanLevelDirectory();
            Destroy(settings.gameObject);
        }

        /// <summary>
        /// Method tyo simply fade in and out credits and change scene
        /// </summary>
        /// <remarks>
        /// May need to be refactored if credits become more complex
        /// </remarks>
        private IEnumerator ShowCredits() {
            var color = Credits.color;
            var ticks = 60.0f;
            for (var i = 0; i < ticks; i++) {
                color.a = i / ticks;
                Credits.color = color;
                yield return null;
            }
            yield return new WaitForSeconds(wait);
            for (var i = 0; i < ticks; i++) {
                color.a = 1 - i / ticks;
                Credits.color = color;
                yield return null;
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
        }
    }
}

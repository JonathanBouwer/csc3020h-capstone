﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class PlatePairController : MonoBehaviour {
        [HideInInspector] public int PairCount;
        [HideInInspector] public Puzzle0Manager Manager;

        private PlatePair[] triggered;

        private struct PlatePair {
            public bool ATriggered, BTriggered, Solved;
        }

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        public void Start() {
            const short type = (short)NetworkMessageType.PUZZLE_0_PLATE_TRIGGER;
            NetworkServer.RegisterHandler(type, PlateTriggered);
            triggered = new PlatePair[PairCount];
            for (var index = 0; index < triggered.Length; index++) {
                var pair = new PlatePair {
                    ATriggered = false,
                    BTriggered = false,
                    Solved = false
                };
                triggered[index] = pair;
            }
        }

        /// <summary>
        /// Network message handler for plate trigger events
        /// </summary>
        /// <param name="message">Network message sent</param>
        public void PlateTriggered(NetworkMessage message) {
            var messageData = message.ReadMessage<StringMessage>().value;
            var splitData = messageData.Split(' ');
            var plateId = splitData[0];
            var isTriggered = splitData.Length > 1 && splitData[1] == "T";
            var index = int.Parse(plateId.Substring(0, plateId.Length - 1));
            var plateLetter = plateId[plateId.Length - 1];

            var pair = triggered[index];
            if (pair.Solved) return;
            switch (plateLetter) {
                case 'A':
                    pair.ATriggered = isTriggered;
                    break;
                case 'B':
                    pair.BTriggered = isTriggered;
                    break;
            }
            if (pair.ATriggered && pair.BTriggered) {
                Manager.DestroyPair(index);
                pair.Solved = true;
            }

            triggered[index] = pair;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Random = UnityEngine.Random;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class MazeBuilder : MonoBehaviour {
        public int MaxPlatePairs;

        [HideInInspector] public GameObject Wall, Floor, Plate;
        [HideInInspector] public int Width, Breadth;
        [HideInInspector] public GameObject MazeObject;

        private GameObject platePairs;
        private int[][] maze;
        private float yBase;
        private Random.State state;

        /// <summary>
        /// Method to generate maze
        /// </summary>
        /// <param name="seed">Random seed to use for generation</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Build(int seed) {
            Random.InitState(seed);
            MazeObject = new GameObject("Puzzle");
            var _maze = new GameObject("Maze");
            var _floor = new GameObject("Floor");
            platePairs = new GameObject("PlatePairs");
            var _fakeWalls = new GameObject("FakeWalls");
            var wall = Instantiate(Wall);
            var floor = Instantiate(Floor);
            var wallBounds = wall.GetComponent<Collider>().bounds;
            var wallSize = wallBounds.size;
            yBase = wallBounds.center.y - wallBounds.extents.y;
            state = Random.state;
            yield return StartCoroutine(GenerateMaze());
            Random.state = state;

            // Build Floor
            for (var i = 0; i < maze.Length; i++) {
                for (var j = 0; j < maze[i].Length; j++) {
                    CreateBrick(_floor, floor, new Vector3(j * wallSize.x, yBase, i * wallSize.z));
                }
            }
            state = Random.state;
            yield return null;
            Random.state = state;

            // Build Maze
            yBase += wallSize.y;
            var ends = new List<Vector3>();
            var fakes = new List<Vector3>();
            for (var i = 0; i < maze.Length; i++) {
                for (var j = 0; j < maze[i].Length; j++) {
                    if (maze[i][j] == 1) {
                        CreateBrick(_maze, wall, new Vector3(j * wallSize.x, yBase, i * wallSize.z));
                    }
                    if (maze[i][j] == 2) {
                        ends.Add(new Vector3(j * wallSize.x, 0, i * wallSize.z));
                    }
                    if (maze[i][j] == 3) {
                        fakes.Add(new Vector3(j * wallSize.x, 0, i * wallSize.z));
                    }
                }
            }
            state = Random.state;
            yield return null;
            Random.state = state;

            // Generate Plates
            var padPairs = Mathf.Min(MaxPlatePairs, ends.Count / 2);
            var pairControl = platePairs.AddComponent<PlatePairController>();
            pairControl.PairCount = padPairs;
            float hue = 0;
            for (var i = 0; i < padPairs; i++) {
                var index1 = Random.Range(0, ends.Count);
                var index2 = Random.Range(0, ends.Count - 1);
                var pos1 = ends[index1];
                ends.RemoveAt(index1);
                var pos2 = ends[index2];
                ends.RemoveAt(index2);

                var pair = new GameObject("Pair" + i);
                pair.transform.parent = platePairs.transform;
                var color = Color.HSVToRGB(hue, 1, 1);
                CreatePlate(pair, pos1, color, i, true);
                CreatePlate(pair, pos2, color, i, false);
                hue += 0.99f / padPairs;
            }

            state = Random.state;
            yield return null;
            Random.state = state;

            // Generate Fake Walls
            bool player1Ignore = true;
            foreach (var fakePos in fakes) {
                var fake = Instantiate(wall);
                fake.name = "FakeWall";
                fake.transform.parent = _fakeWalls.transform;
                var fakePosition = fakePos;
                fakePosition.y = yBase;
                fake.transform.localPosition = fakePosition;
                Destroy(fake.GetComponent<Collider>());
                fake.layer = LayerMask.NameToLayer(player1Ignore ? "Player1Ignore" : "Player2Ignore");
                player1Ignore = !player1Ignore;
            }
            state = Random.state;
            yield return null;
            Random.state = state;

            // Setup Camera Perspective Change
            var cameraPerspectiveControl = new GameObject("MazeCameraController") {
                layer = LayerMask.NameToLayer("Ignore Raycast")
            };
            cameraPerspectiveControl.transform.parent = _maze.transform;

            var boxCollider = cameraPerspectiveControl.AddComponent<BoxCollider>();
            var size = wallSize;
            size.Scale(new Vector3(2 * Width, 1, 2 * Breadth));
            var center = new Vector3(wallSize.x * Width, 0, wallSize.z * Breadth);
            boxCollider.center = center;
            boxCollider.size = size;
            boxCollider.isTrigger = true;

            cameraPerspectiveControl.AddComponent<CameraPerspectiveController>();

            Destroy(wall);
            Destroy(floor);
            _maze.transform.parent = MazeObject.transform;
            _floor.transform.parent = MazeObject.transform;
            platePairs.transform.parent = MazeObject.transform;
            _fakeWalls.transform.parent = MazeObject.transform;
        }

        /// <summary>
        /// Method to generate a brick which the maze is built out of
        /// </summary>
        /// <param name="parent">Parent of new gameobject ot create</param>
        /// <param name="brick">Prefab to instantiat</param>
        /// <param name="localPosition">Relative position of object to parent</param>
        private void CreateBrick(GameObject parent, GameObject brick, Vector3 localPosition) {
            var newWall = Instantiate(brick, parent.transform);
            newWall.transform.localPosition = localPosition;
            switch (parent.name) {
                case "Floor":
                    newWall.name = "MazeFloor";
                    break;
                case "Maze":
                    newWall.name = "MazeWall";
                    break;
            }
        }

        /// <summary>
        /// Method to generate a pressure plate
        /// </summary>
        /// <param name="parent">Parent of new gameobject ot create</param>
        /// <param name="localPosition">Relative position of object to parent</param>
        /// <param name="col">Plate colour</param>
        /// <param name="id">Plate ID</param>
        /// <param name="player1Plate">Plate visibility</param>
        private void CreatePlate(GameObject parent, Vector3 localPosition, Color col, int id, bool player1Plate) {
            var plate = Instantiate(Plate, parent.transform);
            plate.name = "MazePlate";
            plate.transform.localPosition = localPosition;
            plate.GetComponent<MeshRenderer>().material.color = col;
            plate.layer = LayerMask.NameToLayer(player1Plate ? "Player2Ignore" : "Player1Ignore");
            var trigger = plate.GetComponent<ClientTriggerController>();
            trigger.Prefix = id + (player1Plate ? "A" :"B");
            trigger.Message = " T";
        }

        private struct Position {
            public readonly int X, Y;

            public Position(int x, int y) {
                X = x;
                Y = y;
            }
        };

        /// <summary>
        /// Method to generate a mze within an int array using a randomized variant of Prim's Algorithm
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator GenerateMaze() {
            var mapWidth = 2 * Width + 1;
            var mapHeight = 2 * Breadth + 1;

            // Generate grid
            maze = new int[mapHeight][];
            for (var i = 0; i < maze.Length; i++) {
                maze[i] = new int[mapWidth];
                for (var j = 0; j < maze[i].Length; j++) {
                    if (i % 2 == 1 && j % 2 == 1) {
                        maze[i][j] = 0;
                        continue;
                    }
                    maze[i][j] = 1;
                }
            }


            // Break walls for maze
            var visited = new HashSet<Position> { new Position(1, 1) };
            var walls = new HashSet<Position> {
                new Position(2, 1),
                new Position(1, 2)
            };

            while (walls.Count > 0) {
                var chosenWall = walls.ElementAt(Random.Range(0, walls.Count - 1));
                var unvisitedCell = new Position(-1, -1);

                if (chosenWall.X % 2 == 0) {
                    var adjacentCell1 = new Position(chosenWall.X - 1, chosenWall.Y);
                    var adjacentCell2 = new Position(chosenWall.X + 1, chosenWall.Y);
                    if (!visited.Contains(adjacentCell1)) {
                        unvisitedCell = adjacentCell1;
                    } else if (!visited.Contains(adjacentCell2)) {
                        unvisitedCell = adjacentCell2;
                    }
                } else {
                    var adjacentCell1 = new Position(chosenWall.X, chosenWall.Y - 1);
                    var adjacentCell2 = new Position(chosenWall.X, chosenWall.Y + 1);
                    if (!visited.Contains(adjacentCell1)) {
                        unvisitedCell = adjacentCell1;
                    } else if (!visited.Contains(adjacentCell2)) {
                        unvisitedCell = adjacentCell2;
                    }
                }
                if (unvisitedCell.X < 0) {
                    walls.Remove(chosenWall);
                    continue;
                }
                visited.Add(unvisitedCell);
                maze[chosenWall.Y][chosenWall.X] = 0;
                if (unvisitedCell.X > 1) {
                    walls.Add(new Position(unvisitedCell.X - 1, unvisitedCell.Y));
                }
                if (unvisitedCell.X < mapWidth - 2) {
                    walls.Add(new Position(unvisitedCell.X + 1, unvisitedCell.Y));
                }
                if (unvisitedCell.Y > 1) {
                    walls.Add(new Position(unvisitedCell.X, unvisitedCell.Y - 1));
                }
                if (unvisitedCell.Y < mapHeight - 2) {
                    walls.Add(new Position(unvisitedCell.X, unvisitedCell.Y + 1));
                }
                walls.Remove(chosenWall);
            }

            // Open entrance near center
            var entrance = mapWidth / 2;
            if (maze[1][entrance] != 0) {
                var curOffset = 1;
                while (true) {
                    if (maze[1][entrance + curOffset] == 0) {
                        maze[0][entrance + curOffset] = 0;
                        entrance += curOffset;
                        break;
                    }
                    if (maze[1][entrance - curOffset] == 0) {
                        maze[0][entrance - curOffset] = 0;
                        entrance -= curOffset;
                        break;
                    }
                    curOffset++;
                }
            } else {
                maze[0][entrance] = 0;
            }
            state = Random.state;
            yield return null;
            Random.state = state;
            FindDeadEnds(0, entrance);
            FindFakeWallLocations();
        }

        /// <summary>
        /// Method to recursively find all dead ends in a Depth First Search fashion
        /// Stored in maze as 2
        /// </summary>
        /// <param name="curY">Current Y position</param>
        /// <param name="curX">Current X position</param>
        /// <returns>If (X,y) is a dead end</returns>
        private bool FindDeadEnds(int curY, int curX) {
            if (curY < 0 || curY >= maze.Length
                || curX < 0 || curX >= maze[curY].Length) return false;

            var val = maze[curY][curX];
            if (val == 1 || val == -1) return false;
            maze[curY][curX] = -1;
            if (!(FindDeadEnds(curY + 1, curX)
                | FindDeadEnds(curY - 1, curX)
                | FindDeadEnds(curY, curX + 1)
                | FindDeadEnds(curY, curX - 1))) maze[curY][curX] = 2;
            return true;
        }

        /// <summary>
        /// Method to find fake wall location, stored in maze as 3
        /// </summary>
        private void FindFakeWallLocations() {
            for (var i = 0; i < maze.Length; i++) {
                for (var j = 0; j < maze[i].Length; j++) {
                    if (maze[i][j] != 2) continue;
                    var coordinates = FindNextFork(j, i);
                    var dx = coordinates.X - j;
                    var dy = coordinates.Y - i;
                    if (Mathf.Sqrt(dx * dx + dy * dy) < 2) continue; // Exclude short distances
                    if (coordinates.Y < 2 ) continue; // Exclude near start
                    maze[coordinates.Y][coordinates.X] = 3;
                }
            }
        }

        /// <summary>
        /// Method to find next fork in road from dead end point x,y
        /// </summary>
        /// <param name="x">X Coordinate</param>
        /// <param name="y">Y Cooridante</param>
        /// <returns>Posiiton of next fork as X-Y Pair</returns>
        private Position FindNextFork(int x, int y) {
            var maxIterations = 10;
            var startPos = new Position(x, y);
            while (maxIterations > 0) {
                maxIterations--;
                if (x + 1 < maze[y].Length && maze[y][x + 1] == -1) {
                    if (IsFork(x + 1, y)) return new Position(x, y);
                    x += 1;
                    maze[y][x] = -2;
                } else if (x - 1 > 0 && maze[y][x - 1] == -1) {
                    if (IsFork(x - 1, y)) return new Position(x, y);
                    x -= 1;
                    maze[y][x] = -2;
                } else if (y + 1 < maze.Length && maze[y + 1][x] == -1) {
                    if (IsFork(x, y + 1)) return new Position(x, y);
                    y += 1;
                    maze[y][x] = -2;
                } else if (y - 1 > 0 && maze[y - 1][x] == -1) {
                    if (IsFork(x, y - 1)) return new Position(x, y);
                    y -= 1;
                    maze[y][x] = -2;
                }
            }
            return startPos;
        }

        /// <summary>
        /// Method to tell if a location is a fork
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <returns>Is fork</returns>
        private bool IsFork(int x, int y) {
            var count = 0;
            if (x + 1 < maze[y].Length && maze[y][x + 1] < 0) {
                count++;
            }
            if (x - 1 > 0 && maze[y][x - 1] < 0) {
                count++;
            }
            if (y + 1 < maze.Length && maze[y + 1][x] < 0) {
                count++;
            }
            if (y - 1 > 0 && maze[y - 1][x] < 0) {
                count++;
            }
            return count > 2;
        }
    }
}

﻿using UnityEngine;

public class ShipRotator : MonoBehaviour {

	public float speed;
	public bool flicker;
	public float lightPercentageFlicker;
	public GameObject ShipPrefab;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (0,10*speed * Time.deltaTime ,0,Space.Self);
		if (flicker) {
			if (ShipPrefab.activeSelf== false) { //if the light is on...
				ShipPrefab.SetActive(true); //turn it off
			} 
			if (Random.Range(0,100) <lightPercentageFlicker) { //a random chance
				ShipPrefab.SetActive(false);
			}
		}

	}
}

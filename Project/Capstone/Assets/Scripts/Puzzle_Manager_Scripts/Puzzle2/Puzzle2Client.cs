﻿using System.Collections;
using System.Linq;
using Assets.Scripts.Serialization_Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    public class Puzzle2Client : SceneManager {
        public GameObject Chaser, CodeScreen, TopGlass;
        public Animator Plates;
        public TMP_Text Code;
        public Material AltGlass, AltBlackGlass;
        public Puzzle2PlateTrigger[] PlateLights;

        private const string Filename = "Puzzle2.json";

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            StartCoroutine(NetworkManager.IsHost ? SetupPlayer1() : SetupPlayer2());
            StartCoroutine(Load());
            SetupNetworkHandlers();
        }

        /// <summary>
        /// Helper method to setup network handlers
        /// </summary>
        private void SetupNetworkHandlers() {
            const short type = (short) NetworkMessageType.PUZZLE_2_SOLVED;
            NetworkManager.client.RegisterHandler(type, RewardKey);

            const short type2 = (short)NetworkMessageType.PUZZLE_2_MISTAKE;
            NetworkManager.client.RegisterHandler(type2, ShowMistake);
        }

        /// <summary>
        /// Method to ensure all game objects are loaded before spawning players
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator Load() {
            yield return StartCoroutine(WaitForPersistObjectsToLoad());
            var fileData = GameSettings.LevelManager.LoadLevel(Filename);
            if (string.IsNullOrEmpty(fileData)) {
                yield return StartCoroutine(SaveLevel(false));
            } else {
                StartCoroutine(LoadLevel(fileData));
            }
            yield return StartCoroutine(SaveLevel(false));
            NotifyDoneLoading();
            SetActiveUIElements(false, true);
        }

        /// <summary>
        /// Helper method to save the level
        /// </summary>
        /// <param name="solved">Whether the puzzle is solved</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator SaveLevel(bool solved) {
            var mapper = new ScenePuzzle2Mapper();
            var serializer = new SceneSerializer<SerializedPuzzle2Data>(this, mapper);
            yield return StartCoroutine(serializer.SerializeSceneItem(new GameObject(solved ? "Solved" : "Unsolved").transform));
            GameSettings.LevelManager.SaveLevel(Filename, serializer.SerializedResult);
        }

        /// <summary>
        /// Helper method to load the level
        /// </summary>
        /// <param name="filedata">Loaded level file data</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator LoadLevel(string filedata) {
            var mapper = new ScenePuzzle2Mapper();
            var data = JsonUtility.FromJson<SceneSerializer<SerializedPuzzle2Data>.SerializedScene>(filedata);
            yield return null;
            var item = mapper.UnmapItem(data.Scene[0]);
            if (item.name == "Solved") {
                Plates.SetTrigger("Success");
                if (!NetworkManager.IsHost) Chaser.GetComponent<Chaser>().PowerDown();
                const short type = (short) NetworkMessageType.PUZZLE_2_SAVED_SOLVED;
                NetworkManager.client.Send(type, new EmptyMessage());
            }
        }

        /// <summary>
        /// Method to setup puzzle for player 1, will remove/edit things specific to their view
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator SetupPlayer1() {
            Destroy(Chaser);
            var clueController = GameObject.Find("Clue1").GetComponent<ClueController>();
            clueController.IgnoreLayer = "Player1Ignore";
            clueController = GameObject.Find("Clue2").GetComponent<ClueController>();
            clueController.IgnoreLayer = "Player1Ignore";

            TopGlass.GetComponent<Renderer>().material = AltGlass;
            const short type = (short)NetworkMessageType.PUZZLE_2_PATTERN;
            NetworkManager.client.RegisterHandler(type, SetupPatternPuzzle);
            yield return null;
        }

        /// <summary>
        /// Method to setup puzzle for player 2, will remove/edit things specific to their view
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator SetupPlayer2() {
            var clueController = GameObject.Find("Clue1").GetComponent<ClueController>();
            clueController.IgnoreLayer = "Player2Ignore";
            clueController = GameObject.Find("Clue2").GetComponent<ClueController>();
            clueController.IgnoreLayer = "Player2Ignore";
            CodeScreen.GetComponent<Renderer>().material = AltBlackGlass;
           
            yield return null;
        }

        /// <summary>
        /// Network message handler for receiving puzzle pattern event
        /// </summary>
        /// <param name="message">Network message sent</param>
        private void SetupPatternPuzzle(NetworkMessage message) {
            var pattern = message.ReadMessage<StringMessage>().value;
            const int cipher = 'A' - '1' + 13;
            Code.text = pattern.Aggregate("", (current, value) => current + (char)(value + cipher));
        }

        /// <summary>
        /// Network message handler for puzzle solved events
        /// </summary>
        /// <param name="message">Network message sent</param>
        private void RewardKey(NetworkMessage message) {
            GameHUD.AddKey(2);
            Plates.SetTrigger("Success");
            if (!NetworkManager.IsHost) Chaser.GetComponent<Chaser>().PowerDown();
            foreach (var plate in PlateLights) {
                plate.LightDown();
            }
            StartCoroutine(SaveLevel(true));
        }
        
        /// <summary>
        /// Network message handler for puzzle mistake events
        /// </summary>
        /// <param name="message">Network message sent</param>
        private void ShowMistake(NetworkMessage message) {
            Plates.SetTrigger("Mistake");
            foreach (var plate in PlateLights) {
                plate.LightDown();
            }
        }
    }
}

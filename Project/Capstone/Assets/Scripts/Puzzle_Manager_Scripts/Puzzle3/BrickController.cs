﻿using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
	public class BrickController : MonoBehaviour {
        [HideInInspector] public int Value, Id;
	    [HideInInspector] public PuzzleBuilder3 PuzzleBuilder;
	    [HideInInspector] public Puzzle3Manager Manager;
		[HideInInspector] public bool View, Markedmine, OnMine, GameFin;//,host;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
			View = false;
			OnMine = false;
			Markedmine = false;
			var puzzleClient=GameObject.Find("PuzzleClient");
			var puzzleManagerObj = puzzleClient.transform.GetChild (2);
			Manager = puzzleManagerObj.GetComponent<Puzzle3Manager>();
			PuzzleBuilder = GameObject.Find ("PuzzleClient").GetComponent<PuzzleBuilder3> ();
			GetComponentInParent<Renderer> ().material=PuzzleBuilder.MatBlocks [10];
		}

        /// <summary> 
        /// OnCollisionEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnCollisionEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player 1 enters wheck that they havent stepped on a mine otherwise reveal the block
        /// When player 2 enters set a variable
        /// </remarks>
        void OnCollisionEnter(Collision other) {
			if (!GameFin) {
				// If player 1, enters a block, open the block. If the block value is 9 send game lost message to manager, else open block
				if (other.gameObject.name == "Player1(Clone)" && Markedmine == false) {
					if (Value == 9) {
						Manager.HostGameLost ();
						GameFin = true;
					} else {
						OpenBrick (Value);
						Manager.OpenBlock (Id,Value);
						if (Value == 0) {
							Manager.FloodFill(Id);
						}
					}

				}
				//<summary>  if player 2, enters a block, that is closed, mark the mine is on
				else if (other.gameObject.name == "Player2(Clone)" && View==false) {
					OnMine = true;
				}
			}
		}

        /// <summary> 
        /// OnCollisionExit method to detect player leaves bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnCollisionExit.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player 2 leaves set a variable
        /// </remarks>
        void OnCollisionExit(Collision other){
			if (!GameFin) {
				// If player 2, leaves a block, mark the mine is off.
				if (other.gameObject.name == "Player2(Clone)") {
					OnMine = false;
				}
			}
		}

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// Wheck for player 2 pressing the mark key
        /// </remarks>
        public void Update(){
			if  (!GameFin && OnMine==true&&Input.GetKeyDown ("space")&&View==false) {
				PlaceMine ();
				Manager.MarkMine (Id);
			}
		}

       
		/// <summary> 
		/// Callback method to change block material and end game, sent by Game Manager
		/// </summary>
		public void GameFinish(bool won){
			if (won) {
				InvokeRepeating ("GameWonMat", Random.Range (0f, 2f), Random.Range (0.5f, 2.5f));
			} else {
				if (Value == 9) {
					GetComponentInParent<Renderer> ().material = PuzzleBuilder.MatBlocks [9];
					Manager.OpenBlock (Id,Value);
				} else {
					GetComponentInParent<Renderer> ().material =  PuzzleBuilder.loose;
				}
			}
			GameFin = true;
		}
		/// <summary> 
		/// Callback method to change block material and end game, sent by Game Manager
		/// </summary>
		public void GameWonMat(){
			GetComponentInParent<Renderer> ().material =  PuzzleBuilder.Win [Random.Range(0, 3)];
		}
	


        /// <summary> 
        /// Method to reveal block and change it's material, Ignores -1 which is the clients mine field
        /// </summary>
        public void OpenBrick(int val){
            View = true;
			if (val == -1)
				return;
			GetComponentInParent<Renderer> ().material = PuzzleBuilder.MatBlocks [val];
		}

        /// <summary> 
        /// Method to toggle the marked state of mines and their texture 
        /// </summary>
        public void PlaceMine(){
			if (Markedmine == false) {
				Markedmine = true;
				GetComponentInParent<Renderer> ().material = PuzzleBuilder.MatBlocks [11];
			} 
			else {
				Markedmine = false;
				GetComponentInParent<Renderer> ().material =  PuzzleBuilder.MatBlocks [10];
			}
		}


	}
}

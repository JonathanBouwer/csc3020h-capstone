﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Tutorial {
	public class TutorialManager : NetworkBehaviour {
		
		public GameObject LeavePlate;
		[HideInInspector]
		public int[] OrderWant;
		[HideInInspector]
		public int[] OrderCurr;
		public GameObject[] display;
		[HideInInspector]
		public Material[] mat;
		public Material[] DefaultMat;
		public GameObject playerObjects;
		int count=0;
		[HideInInspector]
		public bool won;
		private int _PlayersWon = 2;
		private const short type = (short)NetworkMessageType.TUTORIAL_WON;
		public TutorialBuilder tutBuild;
		public TutorialClient tutClient;
		NetworkManager _networkManager; 
		private bool wrong;
		public float timeError;
		private float time;
		void Start ()
		{
			_networkManager = NetworkManager.singleton; 
			_networkManager.client.RegisterHandler(type, OtherPlayerWon);
			NetworkServer.RegisterHandler (type, OtherPlayerWon);
			OrderCurr=new int[3];
			wrong = false;
			time = 0;

//			if (! GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>().IsHost) return;
//			var welcome = new StringMessage("Time: You had to do it? You couldn't have left it? Well now I have to help you fix me :(\nI've left some questions that may help you on your way, but please don't do any more damage");
//			_networkManager.client.Send((short) NetworkMessageType.SEND_CHAT_MESSAGE, welcome);
		}
		public void OtherPlayerWon(NetworkMessage message) {
			
			_PlayersWon--;
			if (_PlayersWon > 0) return;
			HasWon ();
		}
		void Update(){
			if (wrong) {
				if (time < timeError) {
					time += Time.deltaTime;
					for (int i = 0; i < 3; i++) {
						OrderCurr [i] = -1;
						display [i].GetComponent<Renderer> ().material = DefaultMat [Random.Range(0,2)];
					}
				} else {
					time = 0;
					wrong = false;
					ResetChoice(DefaultMat[0]);
				}
			
			}

		}
			
	
		public void Step(int index){
			if (won == false) {
				OrderCurr [count] = index;
				display [count].GetComponent<Renderer> ().material = mat [index];
				count++;
				if (count == 3) {
					CheckWon ();

				}
			}
		}
		public void CheckWon(){
		//	bool wonNow = true;
			for (int i = 0; i < 3; i++) {
				if (OrderWant [i] != OrderCurr [i]) {
					wrong = true;
					return;
				}
			}
		
			won = true;
			const short solvedType = (short)NetworkMessageType.TUTORIAL_WON;



			StartCoroutine(tutBuild.SaveEnvironment());
			if (GameObject.Find ("NetworkManager").GetComponent<GameNetworkManager> ().IsHost) {
				NetworkServer.SendToAll(solvedType, new EmptyMessage());

			} else {
				_networkManager.client.Send (solvedType, new EmptyMessage ());
				_PlayersWon--;
				Debug.Log ("PROGRESS");
			}

			if (_PlayersWon > 0) {
				InvokeRepeating ("DisplayWon", 0f, 0.1f);
				return;
			}
			HasWon ();



		}

		public void ResetChoice(Material mat){
			count = 0;
			for (int i = 0; i < 3; i++) {
				OrderCurr [i] = -1;
				display [i].GetComponent<Renderer>().material = mat;
			}
		}
		public void HasWon(bool shouldRewardKey=true){
			won = true;
			var leave = Instantiate (LeavePlate);//, parent.transform);
			leave.transform.localPosition = new Vector3(0,0,0);
			InvokeRepeating ("DisplayWon", 0f, 0.1f);
			tutClient.RewardKey (shouldRewardKey);
			playerObjects.SetActive (false);


		}
		public void DisplayWon(){
			for(int i=0;i<3;i++){
				display [i].GetComponent<Renderer>().material = mat[Random.Range (0, 3)];
			}
		}
			

		}


}

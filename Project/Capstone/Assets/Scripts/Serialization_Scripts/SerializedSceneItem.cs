﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Serialization_Scripts {
    [Serializable]
    public class SerializedSceneItem {
        public int Id;
        public List<int> ChildrenIds;
    }
}

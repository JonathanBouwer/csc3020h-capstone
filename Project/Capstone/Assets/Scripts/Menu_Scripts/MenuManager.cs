﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public enum MenuState {
        TitleScreen,
        MainMenu,
        SettingsMenu,
        PlayMenu
    }

    public class MenuManager : MonoBehaviour {
        private MenuUIManager[] menuUIElements;
        private MenuState menuState;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            menuState = MenuState.TitleScreen;
            var canvas = GameObject.Find("Canvas");
            menuUIElements = canvas.GetComponentsInChildren<MenuUIManager>();
        }
	
        void Update () {
            if (Input.anyKeyDown && menuState == MenuState.TitleScreen) {
                SetMenuMain();
            }
            if (Input.GetKeyDown(KeyCode.Escape) && menuState != MenuState.MainMenu) {
                SetMenuMain();
            }
        }

        /// <summary>
        /// Method to change the menu state and reset animation goal.
        /// </summary>
        /// <param name="state">State to go to</param>
        private void SetMenuState(MenuState state) {
            menuState = state;
            foreach (var menuUIElement in menuUIElements) {
                menuUIElement.SetGoal(menuState);
            }
        }

        /// <summary>
        /// Method to change the menu state to Title.
        /// </summary>
        public void SetMenuTitle() {
            SetMenuState(MenuState.TitleScreen);
        }

        /// <summary>
        /// Method to change the menu state to Main.
        /// </summary>
        public void SetMenuMain() {
            SetMenuState(MenuState.MainMenu);
        }

        /// <summary>
        /// Method to change the menu state to Play.
        /// </summary>
        public void SetMenuPlay() {
            SetMenuState(MenuState.PlayMenu);
        }

        /// <summary>
        /// Method to change the menu state to Settings.
        /// </summary>
        public void SetMenuSettings() {
            SetMenuState(MenuState.SettingsMenu);
        }

        /// <summary>
        /// Method to change the exit the game.
        /// </summary>
        public void ExitGame() {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}

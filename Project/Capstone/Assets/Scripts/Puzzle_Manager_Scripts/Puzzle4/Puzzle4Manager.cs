﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4 {
    public class Puzzle4Manager : NetworkBehaviour{

        [HideInInspector] public GameObject[] Predators;
        [HideInInspector] public PreyController Prey;
        [HideInInspector] public int Width, Breadth;
        [HideInInspector] private enum Turn {PredOne = 0, PreyOne = 1, PredTwo = 2, PreyTwo = 3}
        [HideInInspector] private Turn cTurn;

        private const short preyMoveRequestType = (short)NetworkMessageType.PUZZLE_4_PREY_MOVE_REQUEST;
        private const short preyMoveType = (short) NetworkMessageType.PUZZLE_4_PREY_MOVE_CLIENT;
        private const short predMoveRequestType = (short)NetworkMessageType.PUZZLE_4_PREDATOR_MOVE_REQUEST;
        private const short predMoveType = (short) NetworkMessageType.PUZZLE_4_PREDATOR_MOVE_CLIENT;
        private const short nextTurnType = (short) NetworkMessageType.PUZZLE_4_NEXT_TURN;
        private const short winType = (short) NetworkMessageType.PUZZLE_4_SOLVED;


        /// <summary> 
        /// Awake method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        private void Awake(){
            cTurn = 0;
            NetworkServer.RegisterHandler(preyMoveRequestType, MovePrey);
            NetworkServer.RegisterHandler(predMoveRequestType, MovePredator);
            NetworkServer.RegisterHandler(winType, GameWin);
        }

        private void Update(){
            if(cTurn == Turn.PreyOne | cTurn == Turn.PreyTwo){
            }
            //Prey
            
        }

        /// <summary>
        /// MovePrey method that moves the Prey in the game world
        /// </summary>
        /// <param name="message">Message recieved</param>
        public void MovePrey(NetworkMessage message){
            if (!(cTurn == Turn.PreyOne | cTurn == Turn.PreyTwo)) return;
            NetworkServer.SendToAll(preyMoveType, message.ReadMessage<StringMessage>());
            NextTurn();
        }
        
        /// <summary>
        /// MovePredator method that moves the specified Predator in the game world
        /// </summary>
        /// <param name="message">Message recieved</param>
        public void MovePredator(NetworkMessage message){
            var msg = message.ReadMessage<StringMessage>();
            var id = int.Parse(msg.value.Substring(0, 1));
            if (!(((id == 2 | id == 3) & cTurn == Turn.PredTwo) |
                  ((id == 0 | id == 1) & cTurn == Turn.PredOne))) return;
            NetworkServer.SendToAll(predMoveType, msg);
            NextTurn();
            //NextTurn(); //remove to work as intended
        }

        /// <summary>
        /// GameWon method that is triggered when the Prey is captured by the Predators and updates the game world
        /// </summary>
        /// <param name="message">Message recieved</param>
        public void GameWin(NetworkMessage message){
            NetworkServer.SendToAll(winType ,new EmptyMessage());
        }

        public void GameWon(){
            
        }

        /// <summary>
        /// Method
        /// </summary>
        public void NextTurn(){
            if (cTurn != Turn.PreyTwo) cTurn++;
            else cTurn = Turn.PredOne;
            NetworkServer.SendToAll(nextTurnType, new IntegerMessage((int) cTurn));
        }

    }
}

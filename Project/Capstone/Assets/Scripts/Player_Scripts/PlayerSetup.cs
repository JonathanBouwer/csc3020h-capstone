﻿using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Player_Scripts {
    public class PlayerSetup : NetworkBehaviour {

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            var cam = transform.Find("Camera").gameObject;
            if (isLocalPlayer) {
                cam.SetActive(true);
            } else {
                Destroy(cam);
            }
            var player1 = LayerMask.NameToLayer("Player1Ignore");
            var player2 = LayerMask.NameToLayer("Player2Ignore");
            Physics.IgnoreLayerCollision(player1, player2);
        }
    }
}

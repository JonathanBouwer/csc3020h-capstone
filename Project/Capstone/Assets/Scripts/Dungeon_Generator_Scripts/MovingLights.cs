﻿using UnityEngine;

namespace Assets.Scripts.Dungeon_Generator_Scripts {
	public class MovingLights : MonoBehaviour {

		public float speed;
		private Vector3 direction, MoveToPosition;
		private Vector3[] allDirections;
		public int LookAhead=10; //how far we look ahead --> must be multiple of 10, as looking at centre of tiles
		public int Cycles=4;//how many cycles of looking for corridor
		private int countCycle;
		public bool rotate;
		public bool flicker;
		public float Rotationspeed;
		public int lightPercentageFlicker;

		private Light lightAttatched;
		void Start () {
			allDirections = new Vector3[4];
			allDirections[0] = Vector3.forward;
			allDirections[1] = Vector3.left;
			allDirections[2] = Vector3.back;
			allDirections[3] = Vector3.right;
			int choose= Random.Range (0, 4);
			direction = allDirections [choose];
			GetMoveToPosition ();
			countCycle = 0;
			if (rotate) {
				transform.localRotation = Quaternion.Euler (20, 0, 20);
			}
			if (flicker) {
				lightAttatched = GetComponentInChildren<Light> ();
			}

		}
		
		// Update is called once per frame
		void Update () {

			transform.localPosition = Vector3.MoveTowards (transform.localPosition, MoveToPosition, speed * Time.deltaTime);
			if (rotate) {
				transform.Rotate (0,10*speed * Time.deltaTime ,0,Space.World);
			}

			if (transform.localPosition == MoveToPosition) {//reached position
				if (countCycle == 0) {//we can look for a corridor
					if (CorridorBlock ()) {//can we turn down  a corridor
						countCycle=Cycles;//now must not look for x cycles
						return;//our new moveTo is assigned
					}
				} else {
					countCycle--;
				}
				GetMoveToPosition ();
			}
			if (flicker) {
				if (lightAttatched.enabled == false) { //if the light is on...
					lightAttatched.enabled = true; //turn it off
				} 
				if (Random.Range(0,100) <lightPercentageFlicker) { //a random chance
					lightAttatched.enabled = false;
				}
			}

		}
		bool CorridorBlock(){
			for(int i =0;i<4;i++){
				Vector3 check = transform.position + LookAhead * allDirections [i];
				RaycastHit hit;
				bool rc = Physics.Raycast (check, Vector3.down,out hit,35);
				if (rc == false)
					continue;
				if(hit.transform.CompareTag("LightMoveToBlock")){
					MoveToPosition= transform.localPosition + (2*LookAhead)*allDirections[i];
					return true;
				}
			}
			return false;

		}

			

		void GetMoveToPosition (){
			RaycastHit hit;
			MoveToPosition = transform.localPosition + LookAhead * direction;
			bool rc = Physics.Raycast (transform.position + LookAhead * direction, Vector3.down, out hit, 30);
			while (rc == false || hit.transform.CompareTag ("MovingLightsIgnore")) {
				FindDirection ();
				MoveToPosition = transform.localPosition + LookAhead * direction;
				rc = Physics.Raycast (transform.position + LookAhead * direction, Vector3.down, out hit, 30);
			}
		}
		void FindDirection(){
			Vector3 oldDirection = direction;
			while (oldDirection == direction) {
				int choose= Random.Range (0, 4);
				direction = allDirections [choose];
			}
		}
	}
		
}

﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.UI_Scripts {
	public class ChatManager : NetworkBehaviour {
		private const short typeSend = (short)NetworkMessageType.SEND_CHAT_MESSAGE;
		private const short typeRecieve = (short)NetworkMessageType.RECEIVE_CHAT_MESSAGE;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        private void Awake(){
			NetworkServer.RegisterHandler(typeSend, RecieveMessage);
		}
		
		/// <summary>
        /// Handler for sent messages, forwards back to bothe clients
        /// </summary>
        /// <param name="message">Message sent</param>
		public void RecieveMessage(NetworkMessage message){
			NetworkServer.SendToAll(typeRecieve, message.ReadMessage<StringMessage>());
		}
	}
}
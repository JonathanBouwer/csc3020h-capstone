﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
    [Serializable]
    public class SerializedPuzzle3Data : SerializedSceneItem {
        public string Name;
        public Vector3 Position, Scale;
        public string[] Extra;
    }

    class ScenePuzzle3Mapper : SceneItemMapper<SerializedPuzzle3Data> {
        public PuzzleBuilder3 Builder;

        /// <summary>
        /// Constructor for puzzle mapper
        /// </summary>
        /// <param name="builder">Builder object for the mapper to use prefabs of</param>
		public ScenePuzzle3Mapper(PuzzleBuilder3 builder) {
            Builder = builder;
        }
        
        /// <summary>
        /// Specialized method to map items to SerializedPuzzle3Data
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <returns>Mapped item in SerializedPuzzle3Data format</returns>
        public override SerializedPuzzle3Data MapItem(Transform item) {
            var result = new SerializedPuzzle3Data {
                Position = item.position,
                Scale = item.localScale,
                Name = item.name
            };
			var split = item.name.Split(' ');
			if(split[0]=="decor"){
				result.Extra = new string[1];
				result.Extra [0] = split [1];
				result.Name = "decor";
			}

            if (item.name == "MineWall") {
                result.Extra = new string[1];
                result.Extra[0] = "" + item.localRotation.y;
            }

            return result;
        }
        
        /// <summary>
        /// Specialized method to unmap SerializedPuzzle3Data items
        /// </summary>
        /// <param name="item">Item to unmap</param>
        /// <returns>Game object instantiated by unmapping</returns>
        public override GameObject UnmapItem(SerializedPuzzle3Data item) {
            GameObject result;
            switch (item.Name) {
                case "MineWall":
                    result = Object.Instantiate(Builder.MazeWall);
                    if (item.Extra[0] != "0") {
                        result.transform.Rotate(0, 90, 0);
                    }
                    break;
                case "MineFloor":
                    result = Object.Instantiate(Builder.MazeFloor);
                    break;
                case "LeavePlate":
                    result = Object.Instantiate(Builder.LeavePlate);
                    break;
                case "Block":
                    result = Object.Instantiate(Builder.Block);
                    break;
				case "decor":
					int index = int.Parse (item.Extra [0]);
					result = Object.Instantiate(Builder.decor[index]);
					break;
                default:
                    result = new GameObject();
                    break;
            }
            result.name = item.Name;
            result.transform.position = item.Position;
            result.transform.localScale = item.Scale;
            return result;
        }
    }
}

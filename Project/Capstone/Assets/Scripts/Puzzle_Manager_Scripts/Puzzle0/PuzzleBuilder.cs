﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class PuzzleBuilder : MonoBehaviour {
        public int Width, Breadth, StartAreaLength;
        public MazeBuilder MazeBuilder;
        public RoomBuilder RoomBuilder;
        public GameObject MazeWall, MazeFloor, MazePlate, LeavePlate;

        [HideInInspector] public SavedLevelManager LevelManager;
        [HideInInspector] public SceneManager Manager;
        [HideInInspector] public SceneSerializer<SerializedPuzzle0Data> Serializer;

        private const string FileName = "Puzzle0Data.json";
        private GameObject platePairs;

        /// <summary>
        /// Generate a map from seed of file if it exists
        /// </summary>
        /// <param name="seed">Random seed to generate map based on</param>
        public void BuildMap(int seed) {
            var fileData = LevelManager.LoadLevel(FileName);
            if (string.IsNullOrEmpty(fileData)) {
                StartCoroutine(Build(seed));
            } else {
                StartCoroutine(Build(fileData));
            }
        }

        /// <summary>
        /// Generate a puzzle from a seed
        /// </summary>
        /// <param name="seed">Random seed to generate map based on</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator Build(int seed) {
            MazeBuilder.Width = Width;
            MazeBuilder.Breadth = Breadth - StartAreaLength;
            MazeBuilder.Wall = MazeWall;
            MazeBuilder.Floor = MazeFloor;
            MazeBuilder.Plate = MazePlate;
            yield return StartCoroutine(MazeBuilder.Build(seed));
            var maze = MazeBuilder.MazeObject;
            platePairs = GameObject.Find("PlatePairs");

            RoomBuilder.StartAreaLength = StartAreaLength;
            RoomBuilder.Width = Width;
            RoomBuilder.Breadth = Breadth;
            RoomBuilder.Maze = maze;
            RoomBuilder.Floor = MazeFloor;
            RoomBuilder.LeavingPlate = LeavePlate;
            RoomBuilder.DecorationCount = platePairs.transform.childCount;
            yield return StartCoroutine(RoomBuilder.BuildRoom());

            SetupPairController();

            yield return StartCoroutine(SaveEnvironment());

            yield return StartCoroutine(Manager.WaitForPersistObjectsToLoad());
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }

        /// <summary>
        /// Helper method to save environment to file
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator SaveEnvironment() {
            var environment = GameObject.Find("Environment");
            yield return StartCoroutine(Serializer.SerializeSceneItem(environment.transform));
            LevelManager.SaveLevel(FileName, Serializer.SerializedResult);
        }

        /// <summary>
        /// Generate map fromn file
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Build(string levelData) {
            yield return StartCoroutine(Serializer.DeserializeSceneItem(levelData));
            platePairs = GameObject.Find("PlatePairs");
            SetupPairController();
            SetupRoom();
            yield return StartCoroutine(Manager.WaitForPersistObjectsToLoad());
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }

        /// <summary>
        /// Initializer method to setup pair controller server side
        /// </summary>
        private void SetupPairController() {
            var puzzleManagerObj = GameObject.Find("PuzzleManager");
            if (puzzleManagerObj == null) return; // Server only item
            var puzzleManager = puzzleManagerObj.GetComponent<Puzzle0Manager>();
            puzzleManager.SetPairCount(platePairs.transform.childCount);
            var pairController = platePairs.GetComponent<PlatePairController>();
            pairController.Manager = puzzleManager;
        }

        /// <summary>
        /// Method to decorate the room with predefined decorations is random positions
        /// </summary>
        private void SetupRoom() {
            var decorations = GameObject.FindGameObjectsWithTag("Puzzle0Decoration");
            RoomBuilder.Decorations = new List<GameObject>(decorations);
            RoomBuilder.DecorationCount = platePairs.transform.childCount;

            var brick = GameObject.Find("MazeWall").GetComponent<Collider>();
            var brickWidth = brick.bounds.size.x;
            RoomBuilder.Maze = GameObject.Find("Maze");
            RoomBuilder.Width = Width;
            RoomBuilder.SetupClue(brickWidth);
        }

        /// <summary>
        /// Changes layer of clue to it ignores the other player
        /// </summary>
        /// <param name="tagToIgnore">Either Player1Ignore or PLayer2Ignore</param>
        public void SetClueIgnore(string tagToIgnore) {
            var clueController = GameObject.Find("Clue").GetComponentInChildren<ClueController>();
            clueController.IgnoreLayer = tagToIgnore;
        }
    }
}

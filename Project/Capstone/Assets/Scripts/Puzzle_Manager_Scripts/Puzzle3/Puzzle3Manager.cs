﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
    public class Puzzle3Manager : NetworkBehaviour {

        // MineIds is the list of id's that contain mines,
        // MarkedMineIds are the mines that the player has marked
        [HideInInspector] public HashSet<int> MineIds, MarkedMineIds;
        [HideInInspector] public int Width, Breadth;

        private const short type = (short)NetworkMessageType.PUZZLE_3_MINE_MARK;
        private BrickController[] bricks;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            MineIds = new HashSet<int>();
            MarkedMineIds = new HashSet<int>();
            NetworkServer.RegisterHandler(type, PlaceMineHost);
        }

        /// <summary>
        /// Gets all brick controllers, automatically in order of ID's
        /// </summary>
        public void HostGetListBricks() {
            GameObject enviroment = GameObject.Find("Puzzle");
            bricks = enviroment.GetComponentsInChildren<BrickController>();
        }
        
        /// <summary>
        /// Brick controller calls this to send a message to the client 
        /// with the id and value of the brick
        /// </summary>
        /// <param name="id">Brick ID</param>
        /// <param name="value">Brick Value</param>
        public void OpenBlock(int id, int value) {
            const short type = (short)NetworkMessageType.PUZZLE_3_BLOCK_OPEN;
            var message = new StringMessage(value + " " + id);
            NetworkServer.SendToAll(type, message);
        }
 
        /// <summary>
        /// Brick controller calls this, inorder to send a message 
        /// to the host with the id of the brick marked
        /// </summary>
        /// <param name="id">Marked Brick ID</param>
        public void MarkMine(int id) {
            const short markedtype = (short) NetworkMessageType.PUZZLE_3_MINE_MARK;
            var message = new IntegerMessage(id);
            NetworkManager _networkManager = NetworkManager.singleton;
            _networkManager.client.Send(markedtype, message);
        }
        
        /// <summary>
        /// Handler for the Place Mine Message, places and mine and checks if won.
        /// </summary>
        /// <param name="message">Message recieved</param>
        public void PlaceMineHost(NetworkMessage message) {
            int data = message.ReadMessage<IntegerMessage>().value;
            bricks[data].PlaceMine();
            if (!IsWin(data))
                return;
            GameWon();
            const short solvedType = (short)NetworkMessageType.PUZZLE_3_SOLVED;
            NetworkServer.SendToAll(solvedType, new EmptyMessage());
            StartCoroutine(GameObject.Find("PuzzleClient").GetComponent<PuzzleBuilder3>().SaveEnvironment());
        }
        
        /// <summary>
        /// Updates marked set and checks if equal to base set
        /// </summary>
        /// <param name="id">Marked mine ID</param>
        private bool IsWin(int id) {
            if (MarkedMineIds.Contains(id)) {
                MarkedMineIds.Remove(id);
            } else {
                MarkedMineIds.Add(id);
            }

            return MineIds.SetEquals(MarkedMineIds);
        }
        
        /// <summary>
        /// Game ends and update each brick, with a material
        /// </summary>
        public void GameLost() {
            UpdateAllBricksMaterial(false);
        }

        /// <summary>
        /// Game ends and update each brick, with a material
        /// </summary>
        public void GameWon() {
            UpdateAllBricksMaterial(true);
        }

        /// <summary>
        /// Updates each brick with material
        /// </summary>
        /// <param name="mats">material array to update with</param>
		private void UpdateAllBricksMaterial(bool won) {
            GameObject enviroment = GameObject.Find("Environment");
            BrickController[] allBricks = enviroment.GetComponentsInChildren<BrickController>();
            foreach (var brick in allBricks) {
                brick.GameFinish(won);
            }
        }
        
        /// <summary>
        /// Host sends message to client that game is lost
        /// </summary>
        public void HostGameLost() {
            GameLost();
            const short LostType = (short)NetworkMessageType.PUZZLE_3_LOST;
            NetworkServer.SendToAll(LostType, new EmptyMessage());
        }
        
        /// <summary>
        /// Host open brick flood fill of surrounding empty bricks
        /// </summary>
        /// <param name="id">Current Brick ID</param>
        public void FloodFill(int id) {
            int end = 2, beg = -1;
            if (id % Width == 0)
                beg = 0;
            if (id % Width == Width - 1) {
                end = 1;
            }

            for (int b = -1; b < 2; b++) {
                for (int w = beg; w < end; w++) {
                    int idfind = id + b * Width + w;
                    if (idfind < 0 || idfind >= Width * Breadth || idfind == id) continue;
                    if (bricks[idfind].Markedmine || bricks[idfind].View) continue;
                    int valueFound = bricks[idfind].Value;

                    bricks[idfind].OpenBrick(valueFound);
                    OpenBlock(idfind, valueFound);
                    if (valueFound == 0) {
                        FloodFill(idfind);
                    }
                }
            }
        }
    }
}

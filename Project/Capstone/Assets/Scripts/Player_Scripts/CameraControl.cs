﻿using Assets.Scripts.UI_Scripts;
using UnityEngine;

namespace Assets.Scripts.Player_Scripts {
    public class CameraControl : MonoBehaviour {
        public enum State {
            FirstPerson,
            ThirdPerson,
            TopDown,
            Free
        }
        public Transform LookAt;
        public Transform[] Ignore;
        public Transform FirstPersonCameraPostion, ThirdPersonCameraPostion, TopDownCameraPostion;

        private State CameraState, GoalState;
        private float transitionPercent, maxDistance;
        private Transform goalTransform;
        private GameHudController hud;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            TopDownCameraPostion.LookAt(LookAt);
            ThirdPersonCameraPostion.LookAt(LookAt);
            maxDistance = Vector3.Distance(ThirdPersonCameraPostion.position, LookAt.position);
            goalTransform = transform;
            transform.LookAt(LookAt);
            CameraState = State.ThirdPerson;
            GoalState = State.ThirdPerson;
            hud = GameObject.Find("GameHUD").GetComponent<GameHudController>();
        }

        /// <summary>
        /// Update method to update the camera's position
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        void Update() {
            if (!Input.GetMouseButton(0)) return;
            var mouseMoveY = Input.GetAxis("Mouse Y");
            var mouseMoveX = Input.GetAxis("Mouse X");
            var offsetX = Vector3.Cross(Vector3.up, LookAt.forward) * -mouseMoveX;
            var offsetY = Vector3.up * -mouseMoveY;
            var goal = ThirdPersonCameraPostion.position + offsetY + offsetX;
            goal -= LookAt.position;
            goal *= maxDistance / goal.magnitude;
            goal += LookAt.position;
            ThirdPersonCameraPostion.position = goal;
            ThirdPersonCameraPostion.LookAt(LookAt);
        }

        /// <summary>
        /// LateUpdate method to finally move the camera's position at the end of frame
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.LateUpdate.html">Unity Documentation</see> for more info
        /// </summary>
        void LateUpdate() {
            if (CameraState == GoalState) {
                if (CameraState == State.ThirdPerson) {
                    transform.position = BoundPosition(ThirdPersonCameraPostion.position);
                } else if (CameraState == State.FirstPerson) {
                    transform.position = FirstPersonCameraPostion.position;
                } else if (CameraState == State.TopDown) {
                    transform.position = new Vector3 {
                        x = LookAt.position.x,
                        y = goalTransform.position.y,
                        z = LookAt.position.z
                    };
                }
                transform.LookAt(LookAt.position);
            } else {
                transform.position = Vector3.Lerp(transform.position, goalTransform.position, transitionPercent);
                transform.rotation = Quaternion.Lerp(transform.rotation, goalTransform.rotation, transitionPercent);
                transitionPercent += 0.002f;
                if (Vector3.Distance(transform.position, goalTransform.position) < 0.001) {
                    CameraState = GoalState;
                    hud.SetCallibrating(false);
                }
            }
        }

        /// <summary>
        /// Method to set new camera state
        /// </summary>
        /// <param name="state">New camera state</param>
        public void SetCameraState(State state) {
            GoalState = state;
            transitionPercent = 0;
            hud.SetCallibrating(true);
            if (state == State.FirstPerson) {
                goalTransform = FirstPersonCameraPostion;
            } else if (state == State.ThirdPerson) {
                goalTransform = ThirdPersonCameraPostion;
            } else if (state == State.TopDown) {
                goalTransform = TopDownCameraPostion;
            } else {
                goalTransform = transform;
            }
        }

        /// <summary>
        /// Camera state getter
        /// </summary>
        /// <returns>Current Camera State or Free if it is transiitoning</returns>
        public State GetCameraState() {
            return CameraState == GoalState ? CameraState : State.Free;
        }

        /// <summary>
        /// Method to keep camera within bounds of walls and collisions so that nothing comes between it and view of player
        /// </summary>
        /// <param name="position">Proposed goal postion in world space</param>
        /// <returns>New goal position</returns>
        private Vector3 BoundPosition(Vector3 position) {
            var direction = position - LookAt.position;
            RaycastHit hit;
            var isHit = Physics.Raycast(LookAt.position, direction, out hit, direction.magnitude);
            var hitIsPlayer = hit.transform == LookAt.parent;
            var shouldIgnore = false;

            if (isHit) {
                foreach (var transform1 in Ignore) {
                    shouldIgnore |= hit.transform.name == transform1.name;
                    shouldIgnore |= hit.transform.name == transform1.name + "(Clone)";
                }
            }

            if (isHit && !hitIsPlayer && !shouldIgnore) {
                position = (hit.point - LookAt.position) * 0.99f + LookAt.position;
            }
            return position;
        }
    }
}

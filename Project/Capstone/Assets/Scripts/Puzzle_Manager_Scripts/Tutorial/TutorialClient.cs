﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Assets.Scripts.Serialization_Scripts;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Tutorial {
	public class TutorialClient : SceneManager {

		public TutorialBuilder Builder;
		public TutorialUI UI;

		void Start() {
			Builder.LevelManager = GameSettings.LevelManager;
			Builder.Manager = this;
			var mapper = new SerializedTutorial(Builder);
			Builder.Serializer = new SceneSerializer<TutorialData>(this, mapper);
			Builder.Manager = this;
			StartCoroutine(Builder.BuildMap(NetworkManager.NetworkSeed,NetworkManager.IsHost));
			Builder.SetClueIgnore(NetworkManager.IsHost ? "Player1Ignore" : "Player2Ignore");


		}
		public void RewardKey(bool shouldAddKey = true) {
			if(shouldAddKey) GameHUD.AddKey(0);
			UI.WonUI ();
	
		}

	

	}
}


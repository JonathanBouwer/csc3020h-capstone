﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts {
    /// <summary>
    /// Simple wrapper for a min-max pair
    /// </summary>
    [System.Serializable]
    public struct MinMax {
        public int Min;
        public int Max;
    }

    public enum TileType {
        Empty,
        Room,
        Corridor,
        Wall
    }

    public class Map : MonoBehaviour {
        public Room RoomPrefab;
        public FinalRoom FinalRoomPrefab;
        public string[] PuzzleNames;

        [HideInInspector] public int RoomCount, MaxPlacementAttempts, TotalPuzzleCount;
        [HideInInspector] public IntVector2 MapSize;
        [HideInInspector] public MinMax RoomSize;

        private List<Room> rooms;
        private List<Corridor> corridors;
        private Random.State savedRandomState;
        private TileType[,] tilesTypes;

        /// <summary>
        /// Simple method to set a tile in the tileType array's type.
        /// </summary>
        public void SetTileType(IntVector2 coordinates, TileType tileType) {
            tilesTypes[coordinates.x, coordinates.z] = tileType;
        }

        /// <summary>
        /// Simple method to get a tile in the tileType array's type.
        /// </summary>
        public TileType GetTileType(IntVector2 coordinates) {
            return tilesTypes[coordinates.x, coordinates.z];
        }

        /// <summary> 
        /// Generator function used to generate entire map.
        /// </summary>
        /// <param name="seed">Random seed to be used for generation, ensure this is shared between players</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Generate(int seed) {
            Random.InitState(seed);
            tilesTypes = new TileType[MapSize.x, MapSize.z];
            rooms = new List<Room>();

            // Generate Rooms
            CreateFinalRoom();
            for (int i = 0; i < RoomCount; i++) {
                Room roomInstance = CreateRoom(i);
                if (roomInstance == null) {
                    RoomCount = rooms.Count;
                    Debug.Log("Cannot make more rooms!");
                    Debug.Log("Created Rooms: " + RoomCount);
                    break;
                }

                StartCoroutine(roomInstance.Generate());
                savedRandomState = Random.state;
                yield return null;
                Random.state = savedRandomState;
            }

            // Delaunay Triangulation
            // (Create links between rooms in triangular fashion)
            yield return BowyerWatson();

            // Minimal Spanning Tree
            // (Using links from prior step, create the MST)
            yield return PrimMST();

            // Generate Corridors
            // (Using the MST connect rooms with corridors)
            foreach (Corridor corridor in corridors) {
                StartCoroutine(corridor.Generate());
                yield return null;
            }

            // Generate Walls
            yield return WallCheck();
            foreach (Room room in rooms) {
                yield return room.CreateWalls();
            }
            foreach (Corridor corridor in corridors) {
                yield return corridor.CreateWalls();
            }

            // Set Player Spawn Points
            GameObject player1Spawn = new GameObject("Player1Spawn");
            player1Spawn.transform.parent = transform;
            Vector3 player1Position = rooms[1].transform.localPosition;
            player1Position.x += 7f;
            player1Position.z += 7f;
            player1Spawn.transform.position = player1Position;

            GameObject player2Spawn = new GameObject("Player2Spawn");
            player2Spawn.transform.parent = transform;
            Vector3 player2Position = rooms[1].transform.localPosition;
            player2Position.x -= 7f;
            player2Position.z -= 7f;
            player2Spawn.transform.position = player2Position;
            
        }

        /// <summary> 
        /// Generator function used to set wall tiles to the correct type
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator WallCheck() {
            for (int x = 0; x < MapSize.x; x++) {
                for (int z = 0; z < MapSize.z; z++) {
                    if (tilesTypes[x, z] == TileType.Empty && IsWall(x, z)) {
                        tilesTypes[x, z] = TileType.Wall;
                    }
                }
            }
            yield return null;
        }

        /// <summary> 
        /// Function to determine if a coordinate is a wall.
        /// </summary>
        /// <param name="x">X component of the coordinate</param>
        /// <param name="z">Z component of the coordinate</param>
        /// <returns>This coordinate contains a wall</returns>
        private bool IsWall(int x, int z) {
            for (int i = x - 1; i <= x + 1; i++) {
                if (i < 0 || i >= MapSize.x) continue;
                for (int j = z - 1; j <= z + 1; j++) {
                    if (j < 0 || j >= MapSize.z || i == x && j == z) continue;
                    if (tilesTypes[i, j] == TileType.Room || tilesTypes[i, j] == TileType.Corridor) {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary> 
        /// Function to place a room and generate that room
        /// </summary>
        /// <param name="puzzleId">Puzzle index to use</param>
        /// <returns>This room generated</returns>
        private Room CreateRoom(int puzzleId) {
            Room newRoom = null;

            // Try as many as we can.
            for (int i = 0; i < MaxPlacementAttempts; i++) {
                var xSize = Random.Range(RoomSize.Min, RoomSize.Max + 1);
                var ySize = Random.Range(RoomSize.Min, RoomSize.Max + 1);
                IntVector2 size = new IntVector2(xSize, ySize);

                var xCoordinate = Random.Range(1, MapSize.x - size.x);
                var yCoordinate = Random.Range(1, MapSize.z - size.z);
                IntVector2 coordinates = new IntVector2(xCoordinate, yCoordinate);

                if (IsOverlapped(size, coordinates)) continue;

                newRoom = Instantiate(RoomPrefab);
                rooms.Add(newRoom);
                newRoom.RoomId = rooms.Count;
                newRoom.name = "Room " + newRoom.RoomId + " (" + coordinates.x + ", " + coordinates.z + ")";
                newRoom.Size = size;
                newRoom.Coordinates = coordinates;
                newRoom.transform.parent = transform;
                Vector3 position = CoordinatesToPosition(coordinates);
                position.x += size.x * 0.5f - 0.5f;
                position.z += size.z * 0.5f - 0.5f;
                position *= RoomMapManager.TileSize;
                newRoom.transform.localPosition = position;
                newRoom.GetComponent<ClientTriggerController>().Message = PuzzleNames[puzzleId];
                newRoom.PuzzleId = puzzleId;
                newRoom.Init(this);
                break;
            }

            return newRoom;
        }

        /// <summary> 
        /// Function to place a final room and generate that room
        /// </summary>
        /// <param name="puzzleId">Puzzle index to use</param>
        /// <returns>This room generated</returns>
        public void CreateFinalRoom() {
            FinalRoom newRoom = Instantiate(FinalRoomPrefab); ;
            IntVector2 size = newRoom.Size;

            var xCoordinate = Random.Range(1, MapSize.x - size.x);
            var yCoordinate = Random.Range(1, MapSize.z - size.z);
            IntVector2 coordinates = new IntVector2(xCoordinate, yCoordinate);

            rooms.Add(newRoom);
            newRoom.RoomId = rooms.Count;
            newRoom.name = "FinalRoom (" + coordinates.x + ", " + coordinates.z + ")";
            newRoom.Coordinates = coordinates;
            newRoom.transform.parent = transform;
            Vector3 position = CoordinatesToPosition(coordinates);
            position.x += size.x * 0.5f - 0.5f;
            position.z += size.z * 0.5f - 0.5f;
            position *= RoomMapManager.TileSize;
            newRoom.transform.localPosition = position;
            newRoom.Init(this);

            StartCoroutine(newRoom.Generate());
        }

        /// <summary>
        /// Method to determine whether a room of given size and location overlaps an existing room.
        /// </summary>
        /// <param name="size">Rooms size</param>
        /// <param name="coordinates">Rooms position</param>
        /// <returns>Whether to room overlaps an existing room</returns>
        private bool IsOverlapped(IntVector2 size, IntVector2 coordinates) {
            foreach (Room room in rooms) {
                // Give a little space between two rooms
                if (Mathf.Abs(room.Coordinates.x - coordinates.x + (room.Size.x - size.x) * 0.5f) < (room.Size.x + size.x) * 0.7f &&
                    Mathf.Abs(room.Coordinates.z - coordinates.z + (room.Size.z - size.z) * 0.5f) < (room.Size.z + size.z) * 0.7f) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Triangle class used in Daluaney Triangulation.
        /// </summary>
        private Triangle LootTriangle {
            get {
                Vector3[] vertexs =
                {
                    RoomMapManager.TileSize * new Vector3(MapSize.x * 2, 0, MapSize.z),
                    RoomMapManager.TileSize * new Vector3(-MapSize.x * 2, 0, MapSize.z),
                    RoomMapManager.TileSize * new Vector3(0, 0, -2 * MapSize.z)
                };

                Room[] tempRooms = new Room[3];
                for (int i = 0; i < 3; i++) {
                    tempRooms[i] = Instantiate(RoomPrefab);
                    tempRooms[i].transform.localPosition = vertexs[i];
                    tempRooms[i].name = "Loot Room " + i;
                    tempRooms[i].Init(this);
                }

                return new Triangle(tempRooms[0], tempRooms[1], tempRooms[2]);
            }
        }

        /// <summary>
        /// Generator method to perform the Bowyer-Watson algorithm to determine the Daluaney Triangulation of given rooms
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator BowyerWatson() {
            List<Triangle> triangulation = new List<Triangle>();

            Triangle loot = LootTriangle;
            triangulation.Add(loot);

            foreach (Room room in rooms) {
                List<Triangle> badTriangles = new List<Triangle>();

                foreach (Triangle triangle in triangulation) {
                    if (triangle.IsContaining(room)) {
                        badTriangles.Add(triangle);
                    }
                }

                List<Corridor> polygon = new List<Corridor>();
                foreach (Triangle badTriangle in badTriangles) {
                    foreach (Corridor corridor in badTriangle.Corridors) {
                        if (corridor.Triangles.Count == 1) {
                            polygon.Add(corridor);
                            corridor.Triangles.Remove(badTriangle);
                            continue;
                        }

                        foreach (Triangle triangle in corridor.Triangles) {
                            if (triangle == badTriangle) {
                                continue;
                            }

                            // Delete Corridor which is between two bad triangles.
                            if (badTriangles.Contains(triangle)) {
                                corridor.Rooms[0].RoomCorridor.Remove(corridor.Rooms[1]);
                                corridor.Rooms[1].RoomCorridor.Remove(corridor.Rooms[0]);
                                Destroy(corridor.gameObject);
                            } else {
                                polygon.Add(corridor);
                            }
                            break;
                        }
                    }
                }

                // Delete Bad Triangles
                for (int index = badTriangles.Count - 1; index >= 0; --index) {
                    Triangle triangle = badTriangles[index];
                    badTriangles.RemoveAt(index);
                    triangulation.Remove(triangle);
                    foreach (Corridor corridor in triangle.Corridors) {
                        corridor.Triangles.Remove(triangle);
                    }
                }

                foreach (Corridor corridor in polygon) {
                    Triangle newTriangle = new Triangle(corridor.Rooms[0], corridor.Rooms[1], room);
                    triangulation.Add(newTriangle);
                }
            }
            yield return null;

            for (int index = triangulation.Count - 1; index >= 0; index--) {
                if (triangulation[index].Rooms.Contains(loot.Rooms[0]) || triangulation[index].Rooms.Contains(loot.Rooms[1]) ||
                    triangulation[index].Rooms.Contains(loot.Rooms[2])) {
                    triangulation.RemoveAt(index);
                }
            }

            foreach (Room room in loot.Rooms) {
                List<Corridor> deleteList = new List<Corridor>();
                foreach (KeyValuePair<Room, Corridor> pair in room.RoomCorridor) {
                    deleteList.Add(pair.Value);
                }
                for (int index = deleteList.Count - 1; index >= 0; index--) {
                    Corridor corridor = deleteList[index];
                    corridor.Rooms[0].RoomCorridor.Remove(corridor.Rooms[1]);
                    corridor.Rooms[1].RoomCorridor.Remove(corridor.Rooms[0]);
                    Destroy(corridor.gameObject);
                }
                Destroy(room.gameObject);
            }
        }

        /// <summary>
        /// Generator funtion to perform Prim's algorithm to connect rooms in a minimum spanning tree.
        /// </summary> 
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator PrimMST() {
            List<Room> connectedRooms = new List<Room>();
            corridors = new List<Corridor>();

            connectedRooms.Add(rooms[0]);

            while (connectedRooms.Count < rooms.Count) {
                KeyValuePair<Room, Corridor> minLength = new KeyValuePair<Room, Corridor>();
                List<Corridor> deleteList = new List<Corridor>();

                foreach (Room room in connectedRooms) {
                    foreach (KeyValuePair<Room, Corridor> pair in room.RoomCorridor) {
                        if (connectedRooms.Contains(pair.Key)) {
                            continue;
                        }
                        if (minLength.Value == null || minLength.Value.Length > pair.Value.Length) {
                            minLength = pair;
                        }
                    }
                }

                // Check Unnecessary Corridors.
                foreach (KeyValuePair<Room, Corridor> pair in minLength.Key.RoomCorridor) {
                    if (connectedRooms.Contains(pair.Key) && (minLength.Value != pair.Value)) {
                        deleteList.Add(pair.Value);
                    }
                }

                // Delete corridors
                for (int index = deleteList.Count - 1; index >= 0; index--) {
                    Corridor corridor = deleteList[index];
                    corridor.Rooms[0].RoomCorridor.Remove(corridor.Rooms[1]);
                    corridor.Rooms[1].RoomCorridor.Remove(corridor.Rooms[0]);
                    deleteList.RemoveAt(index);
                    Destroy(corridor.gameObject);
                }

                connectedRooms.Add(minLength.Key);
                corridors.Add(minLength.Value);
            }
            yield return null;
        }

        /// <summary>
        /// Helper method to covert coordinates to world positon.
        /// </summary>
        /// <param name="coordinates">Coordinates of object</param>
        /// <returns>World coordinates of object</returns>
        public Vector3 CoordinatesToPosition(IntVector2 coordinates) {
            return new Vector3 {
                x = coordinates.x - MapSize.x * 0.5f + 0.5f,
                y = 0f,
                z = coordinates.z - MapSize.z * 0.5f + 0.5f
            };
        }

    }
}
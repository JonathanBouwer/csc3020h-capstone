﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class LightController : MonoBehaviour {
        public float Speed;

        private List<Vector3> possibleMovements;
        private Vector3 velocity;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            possibleMovements = new List<Vector3> {
                new Vector3(1, 0, 0),
                new Vector3(1, 0, 1),
                new Vector3(0, 0, 1),
                new Vector3(-1, 0, 0),
                new Vector3(-1, 0, -1),
                new Vector3(0, 0, -1),
                new Vector3(1, 0, -1),
                new Vector3(-1, 0, 1)
            };
            AssigneNewVelocity();
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will attempt to assign a random velocity to the lights if they go off screen
        /// </remarks>
        void Update () {
            transform.position = transform.position + Speed * velocity;
            if(OutOfBounds()) {
                AssigneNewVelocity();
            }
        }

        /// <summary>
        /// Method to determin if lights are off screen.
        /// </summary>
        /// <returns>Whether lights are off screen</returns>
        private bool OutOfBounds() {
            return Mathf.Abs(transform.position.x) > 200 || Mathf.Abs(transform.position.z) > 200;
        }

        /// <summary>
        /// Resets light position and assigns random velocity
        /// </summary>
        private void AssigneNewVelocity() {
            velocity = possibleMovements[Random.Range(0, possibleMovements.Count - 1)];
            transform.position = -200 * velocity;
        }
    }
}

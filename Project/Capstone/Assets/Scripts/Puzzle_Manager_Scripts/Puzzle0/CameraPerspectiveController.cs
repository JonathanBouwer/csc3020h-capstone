﻿using Assets.Scripts.Player_Scripts;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    class CameraPerspectiveController : MonoBehaviour {
        /// <summary> 
        /// OnTriggerEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will change the players camera above the players head
        /// </remarks>
        void OnTriggerEnter(Collider other) {
            if (other.gameObject.tag != "Player") return;
            var camControl = other.GetComponentInChildren<CameraControl>();
            if (camControl == null) return;
            camControl.SetCameraState(CameraControl.State.TopDown);
        }

        /// <summary> 
        /// OnTriggerExit method to detect player exiting bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerExit.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will reset the player camera
        /// </remarks>
        void OnTriggerExit(Collider other) {
            if (other.gameObject.tag != "Player") return;
            var camControl = other.GetComponentInChildren<CameraControl>();
            if (camControl == null) return;
            camControl.SetCameraState(CameraControl.State.ThirdPerson);
        }
    }
}

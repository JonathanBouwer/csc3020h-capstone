using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.UI_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts {
    public class SceneManager : MonoBehaviour {
        protected GameNetworkManager NetworkManager;
        protected GameHudController GameHUD;
        protected Settings GameSettings;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake () {
            NetworkManager = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>();
            GameHUD = GameObject.Find("GameHUD").GetComponent<GameHudController>();
            GameSettings = GameObject.Find("GameSettings").GetComponent<Settings>();

            const short type = (short)NetworkMessageType.SPAWN_DONE;
            NetworkManager.client.RegisterHandler(type, SpawnDone);
            GameHUD.SetLoading(true);
        }

        /// <summary>
        /// Network handler for Sapwn Done messages,interacts with UI
        /// </summary>
        /// <param name="message">Message sent</param>
        private void SpawnDone(NetworkMessage message) {
            GameHUD.SetLoading(false);
            GameHUD.SetHudActive(true);
            // Default Minimap and Chat to active for next scene
            GameSettings.ResetAudioSources();
            GameSettings.UpdateAllAudioSourceVolumes();
            GameHUD.MinimapActive = true;
            GameHUD.ChatActive = true;
        }

        /// <summary>
        /// Method to send message to server to alert it the we are Read for Spawn
        /// </summary>
        public void NotifyDoneLoading() {
            const short type = (short)NetworkMessageType.READY_FOR_SPAWN;
            var message = new IntegerMessage(NetworkManager.client.connection.connectionId);
            NetworkManager.client.Send(type, message);
            GameHUD.SetWaiting();
        }

        /// <summary>
        /// Method to assign which UI Elements to enable/Disable on spawn
        /// </summary>
        /// <param name="minimapActive">Minimap will be active</param>
        /// <param name="chatActive">Chat will be active</param>
        public void SetActiveUIElements(bool minimapActive, bool chatActive) {
            GameHUD.MinimapActive = minimapActive;
            GameHUD.ChatActive = chatActive;
        }

        /// <summary>
        /// Simple method to get a list fo completed puzzles
        /// </summary>
        /// <returns>List containing puzzle ids of completed puzzles</returns>
        public List<int> GetSolvedPuzzles() {
            return GameHUD.AquiredKeys;
        }

        /// <summary>
        /// Simple method to get a list fo completed puzzles
        /// </summary>
        /// <returns>List containing puzzle ids of completed puzzles</returns>
        public Vector3 GetBaseSpawnPos() {
            return GameHUD.MapBaseSpawnPosition;
        }


        /// <summary>
        /// Checker method to ensure scene has loaded properly before spawning players
        /// </summary>
        /// <returns></returns>
        public IEnumerator WaitForPersistObjectsToLoad() {
            string[] persistObjects = { "GameHUD", "GameSettings", "NetworkManager" };
            while (persistObjects.Any(str => GameObject.Find(str) == null)) {
                yield return null;
            }
        }
    }
}

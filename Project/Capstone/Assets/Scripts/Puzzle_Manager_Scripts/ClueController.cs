﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts {
    public class ClueController : MonoBehaviour {
        public TextAsset Text;
        public float FadeRate;
        public string IgnoreLayer;

        private TextMeshProUGUI clueDisplay;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        public void Start () {
            clueDisplay = GameObject.Find("Subtitle").GetComponent<TextMeshProUGUI>();
        }

        /// <summary> 
        /// OnTriggerEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will display a small prompt in this scenario
        /// </remarks>
        public void OnTriggerEnter(Collider other) {
            if (other.gameObject.layer == LayerMask.NameToLayer(IgnoreLayer)) return;
            clueDisplay.text = Text.text;
            StopAllCoroutines();
            StartCoroutine(FadeIn());
        }

        /// <summary> 
        /// OnTriggerExit method to detect player exiting bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerExit.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will remove a small prompt in this scenario
        /// </remarks>
        public void OnTriggerExit(Collider other) {
            if (other.gameObject.layer == LayerMask.NameToLayer(IgnoreLayer)) return;
            StopAllCoroutines();
            StartCoroutine(FadeOut());
        }

        /// <summary>
        /// Generator method to fade opacity of prompt
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator FadeIn() {
            for (var i = clueDisplay.alpha; i < 1f; i += FadeRate) {
                clueDisplay.alpha = i;
                yield return null;
            }
        }

        /// <summary>
        /// Generator method to fade transparency of prompt
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator FadeOut() {
            for (var i = clueDisplay.alpha; i >= 0f; i -= FadeRate) {
                clueDisplay.alpha = i;
                yield return null;
            }
        }
    }
}

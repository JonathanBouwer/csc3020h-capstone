﻿using System.Collections;
using Assets.Scripts.UI_Scripts;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Dungeon_Generator_Scripts {
    public class FinalRoom : Room {
        public Light RoomLight;
        public Color Color1, Color2;
        public Transform Entrances;

        private float lightColorPercentage;
        private GameHudController gameHud;
        private bool doorsAreOpen;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            lightColorPercentage = 0;
            InvokeRepeating("TransitionLight", 0, Time.deltaTime*2);
            gameHud = GameObject.Find("GameHUD").GetComponent<GameHudController>();
            doorsAreOpen = false;
            var networkManager = NetworkManager.singleton;
            var type = (short) NetworkMessageType.KEY_TEST_SUCESS;
            networkManager.client.RegisterHandler(type, ShrinkDoors);
        }

        /// <summary>
        /// Simple method to pulse a light
        /// </summary>
        private void TransitionLight() {
            lightColorPercentage = Mathf.PingPong(Time.time / 2.5f , 1);
            RoomLight.color = Color.Lerp(Color1, Color2, lightColorPercentage);
        }

        /// <summary> 
        /// OnTriggerEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player enters this trigger it will show keys
        /// </remarks>
        void OnTriggerEnter(Collider other) {
            gameHud.ShowKeys();
        }

        /// <summary>
        /// Method to handle messages to shrink doors on key test sucess
        /// </summary>
        /// <param name="message"></param>
        private void ShrinkDoors(NetworkMessage message) {
            if (doorsAreOpen) return;
            StartCoroutine(ShrinkDoorsAnimation());
            doorsAreOpen = true;
        }

        /// <summary> 
        /// OnTriggerExit method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerExit.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player exits this trigger it hide the keys
        /// </remarks>
        void OnTriggerExit(Collider other) {
            gameHud.HideKeys();
        }

        /// <summary>
        /// Simple method to linearly shrink the force field doors
        /// </summary>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator ShrinkDoorsAnimation() {
            for (var i = 0; i < 100; i++) {
                var scale = 1 - i / 100f;
                Entrances.transform.localScale = new Vector3(1, scale, 1);
                yield return null;
            }
        }

    }
}

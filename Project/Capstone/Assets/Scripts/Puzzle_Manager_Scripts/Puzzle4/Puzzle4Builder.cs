﻿using System.Collections;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4 {
    public class Puzzle4Builder : MonoBehaviour {
        public int Width, Breadth, StartAreaLength;
        public HuntBuilder HuntBuilder;
        public Room4Builder RoomBuilder;
        public Puzzle4Manager PuzzleManager;
        public GameObject MazeWall, MazeFloor, Block, LeavePlate, RestartPlate;
        //public Material[] MatBlocks;

        [HideInInspector] public SavedLevelManager LevelManager;
        [HideInInspector] public SceneManager Manager;
        [HideInInspector] public SceneSerializer<SerializedPuzzle4Data> Serializer;

        private const string FileName = "Puzzle4Data.json";
        private bool host;
        
        /// <summary>
        /// Build Map only if previous data not saved
        /// </summary>
        public void BuildMap() {
            host = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>().IsHost;
            var fileData = LevelManager.LoadLevel(FileName);
            if (string.IsNullOrEmpty(fileData)) {
                StartCoroutine(Build());
            } else {
                StartCoroutine(Build(fileData));
            }
        }

        /// <summary>
        /// Builds World, with seperate instructions for host and client
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator Build() {
            HuntBuilder.Width = Width;
            HuntBuilder.Breadth = Breadth;
            HuntBuilder.Block = Block;
            
            yield return StartCoroutine(HuntBuilder.Build());
            if (host){
                PuzzleManager.Width = Width;
                PuzzleManager.Breadth = Breadth;
            }
            //PuzzleManager.HostGetListBricks();
            /*if (host) {
                yield return StartCoroutine(HuntBuilder.BuildHost());
                PuzzleManager.Width = Width;
                PuzzleManager.Breadth = Breadth;
                //PuzzleManager.HostGetListBricks();
            } else {
                yield return StartCoroutine(HuntBuilder.BuildClient());
            }*/
            if (host){
                var clueController = GameObject.Find("Clue1").GetComponent<ClueController>();
                clueController.IgnoreLayer = "Player1Ignore";
            }
            else{
                var clueController = GameObject.Find("Clue1").GetComponent<ClueController>();
                clueController.IgnoreLayer = "Player2Ignore";
            }
            var grid = HuntBuilder.HuntField;
            RoomBuilder.StartAreaLength = StartAreaLength;
            RoomBuilder.Width = Width;
            RoomBuilder.Breadth = Breadth;
            RoomBuilder.Grid = grid;
            RoomBuilder.Floor = MazeFloor;
            RoomBuilder.Wall = MazeWall;
            RoomBuilder.LeavingPlate = LeavePlate;
            RoomBuilder.RestartPlate = RestartPlate;
            yield return StartCoroutine(RoomBuilder.BuildRoom());

            // In this puzzle it only makes sense to save data if won, as we want the puzzle to regenerate everytime when restarts
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }

        /// <summary>
        /// Helper method to save environment to file
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator SaveEnvironment() {
            var environment = GameObject.Find("Environment");
            yield return StartCoroutine(Serializer.SerializeSceneItem(environment.transform));
            LevelManager.SaveLevel(FileName, Serializer.SerializedResult);
        }

        /// <summary>
        /// Generate map fromn file
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Build(string levelData) {
            yield return StartCoroutine(Serializer.DeserializeSceneItem(levelData));
            PuzzleManager.GameWon();
            Manager.NotifyDoneLoading();
            Manager.SetActiveUIElements(false, true);
        }
    }
}

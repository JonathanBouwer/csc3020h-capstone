﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    public class Puzzle2Manager : NetworkBehaviour {
        private string triggerOrder, currentOrder;
        private bool solved;

        /// <summary> 
        /// Start method to initialize class variables before initial frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            SetupNetworkHandlers();
        }

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            currentOrder = "";
            triggerOrder = "";
            solved = false;
            ShuffleOrder();
        }

        /// <summary>
        /// Helper method to setup network handlers
        /// </summary>
        private void SetupNetworkHandlers() {
            const short type = (short) NetworkMessageType.PUZZLE_2_TRIGGER;
            NetworkServer.RegisterHandler(type, PlateTrigger);
            const short type2 = (short) NetworkMessageType.PUZZLE_2_SAVED_SOLVED;
            NetworkServer.RegisterHandler(type2, msg => solved = true);
            const short type3 = (short)NetworkMessageType.PUZZLE_2_RESET;
            NetworkServer.RegisterHandler(type3, Reset);
        }

        /// <summary>
        /// Helper method to shuffle order of puzzle triggers for correct solution
        /// </summary>
        /// <remarks>
        /// Implemented using Fisher-Yates shuffle
        /// </remarks>
        private void ShuffleOrder() {
            char[] vals = {'1', '2', '3', '4', '5', '6', '7', '8'};
            for (var k = 7; k > 0; k--) {
                var swap = Random.Range(0, k);
                var tmp = vals[k];
                vals[k] = vals[swap];
                vals[swap] = tmp;
            }
            triggerOrder = new string(vals);
            foreach (var networkConnection in NetworkServer.localConnections) {
                const short type = (short) NetworkMessageType.PUZZLE_2_PATTERN;
                networkConnection.Send(type, new StringMessage(triggerOrder));
                break;
            }
        }

        /// <summary>
        /// Network handler for plate trigger events
        /// </summary>
        /// <param name="message">The network message sent</param>
        private void PlateTrigger(NetworkMessage message) {
            if (solved) return;
            var messageData = message.ReadMessage<StringMessage>().value;
            var splitData = messageData.Split(' ');
            var isTriggered = splitData.Length > 1 && splitData[1] == "T";
            if (!isTriggered) return;
            var plateId = splitData[0];
            if (currentOrder.Contains(plateId)) return;

            currentOrder = currentOrder + plateId;
            if (currentOrder.Length != triggerOrder.Length) return;

            if (currentOrder == triggerOrder) {
                const short solvedType = (short) NetworkMessageType.PUZZLE_2_SOLVED;
                NetworkServer.SendToAll(solvedType, new EmptyMessage());
                solved = true;
            } else {
                const short solvedType = (short)NetworkMessageType.PUZZLE_2_MISTAKE;
                NetworkServer.SendToAll(solvedType, new EmptyMessage());
            }

            currentOrder = "";
        }

        /// <summary>
        /// Network handler for reset puzzle events
        /// </summary>
        /// <param name="message">The network message sent</param>
        private void Reset(NetworkMessage message) {
            if (solved) return;
            currentOrder = "";
            const short solvedType = (short)NetworkMessageType.PUZZLE_2_MISTAKE;
            NetworkServer.SendToAll(solvedType, new EmptyMessage());
        }
    }
}

﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Tutorial {
	[Serializable]
	public class TutorialData : SerializedSceneItem {
		public bool Won;

	};

	class SerializedTutorial : SceneItemMapper<TutorialData> {
		public TutorialBuilder Builder;

		public SerializedTutorial(TutorialBuilder builder) {
			Builder = builder;
		}

		public override TutorialData MapItem(Transform item) {
			var result = new TutorialData {
				Won=true
			};
			return result;
		}

		public override GameObject UnmapItem(TutorialData item) {
			GameObject result = null;
			return result;
		}
	}
}


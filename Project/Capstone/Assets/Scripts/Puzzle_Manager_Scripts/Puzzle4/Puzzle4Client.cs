﻿using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4 {
	public class Puzzle4Client : SceneManager {
		public Puzzle4Builder Builder;
		public Puzzle4Manager Manager;

		public Material[] WinMats;
		public Material WinPreyMat;

		[HideInInspector] public GameObject[] Predators;
		[HideInInspector] public GameObject Prey;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
			const short type1 = (short)NetworkMessageType.PUZZLE_4_PREY_MOVE_CLIENT;
			NetworkManager.client.RegisterHandler(type1, MovePrey);
			const short type2 = (short)NetworkMessageType.PUZZLE_4_PREDATOR_MOVE_CLIENT;
			NetworkManager.client.RegisterHandler(type2, MovePredator);
	        const short type3 = (short) NetworkMessageType.PUZZLE_4_NEXT_TURN;
	        NetworkManager.client.RegisterHandler(type3, NextTurn);
			const short type4 = (short)NetworkMessageType.PUZZLE_4_SOLVED;
			NetworkManager.client.RegisterHandler(type4, RewardKey);
			Builder.LevelManager = GameSettings.LevelManager;
			Builder.Manager = this;
			var mapper = new ScenePuzzle4Mapper(Builder);
			Builder.Serializer = new SceneSerializer<SerializedPuzzle4Data>(this, mapper);
			Builder.BuildMap();

	        Predators = Builder.HuntBuilder.Predators;
	        Prey = Builder.HuntBuilder.Prey;
        }

		/// <summary>
		/// Handler fot network message to move the prey
		/// </summary>
		/// <param name="message">Message received</param>
		public void MovePrey(NetworkMessage message){
			var data = message.ReadMessage<StringMessage>().value;

			var cPos = data.IndexOf(':');
			
			var x = int.Parse(data.Substring(0, cPos));
			var y = int.Parse(data.Substring(cPos + 1));
			//Move the Prey to the desired block
			Prey.GetComponent<PreyController>().MovePrey(x, y);
		}
		
		/// <summary>
		/// Handler fot network message to move the required predator
		/// </summary>
		/// <param name="message">Message received</param>
		public void MovePredator(NetworkMessage message){
			var data = message.ReadMessage<StringMessage>().value;
			var dPos = data.IndexOf('-');
			var cPos = data.IndexOf(':');
			
			var id = int.Parse(data.Substring(0, dPos));
			var x = int.Parse(data.Substring(dPos + 1, cPos - dPos - 1));
			var y = int.Parse(data.Substring(cPos + 1));
			
			//Move the Predator to the desired block
			Predators[id].GetComponent<PredatorController>().MovePredator(x, y);
		}

        /// <summary>
        /// Method to reward key on victory
        /// </summary>
        /// <param name="message">Message Recieved</param>
		private void RewardKey(NetworkMessage message){
	        bool change = true;
	        foreach (var block in Builder.HuntBuilder.Grid){
		        //var mat = block.GetComponent<Renderer>().material;
		        if (change){
			        block.GetComponent<Renderer>().material = WinMats[0];
			        change = !change;
		        }
		        else{
			        block.GetComponent<Renderer>().material = WinMats[1];
			        change = !change;
		        }
	        }
	        foreach (var predator in Predators){
		        Destroy(predator);
	        }
	        Prey.GetComponentsInChildren<Renderer>()[0].material = WinPreyMat;
	        
            GameHUD.AddKey(4);
            StartCoroutine(Builder.SaveEnvironment());
		}

		private void NextTurn(NetworkMessage message){
			var data = message.ReadMessage<IntegerMessage>().value;
			foreach (var predator in Builder.HuntBuilder.Predators){
				predator.GetComponent<PredatorController>().Turn = data;
			}
			Builder.HuntBuilder.Prey.GetComponent<PreyController>().Turn = data;
		}
	}
}

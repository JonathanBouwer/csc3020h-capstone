﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class RoomBuilder : MonoBehaviour {
        public GameObject DecorationPrefab;
        public Material AlternateDecorationMaterial;

        [HideInInspector] public List<GameObject> Decorations;
        [HideInInspector] public int DecorationCount;
        [HideInInspector] public GameObject Maze, Floor, Room, LeavingPlate;
        [HideInInspector] public int Width, Breadth, StartAreaLength;

        private float yoffset, brickWidth;
        private Random.State state;

        /// <summary>
        /// Method to build room around maze
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator BuildRoom() {
            Room = new GameObject("Environment");
            var startArea = CreateStartArea();
            state = Random.state;
            yield return null;
            Random.state = state;
            yield return StartCoroutine(DecorateStartArea(startArea));
            CreateWalls();
            yield return null;

            var mazePos = new Vector3(0, 0, (StartAreaLength + 1) * brickWidth);
            Maze.transform.parent = Room.transform;
            Maze.transform.localPosition = mazePos;

            CreatePlayerSpawns();
            yield return null;
            SetupClue(brickWidth);

            var leavingPlate = Instantiate(LeavingPlate, Room.transform);
            leavingPlate.transform.position = new Vector3 {
                x = (2 * Width - 2) * brickWidth,
                y = 0,
                z = -2 * brickWidth
            };
            leavingPlate.name = "LeavePlate";
        }

        /// <summary>
        /// Method to postion clue correctly
        /// </summary>
        /// <param name="brickWidth">Scaliong parameter of brick width</param>
        public void SetupClue(float brickWidth) {
            var clue = GameObject.Find("Clue");
            var offset = new Vector3(0.3f * Width * brickWidth, 5, -3f);
            clue.transform.position = Maze.transform.position + offset;
        }

        /// <summary>
        /// Method to place player spawns to be used by network spawning
        /// </summary>
        private void CreatePlayerSpawns() {
            var baseSpawn = new Vector3 {
                x = 3 * brickWidth,
                y = 0,
                z = -brickWidth
            };
            var player1Spawn = new GameObject("Player1Spawn");
            player1Spawn.transform.localPosition = baseSpawn - Vector3.forward;
            player1Spawn.transform.parent = Room.transform;
            var player2Spawn = new GameObject("Player2Spawn");
            player2Spawn.transform.localPosition = baseSpawn + Vector3.forward;
            player2Spawn.transform.parent = Room.transform;
        }

        /// <summary>
        /// Method to setup begining area based on width and breadth
        /// </summary>
        /// <returns>Start area game object</returns>
        private GameObject CreateStartArea() {
            var startArea = new GameObject("StartArea");
            startArea.transform.parent = Room.transform;
            var floor = Instantiate(Floor);
            var floorBounds = floor.GetComponent<Collider>().bounds;
            var wallSize = floorBounds.size;
            yoffset = floorBounds.center.y - floorBounds.extents.y;
            brickWidth = floorBounds.size.x;
            for (var i = 0; i < 2 * StartAreaLength + 1; i++) {
                for (var j = 0; j < 2 * Width + 1; j++) {
                    CreateBrick(startArea, floor, new Vector3(j * wallSize.x, yoffset, (i - StartAreaLength) * wallSize.z));
                }
            }
            Destroy(floor);
            return startArea;
        }

        /// <summary>
        /// Helper method to instantiate bricks
        /// </summary>
        /// <param name="parent">Parent object of new brick</param>
        /// <param name="brick">Prefab to generate brick out of</param>
        /// <param name="localPosition">Relative position to parent</param>
        private void CreateBrick(GameObject parent, GameObject brick, Vector3 localPosition) {
            var newWall = Instantiate(brick, parent.transform);
            newWall.transform.localPosition = localPosition;
            newWall.name = "MazeFloor";
        }

        /// <summary>
        /// Method to add decorations to an area
        /// </summary>
        /// <param name="startArea">Parent game object for decorations</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator DecorateStartArea(GameObject startArea) {
            Decorations = new List<GameObject>();
            var startPosition = new Vector3 {
                x = DecorationCount * Width / 1.5f,
                y = 0,
                z = -4 * StartAreaLength + 1
            };
            for (var i = 0; i < DecorationCount; i++) {
                var decoration = Instantiate(DecorationPrefab);
                Decorations.Add(decoration);
                decoration.transform.parent = startArea.transform;
                var offset = new Vector3 {
                    x = decoration.GetComponent<Collider>().bounds.size.x,
                    y = 0,
                    z = 0
                };
                var randomOffset = new Vector3 {
                    x = Random.value / 2f,
                    y = 0,
                    z = Random.value / 2f
                };
                decoration.transform.position = startPosition + randomOffset;
                decoration.name = "Decoration";
                startPosition -= 1.1f * offset;
                state = Random.state;
                yield return null;
                Random.state = state;
            }
        }

        /// <summary>
        /// Method to generate room walls
        /// </summary>
        private void CreateWalls() {
            var walls = new GameObject("Walls");
            walls.transform.parent = Room.transform;
            var floor = Instantiate(Floor);
            var floorBounds = floor.GetComponent<Collider>().bounds;
            var wallSize = floorBounds.size;
            yoffset = floorBounds.center.y - floorBounds.extents.y;
            brickWidth = floorBounds.size.x;
            var _brickHeight = floorBounds.size.y;
            var trueWidth = 2 * Width + 1;
            var trueBreadth = 2 * Breadth + 2;
            var halfBrickWidth = brickWidth / 2;

            var northWall = new GameObject("NorthWall");
            var southWall = new GameObject("SouthWall");
            var eastWall = new GameObject("EastWall");
            var westWall = new GameObject("WestWall");
            northWall.transform.parent = walls.transform;
            southWall.transform.parent = walls.transform;
            eastWall.transform.parent = walls.transform;
            westWall.transform.parent = walls.transform;
            var northZ = (trueBreadth - StartAreaLength) * wallSize.z - halfBrickWidth;
            var southZ = -StartAreaLength * wallSize.z;
            var eastX = brickWidth * trueWidth - halfBrickWidth;
            var westX = -halfBrickWidth;
            var wallHeight = 10;

            for (var j = 0; j < wallHeight; j++) {
                var yOffset = j * _brickHeight;
                for (var i = 0; i < trueWidth; i++) {
                    var xOffset = i * wallSize.x;
                    var northPos = new Vector3(xOffset, yOffset, northZ);
                    CreateBrick(northWall, floor, northPos);
                    var southPos = new Vector3(xOffset, yOffset, southZ);
                    CreateBrick(southWall, floor, southPos);
                    
                }
                for (var i = 0; i < trueBreadth; i++) {
                    var zOffset = (i - StartAreaLength) * wallSize.z;
                    var eastPos = new Vector3(eastX, yOffset, zOffset);
                    CreateBrick(eastWall, floor, eastPos);
                    var westPos = new Vector3 (westX, yOffset, zOffset);
                    CreateBrick(westWall, floor, westPos);
                }
            }

            Destroy(floor);
        }
    }
}

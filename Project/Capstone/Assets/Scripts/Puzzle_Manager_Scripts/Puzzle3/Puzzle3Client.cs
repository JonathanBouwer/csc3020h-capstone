﻿using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
	public class Puzzle3Client : SceneManager {
		public PuzzleBuilder3 Builder;
		public Puzzle3Manager Manager;

		[HideInInspector] public bool Lost;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
			const short type1 = (short)NetworkMessageType.PUZZLE_3_MINE_MARK;
			NetworkManager.client.RegisterHandler(type1, MarkMine);
			const short type2 = (short)NetworkMessageType.PUZZLE_3_BLOCK_OPEN;
			NetworkManager.client.RegisterHandler(type2, OpenBlock);
			const short type3 = (short)NetworkMessageType.PUZZLE_3_SOLVED;
			NetworkManager.client.RegisterHandler(type3, RewardKey);
			const short type4 = (short)NetworkMessageType.PUZZLE_3_LOST;
			NetworkManager.client.RegisterHandler(type4, Gamelost );
			Builder.LevelManager = GameSettings.LevelManager;
			Builder.Manager = this;
			var mapper = new ScenePuzzle3Mapper(Builder);
			Builder.Serializer = new SceneSerializer<SerializedPuzzle3Data>(this, mapper);
			Builder.BuildMap(NetworkManager.NetworkSeed);
			Builder.SetClueIgnore(NetworkManager.IsHost ? "Player1Ignore" : "Player2Ignore");
		}

        /// <summary>
        /// Handler fot network message to open a block
        /// </summary>
        /// <param name="message">Message received</param>
        public void OpenBlock(NetworkMessage message) {
			string data =message.ReadMessage<StringMessage>().value;
			int value = int.Parse(data.Substring (0, 1 ));
			int id=int.Parse(data.Substring(2, data.Length-2));

			GameObject enviroment= GameObject.Find ("Environment");
			BrickController[] bricks=enviroment.GetComponentsInChildren<BrickController>();
			for (int i = 0; i < bricks.Length; i++) {
				if (bricks [i].Id == id) {
					bricks[i].OpenBrick (value);
				}
			}
		}
        
        /// <summary>
        /// Receive message to mark mine, contains an int reperesenting a value
        /// </summary>
        /// <param name="message">Messge recieved</param>
        public void MarkMine(NetworkMessage message){
			int data = message.ReadMessage<IntegerMessage>().value;
			GameObject enviroment= GameObject.Find ("Environment");

			BrickController[] bricks=enviroment.GetComponentsInChildren<BrickController>();
			for (int i = 0; i < bricks.Length; i++) {
				if (bricks [i].Id == data) {
					bricks[i].PlaceMine ();
				}
			}
		}

        /// <summary>
        /// Method to reward key on victory
        /// </summary>
        /// <param name="message">Message Recieved</param>
		private void RewardKey(NetworkMessage message) {
			Manager.GameWon ();
            GameHUD.AddKey(3);
            StartCoroutine(Builder.SaveEnvironment());
		}

        /// <summary>
        /// Method to handle loss
        /// </summary>
        /// <param name="message">Message Recieved</param>
        private void Gamelost(NetworkMessage message){
			Manager.GameLost ();
		}
	}
}

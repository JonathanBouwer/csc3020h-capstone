﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4 {
    public class HuntBuilder : MonoBehaviour {
        [HideInInspector] public GameObject Block;
        [HideInInspector] public int Width, Breadth;
        [HideInInspector] public GameObject HuntField;
        //Grid Objects
        public Material[] GridMat;
        [HideInInspector] public List<Vector2> GridPositions; //Store possible positions for check
        [HideInInspector] public GameObject[,] Grid; //Stores the grid
        //Predator Objects
        public GameObject PredatorObject;
        public Material[] PredatorMaterials;
        [HideInInspector] public GameObject[] Predators = new GameObject[4];
        //Prey Object
        public GameObject PreyObject;
        [HideInInspector] public GameObject Prey;

        private int blockSize = 5;
        
        /// <summary>
        /// Builds grid world and sets up values for host
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Build() {
            HuntField = new GameObject("HuntField");
            var grid = new GameObject("Grid");
                      
            Grid = new GameObject[Width,Breadth];
            
            bool gridA = true;
            for (int i = 0; i < Breadth; i++){
                for (int j = 0; j < Width; j++){
                    GameObject block = CreateBrick(grid, Block, new Vector3(j * blockSize, 0.2f, i * blockSize));
                    GridPositions.Add(new Vector2(j, i));
                    block.name = "Block";
                    if (gridA){
                        block.GetComponent<Renderer>().material = GridMat[0];
                        gridA = false;
                    }
                    else{
                        block.GetComponent<Renderer>().material = GridMat[1];
                        gridA = true;
                    }
                    Grid[j, i] = block;
                }
            }
            
            PopulateField();

            grid.transform.parent = HuntField.transform;
            yield return null;
        }
        
        /// <summary>
        /// Helper method to instantiate bricks with position.
        /// </summary>
        /// <param name="parent">Bricks parent</param>
        /// <param name="brick">Brick prefab</param>
        /// <param name="localPosition">relative position to parent</param>
        /// <returns>Brick Created</returns>
        private GameObject CreateBrick(GameObject parent, GameObject brick, Vector3 localPosition) {
            var block = Instantiate(brick, parent.transform);
            block.transform.localPosition = localPosition;
            return block;
        }

        /// <summary>
        /// Sets the position of the preditors and prey in the game world
        /// </summary>
        private void PopulateField(){
            //Place predators
            var predatorA = Instantiate(PredatorObject, Grid[0, 0].transform);
            predatorA.transform.localPosition = Vector3.zero;
            //predatorA.transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            predatorA.GetComponentsInChildren<Renderer>()[0].material = PredatorMaterials[0];
            predatorA.name = "PredatorP1";
            var predAController = predatorA.GetComponent<PredatorController>();
            predAController.ID = 0;
            predAController.CurrentPos = new Vector2(0, 0);
            Predators[0] = predatorA;
            
            var predatorB = Instantiate(Predators[0], Grid[Width - 1, 0].transform);
            predatorB.transform.localPosition = Vector3.zero;
            //predatorB.transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            predatorB.GetComponentsInChildren<Renderer>()[0].material = PredatorMaterials[1];
            predatorB.name = "PredatorP2";
            var predBController = predatorB.GetComponent<PredatorController>();
            predBController.ID = 2;
            predBController.CurrentPos = new Vector2(Width - 1, 0);
            Predators[2] = predatorB;
            
            var predatorC = Instantiate(Predators[0], Grid[Width-1, Breadth-1].transform);
            predatorC.transform.localPosition = Vector3.zero;
            //predatorC.transform.rotation = Quaternion.Euler(0.0f,-90.0f, 0.0f);
            predatorC.GetComponentsInChildren<Renderer>()[0].material = PredatorMaterials[0];
            predatorC.name = "PredatorP1";
            var predCController = predatorC.GetComponent<PredatorController>();
            predCController.ID = 1;
            predCController.CurrentPos = new Vector2(Width - 1, Breadth - 1);
            Predators[1] = predatorC;
            
            var predatorD = Instantiate(Predators[0], Grid[0, Breadth-1].transform);
            predatorD.transform.localPosition = Vector3.zero;
            //predatorD.transform.rotation = Quaternion.Euler(0.0f,-90.0f, 0.0f);
            predatorD.GetComponentsInChildren<Renderer>()[0].material = PredatorMaterials[1];
            predatorD.name = "PredatorP2";
            var predDController = predatorD.GetComponent<PredatorController>();
            predDController.ID = 3;
            predDController.CurrentPos = new Vector2(0, Breadth - 1);
            Predators[3] = predatorD;
            
            //Place Prey
            Prey = Instantiate(PreyObject, Grid[Width / 2, Breadth / 2].transform);
            Prey.transform.localPosition = Vector3.zero;
            Prey.GetComponent<PreyController>().CurrentPos = new Vector2(Width / 2, Width / 2);
        }
    }
}
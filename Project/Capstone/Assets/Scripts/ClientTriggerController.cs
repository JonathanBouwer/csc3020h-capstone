﻿using Assets.Scripts.Player_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts {
    public class ClientTriggerController : MonoBehaviour {
        public NetworkMessageType Type;
        public string Prefix, Message;
        public bool IncludeNonPlayers;

        private NetworkManager networkManager;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            networkManager = NetworkManager.singleton;
        }

        /// <summary> 
        /// OnTriggerEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player enters a trigger it sends a Network mesage with given parameters
        /// </remarks>
        public void OnTriggerEnter(Collider other) {
            if (ShouldIgnore(other)) return;
            var message = new StringMessage(Prefix + Message);
            networkManager.client.Send((short)Type, message);
        }

        /// <summary> 
        /// OnTriggerExit method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerExit.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player exits a trigger it sends a Network mesage with given parameters
        /// </remarks>
        public void OnTriggerExit(Collider other) {
            if (ShouldIgnore(other)) return;
            var message = new StringMessage(Prefix);
            networkManager.client.Send((short)Type, message);
        }

        /// <summary>
        /// Helper method to decide which components to ignore in a collision
        /// </summary>
        /// <param name="other">Component that hasbeen collided with</param>
        /// <returns>Whether component should be ignored</returns>
        private bool ShouldIgnore(Component other) {
            if (IncludeNonPlayers) return false;
            var player = other.GetComponent<PlayerSetup>();
            return (player == null || !player.isLocalPlayer);
        }
    }
}

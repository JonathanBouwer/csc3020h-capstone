﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.Serialization_Scripts;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Tutorial {
	public class TutorialBuilder : MonoBehaviour {
		public TutorialManager manager;

		public GameObject Room;
		public Material[] material=new Material[3];
		public GameObject[] pad;
		public GameObject[] otherPlayerCyl;



		[HideInInspector] public SavedLevelManager LevelManager;
		[HideInInspector] public SceneManager Manager;
		[HideInInspector] public SceneSerializer<TutorialData> Serializer;
		private const string FileName = "Tutorial.json";

		public IEnumerator BuildMap(int seedIn, bool hostIn){
			int seed;
			int partnerseed;

			manager.mat = material;
			var fileData = LevelManager.LoadLevel (FileName);
			if (string.IsNullOrEmpty (fileData)) {
				if (hostIn) {
					seed = seedIn;
					partnerseed = seedIn + 10;
				} else {
					seed = seedIn + 10;
					partnerseed = seedIn;
				}
				StartCoroutine (Build (seed));
				SetCylinderColors (partnerseed);
			} else {
				StartCoroutine (BuildWon ());
			}

			yield return StartCoroutine(CreatePlayerSpawns ());
			Manager.NotifyDoneLoading();
			Manager.SetActiveUIElements(false,true) ;
			yield return null;

		}
		public IEnumerator Build(int seed){
			int[] OrderCorrect = new int[3];
			Random.InitState (seed);
			for (int i = 0; i < 3; i++) {
				OrderCorrect[i]=Random.Range (0, 3);
			}
			manager.OrderWant = OrderCorrect;
			manager.won = false;

			RandomizePads (seed);
			yield return null;


		}
		public IEnumerator BuildWon(){
			manager.HasWon (false);
			yield return null;

		}
			
		public void SetCylinderColors(int partnerseed){
			Random.InitState (partnerseed);
			for (int i = 0; i < 3; i++) {
				otherPlayerCyl [i].GetComponent<Renderer> ().material = material [Random.Range (0, 3)];
				
			}
		}
		private void RandomizePads(int seed) {
			int[] PadOrderRandom = new int[3];
			ShuffleOrder (PadOrderRandom, seed);
			for (int i = 0; i < 3; i++) {
				PadController padControl = pad [i].GetComponent<PadController> ();
				padControl.SetMaterial (material [PadOrderRandom [i]]);
				padControl.index = PadOrderRandom [i];

			}


		}

		private void ShuffleOrder(int[] ArrayShuf, int seed) {
			for (int i = 0; i < 3; i++) {
				ArrayShuf[i] = i;
			}
			Random.InitState(seed + 30);
			for (int i = 3 - 1; i > 0; i--) {
				int j = Random.Range(0, i);
				int tmp = ArrayShuf[j];
				ArrayShuf[j] = ArrayShuf[i];
				ArrayShuf[i] = tmp;

			}
		}
		private IEnumerator CreatePlayerSpawns() {
			var player1Spawn = new GameObject("Player1Spawn");
			player1Spawn.transform.localPosition = new Vector3(2.8f, 0.3f, 11);
			player1Spawn.transform.parent = Room.transform;
			var player2Spawn = new GameObject("Player2Spawn");
			player2Spawn.transform.localPosition = new Vector3(-2.8f, 0.3f, 11);
            player2Spawn.transform.parent = Room.transform;
			yield return null;
		}
		public IEnumerator SaveEnvironment() {
			GameObject blank = new GameObject ("Blank");
			yield return StartCoroutine(Serializer.SerializeSceneItem(blank.transform));
			LevelManager.SaveLevel(FileName, Serializer.SerializedResult);
		}
		/// <summary>
		/// Changes layer of clue to it ignores the other player
		/// </summary>
		/// <param name="tagToIgnore">Either Player1Ignore or PLayer2Ignore</param>
		public void SetClueIgnore(string tagToIgnore) {
			var clue = GameObject.Find ("Clue");
			var clueController =clue.GetComponentInChildren<ClueController>();
			clueController.IgnoreLayer = tagToIgnore;
		}
	
	

	}
}


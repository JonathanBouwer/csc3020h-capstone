﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts {
    public class Corridor : MonoBehaviour {
        public Tile TilePrefab;
		public GameObject WallPrefab, MovingLight;
		public int lightCountPerCorridor;
		public Color[] LightColours;

        [HideInInspector] public Room[] Rooms = new Room[2];
        [HideInInspector] public float Length;
        [HideInInspector] public List<Triangle> Triangles = new List<Triangle>();
        [HideInInspector] public IntVector2 Coordinates;

        private GameObject tilesObject;
        private GameObject wallsObject;
        private Map map;
        private List<Tile> tiles;

        /// <summary> 
        /// Initialize method to setup map instance.
        /// </summary>
        /// <param name="map">The map instance to be used</param>
        public void Init(Map map) {
            this.map = map;
        }

        /// <summary> 
        /// Generator function used to generate corridors between 2 rooms.
        /// </summary>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator Generate() {
            transform.localPosition *= RoomMapManager.TileSize;
            tilesObject = new GameObject("Tiles");
            tilesObject.transform.parent = transform;
            tilesObject.transform.localPosition = Vector3.zero;
		

            // Seperate Corridor to room
            MoveStickedCorridor();

            tiles = new List<Tile>();
            int start = Rooms[0].Coordinates.x + Rooms[0].Size.x / 2;
            int end = Coordinates.x;
            if (start > end) {
                int temp = start;
                start = end;
                end = temp;
            }
            for (int i = start; i <= end; i++) {
                Tile newTile = CreateTile(new IntVector2(i, Coordinates.z));
                if (newTile) {
                    tiles.Add(newTile);
                }
            }
            start = Rooms[1].Coordinates.z + Rooms[1].Size.z / 2;
            end = Coordinates.z;
            if (start > end) {
                int temp = start;
                start = end;
                end = temp;
            }
            for (int i = start; i <= end; i++) {
                Tile newTile = CreateTile(new IntVector2(Coordinates.x, i));
                if (newTile) {
                    tiles.Add(newTile);
                }
            }
			CreateMovingLights ();
            yield return null;
        }

        /// <summary> 
        /// Debug method to show layout of fake corridors during earlier stage of generation
        /// </summary>
        public void Show() {
            Debug.DrawLine(Rooms[0].transform.localPosition, transform.localPosition, Color.white, 3.5f);
            Debug.DrawLine(transform.localPosition, Rooms[1].transform.localPosition, Color.white, 3.5f);
        }

        /// <summary> 
        /// Internal methof to generate a tile game object
        /// </summary>
        /// <param name="coordinates">Coordinates of tile to be build</param>
        /// <returns>The tile game object generated</returns>
        private Tile CreateTile(IntVector2 coordinates) {
            if (map.GetTileType(coordinates) == TileType.Empty) {
                map.SetTileType(coordinates, TileType.Corridor);
            } else {
                return null;
            }

            Tile newTile = Instantiate(TilePrefab);
            newTile.Coordinates = coordinates;
            newTile.name = "Tile " + coordinates.x + ", " + coordinates.z;
            newTile.transform.parent = tilesObject.transform;
            var tilePosition = new Vector3 {
                x = coordinates.x - Coordinates.x + 0.5f,
                y = 0.0f,
                z = coordinates.z - Coordinates.z + 0.5f
            };
            newTile.transform.localPosition = RoomMapManager.TileSize * tilePosition;
            return newTile;
        }

        /// <summary> 
        /// Helper method to ensure corridoors aren't stuck to corners
        /// </summary>
        private void MoveStickedCorridor() {
            IntVector2 correction = new IntVector2(0, 0);

            if (Rooms[0].Coordinates.x == Coordinates.x + 1) {
                // left 2
                correction.x = 2;
            } else if (Rooms[0].Coordinates.x + Rooms[0].Size.x == Coordinates.x) {
                // right 2
                correction.x = -2;
            } else if (Rooms[0].Coordinates.x == Coordinates.x) {
                // left
                correction.x = 1;
            } else if (Rooms[0].Coordinates.x + Rooms[0].Size.x == Coordinates.x + 1) {
                // right
                correction.x = -1;
            }


            if (Rooms[1].Coordinates.z == Coordinates.z + 1) {
                // Bottom 2
                correction.z = 2;
            } else if (Rooms[1].Coordinates.z + Rooms[1].Size.z == Coordinates.z) {
                // Top 2
                correction.z = -2;
            } else if (Rooms[1].Coordinates.z == Coordinates.z) {
                // Bottom
                correction.z = 1;
            } else if (Rooms[1].Coordinates.z + Rooms[1].Size.z == Coordinates.z + 1) {
                // Top
                correction.z = -1;
            }

            Coordinates += correction;
            transform.localPosition += RoomMapManager.TileSize * new Vector3(correction.x, 0f, correction.z);
        }

        /// <summary> 
        /// Generator function used to generate walls for the corridor
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator CreateWalls() {
            wallsObject = new GameObject("Walls");
            wallsObject.transform.parent = transform;
            wallsObject.transform.localPosition = Vector3.zero;

            foreach (Tile tile in tiles) {
                foreach (MapDirection direction in MapDirections.Directions) {
                    IntVector2 coordinates = tile.Coordinates + direction.ToIntVector2();
                    if (map.GetTileType(coordinates) != TileType.Wall) continue;

                    GameObject newWall = Instantiate(WallPrefab);
                    newWall.name = "Wall (" + coordinates.x + ", " + coordinates.z + ")";
                    newWall.transform.parent = wallsObject.transform;
                    newWall.transform.localPosition = RoomMapManager.TileSize * map.CoordinatesToPosition(coordinates) - transform.localPosition;
                    newWall.transform.localRotation = direction.ToRotation();
                }
            }
            yield return null;
        }
	
		public void CreateMovingLights() {

			GameObject lightObject = new GameObject("Moving Lights");
			lightObject.transform.parent = transform;
			lightObject.transform.localPosition = Vector3.zero;
			Vector3 positionLight = new Vector3 (5, 25, 5);
			for (int i = 0; i < lightCountPerCorridor; i++) {
				GameObject mlight = Instantiate (MovingLight);
				mlight.transform.parent = lightObject.transform;
				mlight.transform.localPosition = positionLight;
				mlight.GetComponentInChildren<Light> ().color = LightColours [Random.Range (0, LightColours.Length)];
			}

		} 

    }
}

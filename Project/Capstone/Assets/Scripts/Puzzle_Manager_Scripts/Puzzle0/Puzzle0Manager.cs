﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class Puzzle0Manager : NetworkBehaviour {
        private int progress;

        /// <summary>
        /// Initilizer for progress
        /// </summary>
        /// <param name="count">Number of plates</param>
        public void SetPairCount(int count) {
            progress = count;
        }

        /// <summary>
        /// Method to send destroy pair messages to all clients
        /// </summary>
        /// <param name="pairIndex">Pair to destroy</param>
        public void DestroyPair(int pairIndex) {
            const short type = (short)NetworkMessageType.PUZZLE_0_DESTROY_PLATE_PAIR;
            var message = new StringMessage("Pair" + pairIndex);
            NetworkServer.SendToAll(type, message);

            progress--;
            if (progress > 0) return;

            const short solvedType = (short)NetworkMessageType.PUZZLE_0_SOLVED;
            NetworkServer.SendToAll(solvedType, new EmptyMessage());
        }
    }
}
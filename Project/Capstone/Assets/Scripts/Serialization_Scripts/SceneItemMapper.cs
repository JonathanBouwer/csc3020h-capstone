﻿using System;
using UnityEngine;

namespace Assets.Scripts.Serialization_Scripts {
    public abstract class SceneItemMapper<T> where T : SerializedSceneItem {
        public abstract T MapItem(Transform item);
        public abstract GameObject UnmapItem(T item);

        public static TCsv[] ParseCsv<TCsv>(string s) {
            var vals = s.Split(',');
            var result = new TCsv[vals.Length];
            for (var i = 0; i < vals.Length; i++) {
                result[i] = (TCsv)Convert.ChangeType(vals[i], typeof(TCsv));
            }
            return result;
        }
    }
}

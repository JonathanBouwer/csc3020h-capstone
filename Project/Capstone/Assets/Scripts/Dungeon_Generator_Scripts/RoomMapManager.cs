﻿using System.Collections;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts{
	public class RoomMapManager : MonoBehaviour{
		public Map MapPrefab;
	    public int MapSizeX;
	    public int MapSizeZ;
	    public int MaxRooms;
	    public int MinRoomSize;
	    public int MaxRoomSize;
	    public int TileSizeFactor;
	    public static int TileSize;
	    public int TotalPuzzleCount;

	    [HideInInspector] public SceneManager Manager;
	    [HideInInspector] public SavedLevelManager LevelManager;
	    [HideInInspector] public SceneSerializer<SerializedMapData> Serializer;

	    private Map mapInstance;
	    private const string Filename = "Map.json";

        /// <summary>
        /// Method to generate the map in it's entirity or reload from file if it exists
        /// </summary>
        /// <param name="seed">Seed of the map, ensure this is shared between networked clients</param>
	    public void GenerateMap(int seed) {
	        var levelData = LevelManager.LoadLevel(Filename);
	        if (string.IsNullOrEmpty(levelData)) {
	            Generate(seed);
	        } else {
                StartCoroutine(StartGeneration(levelData));
            }
	    }

        /// <summary>
        /// Method to generate the map in it's entirity
        /// </summary>
        /// <param name="seed">Seed of the map, ensure this is shared between networked clients</param>
	    private void Generate(int seed) {
	        mapInstance = Instantiate(MapPrefab);
	        mapInstance.name = "Map";
	        mapInstance.RoomCount = Mathf.Min(MaxRooms, TotalPuzzleCount);
	        mapInstance.MaxPlacementAttempts = mapInstance.RoomCount * mapInstance.RoomCount;
	        mapInstance.MapSize = new IntVector2(MapSizeX, MapSizeZ);
	        mapInstance.RoomSize.Min = MinRoomSize;
	        mapInstance.RoomSize.Max = MaxRoomSize;
	        TileSize = TileSizeFactor;
	        mapInstance.TotalPuzzleCount = TotalPuzzleCount;
	        StartCoroutine(StartGeneration(seed));
	    }

        /// <summary>
        /// Internal method to generate the map.
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
	    private IEnumerator StartGeneration(int seed) {
	        yield return StartCoroutine(mapInstance.Generate(seed));
            ColorSolvedPuzzles();
            yield return StartCoroutine(Serializer.SerializeSceneItem(mapInstance.transform));
	        LevelManager.SaveLevel(Filename, Serializer.SerializedResult);
	        Manager.NotifyDoneLoading();
	    }

	    /// <summary>
        /// Internal method to load map from file.
        /// </summary>
        /// <param name="levelData">Level filename</param>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator StartGeneration(string levelData) {
            yield return StartCoroutine(Serializer.DeserializeSceneItem(levelData));
            ColorSolvedPuzzles();
	        SetNewSpawn();
	        yield return null;
            Manager.NotifyDoneLoading();
        }

        /// <summary>
        /// Simple method to find all solved puzzles and make their particle systems green
        /// </summary>
	    private void ColorSolvedPuzzles() {
            var particleSystems = FindObjectsOfType<ParticleSystem>();
            var solvedPuzzles = Manager.GetSolvedPuzzles();
            var gradient = new Gradient {
                colorKeys = new[] {
                    new GradientColorKey(Color.yellow, 0.0f),
                    new GradientColorKey(Color.white, 1.0f)
                },
                alphaKeys = new[] {
                    new GradientAlphaKey(0.0f, 0.0f),
                    new GradientAlphaKey(1.0f, 0.22f),
                    new GradientAlphaKey(0.0f, 1.0f),
                }
            };
            foreach (var system in particleSystems) {
                if (system.name != "Fog") continue;

                var room = system.GetComponentInParent<Room>();
                if (!solvedPuzzles.Contains(room.PuzzleId)) continue;
                var temp = system.main;
                temp.startColor = new ParticleSystem.MinMaxGradient(gradient);
            }
        }


        private void SetNewSpawn() {
            var baseSpawn = Manager.GetBaseSpawnPos();
            var player1Spawn = GameObject.Find("Player1Spawn");
            player1Spawn.transform.position = baseSpawn + new Vector3(10, 0, 10);
            var player2Spawn = GameObject.Find("Player2Spawn");
            player2Spawn.transform.position = baseSpawn + new Vector3(-10, 0, 10);
        }
	}
}
﻿using System.Collections;
using Assets.Scripts.UI_Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Assets.Scripts.Menu_Scripts {
    public class ConnectButton : MonoBehaviour {
        public GameNetworkManager GameNetworkManager;
        public TextMeshProUGUI ButtonText;
        public ErrorText ErrorText;
        public ValidateIP Validator;
        public Button HostButton;

        private Button button;
        private const float Wait = 0.2f;
        private bool connecting, disabled;

        /// <summary> 
        /// Start method to initialize class variables right before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            button = GetComponent<Button>();
            connecting = false;
        }

        /// <summary>
        /// UI Callback method to connect to the server, animates UI during connection
        /// </summary>
        public void Connect() {
            GameNetworkManager.GameStartClient();
            GameNetworkManager.client.RegisterHandler(MsgType.Disconnect, ConnectionError);
            ButtonText.alignment = TextAlignmentOptions.Left;
            StartCoroutine(LoopDots());
            button.interactable = false;
            HostButton.interactable = false;
            connecting = true;
        }
        
        /// <summary>
        /// Tests all valid enable conditions and enables button if allowed
        /// </summary>
        public void Enable() {
            button.interactable = Validator.ContainsValidIp && !connecting && !disabled;
        }

        /// <summary>
        /// Sets the disabled state for the button
        /// </summary>
        public void Disable() {
            button.interactable = false;
            disabled = true;
        }

        /// <summary>
        /// Unsets the disabled state ffor the button
        /// </summary>
        public void Undisable() {
            disabled = false;
            Enable();
        }

        /// <summary>
        /// Callback method for connection error
        /// </summary>
        /// <param name="netMsg">Connection Error Messsage</param>
        private void ConnectionError(NetworkMessage netMsg) {
            ErrorText.FadeInOut("Connection to server failed");
            StopAllCoroutines();
            ButtonText.alignment = TextAlignmentOptions.Center;
            connecting = false;
            Enable();
            HostButton.interactable = true;
            ButtonText.text = "Connect";
        }

        /// <summary>
        /// Simple method to loop 3 dots after text
        /// </summary>
        private IEnumerator LoopDots() {
            var n = 0;
            while (true) {
                ButtonText.text = "Connecting" + new string('.', n) + new string(' ', 3 - n);
                n = (n + 1) % 4;
                yield return new WaitForSeconds(Wait);
            }
        }
    }
}

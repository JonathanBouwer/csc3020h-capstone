﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    public class Puzzle2PlateTrigger : ClientTriggerController {
        private Renderer texture;
        private bool isLit;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            texture = GetComponent<Renderer>();
            isLit = false;
        }

        /// <summary> 
        /// OnTriggerEnter method to detect player entering bounding box.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerEnter.html">Unity Documentation</see> for more info
        /// </summary>
        /// <param name="other">Collision object</param>
        /// <remarks>
        /// When player enters a trigger calls base trigger enter and lights up
        /// </remarks>
        new void OnTriggerEnter(Collider other) {
            base.OnTriggerEnter(other);
            LightUp();
        }

        /// <summary>
        /// Starts a coroutine to gradualty brighten texture
        /// </summary>
        public void LightUp() {
            if (isLit) return;
            StartCoroutine(TransitionEmission(0, 0.3f));
            isLit = true;
        }

        /// <summary>
        /// Starts a coroutine to gradualty darken texture
        /// </summary>
        public void LightDown() {
            if (!isLit) return;
            StartCoroutine(TransitionEmission(0.3f, 0));
            isLit = false;
        }

        /// <summary>
        /// Transitions brightness between values linearly over 30 steps
        /// </summary>
        /// <param name="f1">Start</param>
        /// <param name="f2">End</param>
        private IEnumerator TransitionEmission(float f1, float f2) {
            for (var i = 0; i < 30; i++) {
                var value = Mathf.Lerp(f1, f2, i / 30.0f);
                texture.material.SetColor("_EmissionColor", new Color(value, value, value));
                yield return null;
            }
        }
    }
}

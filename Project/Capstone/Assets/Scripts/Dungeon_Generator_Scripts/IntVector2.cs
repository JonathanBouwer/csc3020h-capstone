﻿using UnityEngine;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts {
    [System.Serializable]
    public struct IntVector2 {
        public int x, z;

        /// <summary> 
        /// Constructor for IntVector2
        /// </summary>
        /// <param name="x">X coordinate of vector</param>
        /// <param name="z">Z coordinate of vector</param>
		public IntVector2(int x, int z) {
            this.x = x;
            this.z = z;
        }

        /// <summary> 
        /// Componentwise addition operator for IntVector2
        /// </summary>
		public static IntVector2 operator +(IntVector2 a, IntVector2 b) {
            a.x += b.x;
            a.z += b.z;
            return a;
        }

        /// <summary> 
        /// Componentwise subtraction operator for IntVector2
        /// </summary>
		public static IntVector2 operator -(IntVector2 a, IntVector2 b) {
            a.x -= b.x;
            a.z -= b.z;
            return a;
        }

        /// <summary> 
        /// Componentwise addition operator for Vector3 and IntVector2
        /// </summary>
		public static Vector3 operator +(Vector3 a, IntVector2 b) {
            a.x += b.x;
            a.z += b.z;
            return a;
        }
    }
}
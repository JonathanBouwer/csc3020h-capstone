﻿using UnityEngine;

public class IntroBackgroundAnimations : MonoBehaviour {

	public GameObject Ship, Nova, Bloom;
	public float TimePreExplosion;
	public ParticleSystem explosion;
	private bool displayScene1;
	private bool displayScene2;

	public Vector3 ShipPos;
	public float speed;
	void Start(){
		displayScene1 = false;
		displayScene2 = false;
	}
	void Update(){
		if (displayScene1 && Ship.transform.localPosition == ShipPos) {//reached position

			displayScene1 = false;
		} 
		if (displayScene1 && !displayScene2) {//move towards slowly
			Ship.transform.localPosition = Vector3.MoveTowards (Ship.transform.localPosition, ShipPos, speed * Time.deltaTime);
		} else if (displayScene1 && displayScene2) {//need toput ship in position fast
			Ship.transform.localPosition = Vector3.MoveTowards (Ship.transform.localPosition, ShipPos, speed * 10 * Time.deltaTime);
		} else if (displayScene2 && !displayScene1) {//in place and therefore start explosion.
			displayScene2 = false;
			Invoke ("Explosion", TimePreExplosion);
		}

	}
	public void Display1(){
		Ship.SetActive (true);
		displayScene1 = true;
	}
	public void Display2(){
		displayScene2 = true;
	}
	public void Explosion(){
		Nova.SetActive (true);
		Bloom.SetActive (true);
		Instantiate (explosion);
		explosion.Play ();
		Ship.SetActive (false);

	}

}

﻿using UnityEngine;

public class LightRotator : MonoBehaviour {


	public float Rotationspeed;
	public GameObject[] lights;
	// Update is called once per frame
	void Update () {
		for(int i =0;i<lights.Length;i++){
			lights[i].transform.Rotate (0,10*Rotationspeed * Time.deltaTime ,0,Space.World);
		}
	}

}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Serialization_Scripts {
    public class SceneSerializer<T> where T : SerializedSceneItem {
        public MonoBehaviour Controller;
        public SceneItemMapper<T> Mapper;
        public string SerializedResult;
        public int SerializationRate = 5;

        [Serializable]
        public struct SerializedScene {
            public List<T> Scene;
        }

        public SceneSerializer(MonoBehaviour controller, SceneItemMapper<T> mapper) {
            Controller = controller;
            Mapper = mapper;
        }

        public IEnumerator SerializeSceneItem(Transform item) {
            var data = new List<T>();
            yield return Controller.StartCoroutine(Serialize(item, data, null));
            var result = new SerializedScene {Scene = data};
            SerializedResult = JsonUtility.ToJson(result);
        }

        private IEnumerator Serialize(Transform item, List<T> data, T parent) {
            var result = Mapper.MapItem(item);
			if (result == null)
				yield break;
            result.Id = data.Count;
            result.ChildrenIds = new List<int>();
            if(parent != null) {
                parent.ChildrenIds.Add(data.Count);
            }
            data.Add(result);
            var count = 0;
            foreach (Transform child in item) {
                if (count >= SerializationRate) {
                    count = 0;
                    yield return Controller.StartCoroutine(Serialize(child, data, result));
                } else {
                    count++;
                    var step = Serialize(child, data, result);
                    while (step.MoveNext()) {
                        // Perform Serialization
                    }
                }
            }
        }

        public IEnumerator DeserializeSceneItem(string data) {
            var items = JsonUtility.FromJson<SerializedScene>(data);
            yield return Controller.StartCoroutine(Deserialize(0, items.Scene, null));
        }

        private IEnumerator Deserialize(int index, List<T> data, Transform parent) {
            var sceneItem = data[index];
            var result = Mapper.UnmapItem(sceneItem);
            if (parent != null) {
                result.transform.parent = parent;
            }
            var count = 0;
            foreach (var childIndex in sceneItem.ChildrenIds) {
                if (count >= SerializationRate) {
                    count = 0;
                    yield return Controller.StartCoroutine(Deserialize(childIndex, data, result.transform));
                } else {
                    count++;
                    var step = Deserialize(childIndex, data, result.transform);
                    while (step.MoveNext()) {
                        // Perform Deserialization
                    }
                }
            }
        }
    }
}

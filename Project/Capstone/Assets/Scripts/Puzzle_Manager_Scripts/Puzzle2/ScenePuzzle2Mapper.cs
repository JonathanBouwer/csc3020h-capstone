﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    /// <summary>
    /// Class to help serialize puzzle 2
    /// </summary>
    [Serializable]
    public class SerializedPuzzle2Data : SerializedSceneItem {
        public bool Solved;
    }

    class ScenePuzzle2Mapper : SceneItemMapper<SerializedPuzzle2Data> { 

        /// <summary>
        /// Customized map method for SerializedPuzzle2Data
        /// </summary>
        /// <param name="item">Item named "Solved" or "Unsolved" indicating nature of puzzle</param>
        /// <returns>SerializedPuzzle2Data version of item</returns>
        public override SerializedPuzzle2Data MapItem(Transform item) {
            return new SerializedPuzzle2Data {
                Solved = item.name == "Solved"
            };
        }

        /// <summary>
        /// Method to unmap SerializedPuzzle2Data item
        /// </summary>
        /// <param name="item">Item to unmap</param>
        /// <returns>GameObject named "Solved" or "Unsolved" indicating nature of puzzle</returns>
        public override GameObject UnmapItem(SerializedPuzzle2Data item) {
            return new GameObject(item.Solved ? "Solved" : "Unsolved");
        }
    }
}
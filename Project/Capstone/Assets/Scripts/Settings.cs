﻿using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    [System.Serializable]
    public struct SettingsData {
        public int Port;
        public float Volume;
    }

    public class Settings : MonoBehaviour {
        [HideInInspector] public SettingsData GameSettings;
        [HideInInspector] public SavedLevelManager LevelManager;

        private string configFilePath;
        private AudioSource[] audioSources;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            DontDestroyOnLoad(transform.gameObject);
            LevelManager = new SavedLevelManager(Application.dataPath + "/LevelData");
            configFilePath = Application.dataPath + "/GameSettings.json";
            GameSettings = new SettingsData {
                Port = 7777,
                Volume = 1.0f
            };
        }

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            ResetAudioSources();
            SetupGameSettings();
            UpdateAllAudioSourceVolumes();
        }
		
        /// <summary>
        /// Method called on application exit. Cleans level directory
        /// </summary>
        void OnApplicationQuit() {
            LevelManager.CleanLevelDirectory();
            var settingsData = JsonUtility.ToJson(GameSettings);
            File.WriteAllText(configFilePath, settingsData);
        }
        
        /// <summary>
        /// Sets window to full screen if passed 0 or windowed otherwise
        /// </summary>
        public void SetWindowType(int fullscreen) {
            Screen.fullScreen = fullscreen == 0;
        }

        /// <summary>
        /// UI Callback metyhods to change the volume dynamically
        /// </summary>
        /// <param name="value">New Volume (between 0.0 and 1.0)</param>
        public void ChangeVolume(float value) {
            GameSettings.Volume = value;
            UpdateAllAudioSourceVolumes();
        }

        /// <summary>
        /// Method to find and save a local refrence to all audio sources in scene
        /// </summary>
        public void ResetAudioSources() {
            audioSources = FindObjectsOfType<AudioSource>();
        }

        /// <summary>
        /// Method to update all known audio source volumes
        /// </summary>
        public void UpdateAllAudioSourceVolumes() {
            foreach (var audioSource in audioSources) {
                audioSource.volume = GameSettings.Volume;
            }
        }

        /// <summary>
        /// Sets window resolution by index
        /// </summary>
        /// <param name="resolution">Index of resolution in list</param>
        public void SetWindowResoltion(int resolution) {
            switch (resolution) {
                case 0:
                    Screen.SetResolution(800, 600, Screen.fullScreen);
                    break;
                case 1:
                    Screen.SetResolution(1024, 768, Screen.fullScreen);
                    break;
                case 2:
                    Screen.SetResolution(1280, 720, Screen.fullScreen);
                    break;
                case 3:
                    Screen.SetResolution(1280, 800, Screen.fullScreen);
                    break;
                case 4:
                    Screen.SetResolution(1280, 1024, Screen.fullScreen);
                    break;
                case 5:
                    Screen.SetResolution(1366, 768, Screen.fullScreen);
                    break;
                case 6:
                    Screen.SetResolution(1600, 900, Screen.fullScreen);
                    break;
                case 7:
                    Screen.SetResolution(1920, 1080, Screen.fullScreen);
                    break;
            }
        }

        /// <summary>
        /// Method to determin window resolution and set UI to that before game starts
        /// </summary>
        /// <returns> Index of matching resolution or else 0 (800x600)</returns>
        private int GetWindowResolutionIndex() {
            if (Screen.width == 1024 && Screen.height == 768) {
                return 1;
            }
            if (Screen.width == 1280 && Screen.height == 720) {
                return 2;
            }
            if (Screen.width == 1280 && Screen.height == 800) {
                return 3;
            }
            if (Screen.width == 1280 && Screen.height == 1024) {
                return 4;
            }
            if (Screen.width == 1366 && Screen.height == 768) {
                return 5;
            }
            if (Screen.width == 1600 && Screen.height == 900) {
                return 6;
            }
            if (Screen.width == 1920 && Screen.height == 1080) {
                return 7;
            }
            return 0;
        }
        
        /// <summary>
        /// Helper meethod to initialize game settigns
        /// </summary>
        private void SetupGameSettings() {
            var fileData = "{\"Port\":7777,\"Volume\":1.0}";
            try {
                fileData = File.ReadAllText(configFilePath);
            } catch (Exception) {
                print("Could not find settings file at " + configFilePath + ", Defaulting to: " + fileData);
            }
            GameSettings = JsonUtility.FromJson<SettingsData>(fileData);

            var windowDropdown = GameObject.Find("WindowSetting").GetComponentInChildren<TMP_Dropdown>();
            windowDropdown.value = Screen.fullScreen ? 0 : 1;

            var resolutionDropdown = GameObject.Find("ResolutionSetting").GetComponentInChildren<TMP_Dropdown>();
            resolutionDropdown.value = GetWindowResolutionIndex();

            var volumeScrollbar = GameObject.Find("MasterVolume").GetComponentInChildren<Scrollbar>();
            volumeScrollbar.value = GameSettings.Volume;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

namespace Assets.Scripts.UI_Scripts {
    public class GameHudController : MonoBehaviour {
        public GameObject MiniMap, Chat, Subtitle, LoadingScreen, Keys;
        public Animator VictoryAnimator, CallibratingAnimator;

        public int NumKeys;
        public float AnimationDuration;

        [HideInInspector] public bool MinimapActive, ChatActive;
        [HideInInspector] public List<int> AquiredKeys;
        [HideInInspector] public Vector3 MapBaseSpawnPosition;

        private TextMeshProUGUI loadingText;
        private int numDots;
        private GameObject[] keys;
        private bool animatingKey;
        private NetworkManager networkManager;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            DontDestroyOnLoad(this);
        }

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            loadingText = LoadingScreen.GetComponentInChildren<TextMeshProUGUI>();
            MinimapActive = true;
            ChatActive = true;
            AquiredKeys = new List<int>();
            SetupKeys();
            SetupNetworking();
        }

        /// <summary>
        /// Method to assign refrence to network manager and set up handlers
        /// </summary>
        private void SetupNetworking() {
            networkManager = NetworkManager.singleton;
            var type = (short) NetworkMessageType.DO_KEY_TEST;
            networkManager.client.RegisterHandler(type, TestKeys);
        }

        /// <summary>
        /// Simple method to set up key sprites for the UI
        /// </summary>
        private void SetupKeys() {
            var keyPrefab = Keys.transform.GetChild(0).gameObject;
            keys = new GameObject[NumKeys];
            for (var i = 0; i < NumKeys; i++) {
                var newKey = Instantiate(keyPrefab, Keys.transform);
                var rectTransform = newKey.GetComponent<RectTransform>();
                newKey.name = "Key" + i;
                rectTransform.anchoredPosition = new Vector3((52 + 10) * i + 26, 0);
                keys[i] = newKey;
            }
            Destroy(keyPrefab);
        }

        /// <summary>
        /// Public interface to play the showing keys animation
        /// </summary>
        /// <remarks>
        /// 40 is a magic number related to the UI size, It would be nice to use Unity's animation system
        /// to avoid this but it doesn't support dynamic animation editing at runtime.
        /// </remarks>
        public void ShowKeys() {
            StartCoroutine(SlideKeysAnimation(-40));
        }

        /// <summary>
        /// Public interface to play the hiding keys animation
        /// </summary>
        /// <remarks>
        /// -40 is a magic number related to the UI size, It would be nice to use Unity's animation system
        /// to avoid this but it doesn't support dynamic animation editing at runtime.
        /// </remarks>
        public void HideKeys() {
            StartCoroutine(SlideKeysAnimation(40));
        }

        /// <summary>
        /// Method to animate the keys sliding in from the top
        /// </summary>
        /// <param name="goalY">Ending y position</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator SlideKeysAnimation(float goalY) {
            var ticks = AnimationDuration / Time.deltaTime;
            var rectTransform = Keys.GetComponent<RectTransform>();
            var goal = rectTransform.anchoredPosition;
            goal.y = goalY;
            for (var i = 0; i < ticks; i++) {
                rectTransform.anchoredPosition = Vector3.Lerp(rectTransform.anchoredPosition, goal, i / ticks);
                yield return null;
            }
            rectTransform.anchoredPosition = goal;
            yield return null;
        }

        /// <summary>
        /// Public interface to activate/add a key to the players
        /// </summary>
        /// <param name="keyId">
        /// Key to activate, probably use your puzzle number
        /// </param>
        public void AddKey(int keyId) {
            AquiredKeys.Add(keyId);
            StartCoroutine(AddKeyAnimation(keyId));
            VictoryAnimator.SetTrigger("Start");
        }

        /// <summary>
        /// Method to animate the keys sliding in from the top, key being aquired to pulse and activate
        /// and then slide back out.
        /// </summary>
        /// <param name="keyId">Key to activate</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator AddKeyAnimation(int keyId) {
            animatingKey = true;
            yield return StartCoroutine(SlideKeysAnimation(-40));
            yield return new WaitForSeconds(0.1f);
            yield return StartCoroutine(FadeKeyAnimation(keyId));
            yield return new WaitForSeconds(0.1f);
            yield return StartCoroutine(SlideKeysAnimation(40));
            animatingKey = false;
        }

        /// <summary>
        /// Transitions a key from being greyed out to full colour
        /// </summary>
        /// <param name="keyId">Key to fade in</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator FadeKeyAnimation(int keyId) {
            var keyTransform = keys[keyId].transform;
            var on = keyTransform.Find("On").GetComponent<Image>();
            var off = keyTransform.Find("Off").GetComponent<Image>();
            var ticks = AnimationDuration / 2 / Time.deltaTime;
            Color onColor = on.color, offColor = off.color;
            for (var i = 0; i < ticks; i++) {
                onColor.a = Mathf.Lerp(0, 1, i / ticks);
                on.color = onColor;
                offColor.a = 1 - onColor.a;
                off.color = offColor;
                var perc = Mathf.Max(Mathf.Sin(Mathf.Lerp(-0.1f, Mathf.PI+0.1f, i / ticks)), 0);
                keyTransform.localScale = Vector3.one * Mathf.Lerp(1, 1.2f, perc);
                yield return null;
            }
            onColor.a = 1;
            on.color = onColor;
            offColor.a = 0;
            off.color = offColor;
            keyTransform.localScale = Vector3.one;
            yield return null;
        }

        /// <summary>
        /// Function to indicate whether player has aquired all keys.
        /// Also plays animation showing the test but will return immediately.
        /// </summary>
        /// <returns>
        /// Aquired Keys > Total Keys 
        /// </returns>
        public void TestKeys(NetworkMessage message) {
            if(!animatingKey) StartCoroutine(TestKeysAnimation());
        }

        /// <summary>
        /// Simple animation to show keys being tested from left to right,
        /// Pulses size on sucess and red on fail.
        /// </summary>
        private IEnumerator TestKeysAnimation() {
            animatingKey = true;
            yield return StartCoroutine(SlideKeysAnimation(-40));
            for (var i = 0; i < NumKeys; i++) {
                if (AquiredKeys.Contains(i)) {
                    yield return StartCoroutine(KeyExistsAnimation(i));
                } else {
                    yield return StartCoroutine(KeyMissingAnimation(i));
                    animatingKey = false;
                    yield break;
                }
            }
            var type = (short) NetworkMessageType.KEY_TEST_SUCESS;
            networkManager.client.Send(type, new EmptyMessage());
            animatingKey = false;
        }

        /// <summary>
        /// Simple method to pulse a key's size
        /// </summary>
        /// <param name="keyId">Key to animate</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator KeyExistsAnimation(int keyId) {
            var keyTransform = keys[keyId].transform;
            var ticks = AnimationDuration / 2 / Time.deltaTime;
            for (var i = 0; i < ticks; i++) {
                var perc = Mathf.Max(Mathf.Sin(Mathf.Lerp(-0.1f, Mathf.PI + 0.1f, i / ticks)), 0);
                keyTransform.localScale = Vector3.one * Mathf.Lerp(1, 1.2f, perc);
                yield return null;
            }
            keyTransform.localScale = Vector3.one;
            yield return null;
        }

        /// <summary>
        /// Simple method to pulse a key to red to indicate missing
        /// </summary>
        /// <param name="keyId">Key to animate</param>
        /// <remarks>
        /// This will be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator KeyMissingAnimation(int keyId) {
            var keyTransform = keys[keyId].transform;
            var off = keyTransform.Find("Off").GetComponent<Image>();
            var ticks = AnimationDuration / 2 / Time.deltaTime;
            for (var i = 0; i < ticks; i++) {
                var perc = 0.5f * (Mathf.Sin(Mathf.Lerp(0, 4 * Mathf.PI, i / ticks)) + 1);
                off.color = Color.Lerp(Color.white, Color.red, perc);
                yield return null;
            }
            off.color = Color.white;
            yield return null;
        }

        /// <summary>
        /// Generator method to do loading animation
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        private IEnumerator LoadingAnimation() {
            while (true) {
                numDots = (numDots + 1) % 4;
                var text = "Loading".PadRight(7 + numDots, '.');
                SetText(text);
                yield return new WaitForSeconds(0.1f);
            }
        }

        /// <summary>
        /// Changes the loading state affecting the hud and active coroutines
        /// </summary>
        public void SetLoading(bool loading) {
            SetHudActive(!loading);
            LoadingScreen.SetActive(loading);
            if (loading) StartCoroutine(LoadingAnimation());
            else StopAllCoroutines();
        }

        /// <summary>
        /// Updates UI to say waiting
        /// </summary>
        public void SetWaiting() {
            StopAllCoroutines();
            SetText("Waiting for Partner");
        }

        /// <summary>
        /// Helper method to update UI text
        /// </summary>
        /// <param name="text">New text to set UI to</param>
        public void SetText(string text) {
            loadingText.text = text;
        }

        /// <summary>
        /// Activates/Deactivates HUD elemenst
        /// </summary>
        /// <param name="active">Active state</param>
        public void SetHudActive(bool active) {
            MiniMap.SetActive(active & MinimapActive);
            Chat.SetActive(active & ChatActive);
        }

        /// <summary>
        /// Sets animation for callibration
        /// </summary>
        /// <param name="callibrating">Whether calibrating or not</param>
        public void SetCallibrating(bool callibrating) {
            CallibratingAnimator.SetBool("Pulsing", callibrating);
        }
    }
}

﻿using UnityEngine;
using TMPro;

namespace Assets.Scripts.CutScene_Scripts {
    public class IntroPrintText : MonoBehaviour {
        public TextMeshProUGUI IntroTextContainer;
        public TextAsset FullText;
        public GameObject Hint;
        public float Speed;
        public float HintSpeed;
		public IntroBackgroundAnimations background;

        private string[] paragraphs;
        private int paragraphCount;
        private int letterCount;
        private float time;
        private bool timeToDisplay;
        private bool displayTip;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            IntroTextContainer.SetText("");
            displayTip = false;
            paragraphCount = 0;
            letterCount = 0;
            paragraphs = FullText.text.Split('*');
            time = Time.fixedTime + Speed;
            NextButtonPushed();
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will attempt to update the text on screen every <code>Speed</code> seconds.
        /// After the current line is finished it will wait for the user to interact before chaging the test to the next line.
        /// If the user waits longer than <code>HintSpeed</code> it will show a prompt to interact.
        /// </remarks>
        void Update() {
            if (Time.fixedTime >= time && paragraphCount < paragraphs.Length) {
                if (letterCount <= paragraphs[paragraphCount].Length) {
                    IntroTextContainer.SetText(paragraphs[paragraphCount].Substring(0, letterCount));
                    letterCount++;
                } else {
                    if(paragraphCount == paragraphs.Length - 1) {
                        GoToMenuScene();
                    }
                    if (displayTip == false) {
                        if (timeToDisplay == false) {
                            time = Time.fixedTime + HintSpeed;
                            timeToDisplay = true;
                        } else {
                            displayTip = true;
                            Hint.SetActive(true);
                        }                       
                    }
                }
                time = Time.fixedTime + Speed;
            }
            if (Input.anyKeyDown) {
                NextButtonPushed();
            }
        }

        /// <summary> 
        /// Class method to reset current text and change paragraph.
        /// </summary> 
        private void NextButtonPushed() {
            if (displayTip) {
                Hint.SetActive(false);
            }
            if (paragraphCount < paragraphs.Length - 1) {
                letterCount = 0;
                IntroTextContainer.SetText("");
                paragraphCount++;
                time = Time.fixedTime + Speed;
				if (paragraphCount == 2) {
					background.Display1 ();
				}
				if (paragraphCount == 3) {
					background.Display2 ();
				}
            } else {
                UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
            }
        }

        /// <summary> 
        /// Callback method skip button uses to go to next scene.
        /// </summary> 
        public void GoToMenuScene() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
        }
    }
}

﻿using System;
using Assets.Scripts.UI_Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts {
    public class GameNetworkManager : NetworkManager {
        public Settings Settings;
        public bool SinglePlayer;
        public bool Tutorial;

        [HideInInspector] public int NetworkSeed;
        [HideInInspector] public bool IsHost;

        private NetworkConnection host, partner;
        private string hostGoalScene, partnerGoalScene;
        private bool hostReadyForSpawn, partnerReadyForSpawn, menuTransition;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        public void Awake() {
            host = null;
            partner = null;
            hostReadyForSpawn = false;
            menuTransition = true;
            partnerReadyForSpawn = SinglePlayer;
            NetworkSeed = -1;
        }


        /// <summary>
        /// Finds position set by scene for players to spawn and spawns players there
        /// </summary>
        public void SpawnPlayers() {
            if (!NetworkServer.active) return;
            var player1SpawnPos = GameObject.Find("Player1Spawn").transform.position;
            var player1 = Instantiate(spawnPrefabs[0], player1SpawnPos, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(host, player1, 0);
            if (SinglePlayer) return;

            var player2SpawnPos = GameObject.Find("Player2Spawn").transform.position;
            var player2 = Instantiate(spawnPrefabs[1], player2SpawnPos, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(partner, player2, 0);
        }

        /// <summary>
        /// Handler method for Ready for Spawn messages. Once both players are ready calls spawn.
        /// </summary>
        /// <param name="message">Network message to handle</param>
        private void ReadyForSpawn(NetworkMessage message) {
            var playerConnectionId = message.ReadMessage<IntegerMessage>().value;
            if (playerConnectionId == host.connectionId) {
                hostReadyForSpawn = true;
            } else if (playerConnectionId == partner.connectionId) {
                partnerReadyForSpawn = true;
            }
            if (hostReadyForSpawn & partnerReadyForSpawn) {
                SpawnPlayers();

                const short type3 = (short)NetworkMessageType.SCENE_TRANSITION_START;
                NetworkServer.SendToAll(type3, new IntegerMessage(0));

                const short type = (short)NetworkMessageType.SPAWN_DONE;
                hostReadyForSpawn = false;
                host.Send(type, new EmptyMessage());

                if (SinglePlayer) return;
                partnerReadyForSpawn = false;
                partner.Send(type, new EmptyMessage());
            }
        }

        /// <summary>
        /// Handler method for Change Scene messages. Once both players wish to change to the same scene it changes scene.
        /// </summary>
        /// <param name="message"></param>
        private void RequestChangeScene(NetworkMessage message) {
            var goalScene = message.ReadMessage<StringMessage>().value;
            if (message.conn.Equals(host)) {
                hostGoalScene = goalScene;
            } else {
                partnerGoalScene = goalScene;
            }

            if (!SinglePlayer &&
                (string.IsNullOrEmpty(hostGoalScene) ||
                !hostGoalScene.Equals(partnerGoalScene))) return;
            const short type = (short) NetworkMessageType.SCENE_TRANSITION_START;
            NetworkServer.SendToAll(type, new IntegerMessage(0));
        }

        /// <summary>
        /// Helper method to change scene and set new network seed
        /// </summary>
        /// <param name="goalScene">Scene to change to</param>
        private void ChangeScene(string goalScene) {
            var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            if (sceneName == "GameScene") {
                SaveSpawnPositions();
            }
            SetNewNetworkedSeed();
            ServerChangeScene(goalScene);
        }

        private void SaveSpawnPositions() {
            var player1Pos = GameObject.Find("Player1(Clone)").transform.position;
            var gameHUD = GameObject.Find("GameHUD").GetComponent<GameHudController>();
            gameHUD.MapBaseSpawnPosition = player1Pos;
        }

        /// <summary>
        /// Generate a network seed and send to partner
        /// </summary>
        private void SetNewNetworkedSeed() {
            var now = System.DateTime.Now;
            NetworkSeed = now.Second * 1000 + now.Millisecond;
            if (partner == null) return;

            var type = (short)NetworkMessageType.TRANSFER_NETWORK_SEED;
            var seedMessage = new IntegerMessage(NetworkSeed);
            partner.Send(type, seedMessage);
        }

        /// <summary>
        /// Method called on the server when a client connects. Waits for 2 players before starting
        /// </summary>
        /// <param name="connection">Connection made</param>
        public override void OnServerConnect(NetworkConnection connection) {
            if (partner != null) return;
            if (host == null) host = connection;
            else partner = connection;

            if (SinglePlayer || partner != null) {
                const short type = (short)NetworkMessageType.CHANGE_SCENE_REQUEST;
                NetworkServer.RegisterHandler(type, RequestChangeScene);
                const short type2 = (short)NetworkMessageType.READY_FOR_SPAWN;
                NetworkServer.RegisterHandler(type2, ReadyForSpawn);
                const short type3 = (short)NetworkMessageType.QUIT;
                NetworkServer.RegisterHandler(type3, ReturnToMenu);

                const short type4 = (short)NetworkMessageType.SCENE_TRANSITION_START;
                NetworkServer.SendToAll(type4, new IntegerMessage(-1));
            }
        }

        private void ReturnToMenu(NetworkMessage msg) {
            NetworkServer.SendToAll((short) NetworkMessageType.RETURN_TO_MENU, new EmptyMessage());
        }

        /// <summary>
        /// Method called on the client when they connect to a server.
        /// </summary>
        /// <param name="connection">Connection made</param>
        public override void OnClientConnect(NetworkConnection connection) {
            // Left empty to only set client ready on client scene change
        }

        /// <summary>
        /// Method called on the client when scene changes, sets ready state
        /// </summary>
        /// <param name="connection">Connection which changed scene</param>
        public override void OnClientSceneChanged(NetworkConnection connection) {
            ClientScene.Ready(connection);
            if (connection.Equals(client.connection))
                ClientScene.localPlayers.Add(null);
        }

        /// <summary>
        /// Callback method for UI, starts hosting.
        /// </summary>
        public void GameStartHost() {
            networkPort = Settings.GameSettings.Port;
            IsHost = true;
            if (StartHost() == null) throw new Exception();
            const short type = (short)NetworkMessageType.SCENE_TRANSITION_END;
            NetworkServer.RegisterHandler(type, CompleteChangeScene);
            GetComponentInChildren<SceneTransitioner>().RegisterHandler();
        }

        private int count = 0;
        private void CompleteChangeScene(NetworkMessage msg) {
            count++;
            if (!SinglePlayer && count <= 1) return;
            count = 0;
            if (menuTransition) {
                InitialSceneTransition();
                const short type2 = (short)NetworkMessageType.SCENE_TRANSITION_CLEAR;
                NetworkServer.SendToAll(type2, new EmptyMessage());
                return;
            }
            ChangeScene(hostGoalScene);
            hostGoalScene = null;
            partnerGoalScene = null;
            const short type = (short)NetworkMessageType.SCENE_TRANSITION_CLEAR;
            NetworkServer.SendToAll(type, new EmptyMessage());
        }

        private void InitialSceneTransition() {
            ServerChangeScene("PersistObjectsScene");
            SetNewNetworkedSeed();
            if(Tutorial){
                  ServerChangeScene("TutorialScene");
            }
            else{
              ServerChangeScene("GameScene");
             }
            menuTransition = false;
        }

        /// <summary>
        /// Callback method for UI, stops hosting.
        /// </summary>
        public void GameStopHost() {
            singleton.StopHost();
            IsHost = false;
            host = null;
        }

        /// <summary>
        /// Callback method for UI, starts connection.
        /// </summary>
        public void GameStartClient() {
            networkAddress = GameObject.Find("IPInput").GetComponent<TMP_InputField>().text;
            networkPort = Settings.GameSettings.Port;
            StartClient();
            const short type = (short)NetworkMessageType.TRANSFER_NETWORK_SEED;
            client.RegisterHandler(type, msg => NetworkSeed = msg.ReadMessage<IntegerMessage>().value);
            GetComponentInChildren<SceneTransitioner>().RegisterHandler();
        }
    }
}
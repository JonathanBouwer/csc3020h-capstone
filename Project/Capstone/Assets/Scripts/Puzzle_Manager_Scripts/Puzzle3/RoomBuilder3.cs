﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
    public class RoomBuilder3 : MonoBehaviour {
        [HideInInspector] public GameObject Mine, Floor, Wall, LeavingPlate, RestartPlate, Room;
        [HideInInspector] public int Width, Breadth, StartAreaLength;
		[HideInInspector] public bool host;
		[HideInInspector]public GameObject[] decor;

        // offsets for different parameters.
        private float blockWidth = 5, yoffset = 5, tileWidth = 10;

        /// <summary>
        /// Method to Build room
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator BuildRoom() {
            Room = new GameObject("Environment");
            CreateStartArea();
            yield return null;
            CreateWalls();
            yield return null;
            var mazePos = new Vector3(0, 0, 0);
            Mine.transform.parent = Room.transform;
            Mine.transform.localPosition = mazePos;

            CreatePlayerSpawns();
            yield return null;

            var leavingPlate = Instantiate(LeavingPlate, Room.transform);
            leavingPlate.transform.position = new Vector3 {
				x = (-(StartAreaLength/2)*tileWidth)-blockWidth/2,
                y = 0.1f,
                z = (-StartAreaLength * tileWidth) + tileWidth / 2
            };
            leavingPlate.name = "LeavePlate";
            var restartingPlate = Instantiate(RestartPlate, Room.transform);
            restartingPlate.transform.position = new Vector3 {
				x =( Width * blockWidth)+((StartAreaLength/2)*tileWidth)-blockWidth/2,
                y = -0.1f,
                z = (-StartAreaLength * tileWidth) + tileWidth / 2
            };
            restartingPlate.name = "RestartPlate";
			PlaceDecor ();

        }

        /// <summary>
        /// Helper method to set player spanws
        /// </summary>
        private void CreatePlayerSpawns() {
            var baseSpawn = new Vector3 {
                x = (Width * blockWidth) / 2,
                y = 1,
                z = -(StartAreaLength * tileWidth) / 2
            };
            Vector3 apart = new Vector3(0, 0, 3);
            var player1Spawn = new GameObject("Player1Spawn");
            player1Spawn.transform.localPosition = baseSpawn - apart;
            player1Spawn.transform.parent = Room.transform;
            var player2Spawn = new GameObject("Player2Spawn");
            player2Spawn.transform.localPosition = baseSpawn + apart;
            player2Spawn.transform.parent = Room.transform;
        }
        
        /// <summary>
        /// Start area is a floor sorrounding mine field
        /// </summary>
        private void CreateStartArea() {
            var startArea = new GameObject("StartArea");
            startArea.transform.parent = Room.transform;
            for (int a = -Breadth / 2 - StartAreaLength; a < StartAreaLength; a++) {
                for (int i = -StartAreaLength; i < Width / 2 + StartAreaLength; i++) {
                    var floor = Instantiate(Floor);
                    floor.transform.parent = startArea.transform;
                    floor.name = "MineFloor";
                    floor.transform.localPosition = new Vector3 {
                        x = i * tileWidth + (tileWidth / 4),
                        y = 0,
                        z = (blockWidth / 2) - (a + 1) * tileWidth
                    };
                }
            }
        }
		/// <summary>
		/// Place Decor;
		/// </summary>
		public void PlaceDecor() {
			var decorObject = new GameObject("Decor");
			decorObject.transform.parent = Room.transform;
			int DecorNum;
			if (host)
				DecorNum = 1;
			else
				DecorNum = 0;
		
			for (int w = -StartAreaLength; w < Width / 2 + StartAreaLength; w=w+2) {
				var position = new Vector3 {
					x = w * 10 + (blockWidth / 2),
					y = 0f,
					z = (Breadth + StartAreaLength * 2) * (blockWidth) - (blockWidth )
				};
	
				var Decor = Instantiate(decor[DecorNum]);
				Decor.name = "decor "+DecorNum;
				Decor.transform.parent = decorObject.transform;
				Decor.transform.localPosition = position;
			}
		}
        
        /// <summary>
        /// Create the room walls
        /// </summary>
        private void CreateWalls() {
            var walls = new GameObject("Walls");
            walls.transform.parent = Room.transform;
            var nsRotation = new Vector3(0, 0, 0);
            var ewRotation = new Vector3(0, 90, 0);
            // south wall
            for (int w = -StartAreaLength; w < Width / 2 + StartAreaLength; w++) {
                var southPos = new Vector3 {
                    x = w * tileWidth + (tileWidth / 4),
                    y = yoffset,
                    z = -tileWidth * StartAreaLength - (blockWidth / 2)
                };
                BuildWall(walls, nsRotation, southPos);
            }
            // north wall
            for (int w = -StartAreaLength; w < Width / 2 + StartAreaLength; w++) {
                var northPos = new Vector3 {
                    x = w * 10 + (blockWidth / 2),
                    y = yoffset,
                    z = (Breadth + StartAreaLength * 2) * (blockWidth) - (blockWidth / 2)
                };
                BuildWall(walls, nsRotation, northPos);
            }
            // west wall
            for (int w = -StartAreaLength; w < Breadth / 2 + StartAreaLength; w++) {

                var westPos = new Vector3 {
                    x = -blockWidth / 2 - StartAreaLength * tileWidth,
                    y = yoffset,
                    z = w * 10 + (blockWidth / 2)
                };
                BuildWall(walls, ewRotation, westPos);

            }
            // east wall
            for (int w = -StartAreaLength; w < Breadth / 2 + StartAreaLength; w++) {
                var eastPos = new Vector3 {
                    x = Width * blockWidth + StartAreaLength * tileWidth - blockWidth / 2,
                    y = yoffset,
                    z = w * 10 + (blockWidth / 2)
                };
                BuildWall(walls, ewRotation, eastPos);
            }
        }

        /// <summary>
        /// Helper method to build walls
        /// </summary>
        /// <param name="parent">Parent of the wall</param>
        /// <param name="rotation">Local rotation fo the wall</param>
        /// <param name="position">Relative position of the wall</param>
        private void BuildWall(GameObject parent, Vector3 rotation, Vector3 position) {
            var wall = Instantiate(Wall);
            wall.name = "MineWall";
            wall.transform.parent = parent.transform;
            wall.transform.localPosition = position;
            wall.transform.Rotate(rotation);
        }
    }
}


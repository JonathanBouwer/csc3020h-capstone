﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class TitleManager : MenuUIManager {
        public float RandomDelay;

        private TextMeshProUGUI text;
        private float dialation, wait, softness, dTsoftness;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            text = GetComponent<TextMeshProUGUI>();
            dialation = 0;
            softness = 0;
            dTsoftness = 0;
            wait = Time.time + RandomDelay + 10 * Random.value;
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will have the title pulse and occationally blur the title
        /// </remarks>
        void Update() {
            // Pulse
            dialation = Mathf.SmoothStep(0.05f, 0.1f, Mathf.PingPong(Time.time / 2, 1.0f));
            text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, dialation);

            // Occationally blur
            if (Time.time > wait) {
                softness = Mathf.Lerp(0, 0.5f, Mathf.PingPong(dTsoftness, 1.0f));
                text.fontMaterial.SetFloat(ShaderUtilities.ID_OutlineSoftness, softness);
                dTsoftness += Time.deltaTime;

                if (dTsoftness > 2) {
                    wait = Time.time + RandomDelay + 10 * Random.value;
                    dTsoftness = 0;
                }
            }
            text.UpdateMeshPadding();
        }
    }
}
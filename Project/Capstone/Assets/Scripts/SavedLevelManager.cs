﻿using System.IO;

namespace Assets.Scripts {
    public class SavedLevelManager {
        private readonly string levelDirectory;
        
        /// <summary>
        /// Constructor for Saved Level Manager
        /// </summary>
        /// <param name="levelDirectory">Directory to save levels in</param>
        public SavedLevelManager(string levelDirectory) {
            this.levelDirectory = levelDirectory;
            Directory.CreateDirectory(this.levelDirectory);
        }
        
        /// <summary>
        /// Method to save level data to given file
        /// </summary>
        public void SaveLevel(string filename, string levelData) {
            if (!Directory.Exists(levelDirectory))
                Directory.CreateDirectory(levelDirectory);
            var filePath = levelDirectory + "/" + filename;
            File.WriteAllText(filePath, levelData);
        }
        
        /// <summary>
        /// Method to load level data from given file.
        /// </summary>
        /// <returns>Level data or an empty string if file does not exist.</returns>
        public string LoadLevel(string filename) {
            var filePath = levelDirectory + "/" + filename;
            return File.Exists(filePath) ? File.ReadAllText(filePath) : "";
        }
        
        /// <summary>
        /// Method to clear saved level director on application close.
        /// </summary>
        public void CleanLevelDirectory() {
            var files = Directory.GetFiles(levelDirectory);
            foreach (var file in files) {
                File.Delete(file);
            }
            Directory.Delete(levelDirectory);
        }
    }
}

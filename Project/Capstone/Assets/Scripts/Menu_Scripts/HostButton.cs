﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Menu_Scripts {
    public class HostButton : MonoBehaviour {
        public GameNetworkManager GameNetworkManager;
        public TextMeshProUGUI ButtonText;
        public ErrorText ErrorText;
        public GameObject Cancel;
        public ConnectButton ConnectButton;

        private Button button;
        private bool isHosting;
        private const float Wait = 0.2f;

        /// <summary> 
        /// Start method to initialize class variables right before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        public void Start() {
            button = GetComponent<Button>();
            isHosting = false;
        }

        /// <summary>
        /// UI callback method to host a server, animates UI slightly
        /// </summary>
        public void Host() {
            try {
                GameNetworkManager.GameStartHost();
                ButtonText.alignment = TextAlignmentOptions.Left;
                StartCoroutine(LoopDots());
                button.interactable = false;
                ConnectButton.Disable();
                isHosting = true;
                Cancel.SetActive(true);
            } catch (Exception) {
                ErrorText.FadeInOut("Hosting failed (check log for error)\n\nTry changing port in GameSettings.json");
            }
        }

        /// <summary>
        /// UI Callback method to stop hosting server
        /// </summary>
        public void StopHost() {
            if (!isHosting) return;
            GameNetworkManager.GameStopHost();
            StopAllCoroutines();
            ButtonText.alignment = TextAlignmentOptions.Center;
            ButtonText.text = "Host";
            button.interactable = true;
            ConnectButton.Undisable();
            Cancel.SetActive(false);
        }

        /// <summary>
        /// Simple animation method to add dots after text
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoopDots() {
            var n = 0;
            while (true) {
                ButtonText.text = "Hosting" + new string('.', n) + new string(' ', 3 - n);
                n = (n + 1) % 4;
                yield return new WaitForSeconds(Wait);
            }
        }
    }
}

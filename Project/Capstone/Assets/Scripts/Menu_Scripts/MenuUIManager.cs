﻿using System;
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class MenuUIManager : MonoBehaviour {
        public Goal TitleGoal, MainMenuGoal, SettingsMenuGoal, PlayMenuGoal;

        private Goal goal;
        private RectTransform rectTransform;
        private float dt, velocity, waitRemaining;
        private MenuState currentState, goalState;

        /// <summary> 
        /// Awake method to initialize class variables long before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html">Unity Documentation</see> for more info
        /// </summary>
        void Awake() {
            rectTransform = GetComponent<RectTransform>();
            dt = 0;
            velocity = 0;
            currentState = MenuState.TitleScreen;
            goalState = MenuState.TitleScreen;
        }

        /// <summary> 
        /// Method called every frame at the end of frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.LateUpdate.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method moves towards goal state of animation.
        /// </remarks>
        void LateUpdate() {
            if (!InGoalState()) {
                currentState = MoveToGoal();
            }
        }

        /// <summary>
        /// Method to check if in goal state
        /// </summary>
        /// <returns>If in goal state</returns>
        public bool InGoalState() {
            return currentState == goalState;
        }

        /// <summary>
        /// Method to change animation goal
        /// </summary>
        /// <param name="menuState">Goal to change to</param>
        public void SetGoal(MenuState menuState) {
            if (goalState == menuState) return;
            currentState = goalState;
            dt = 0;
            velocity = 0;
            goalState = menuState;
            switch (goalState) {
                case MenuState.MainMenu:
                    goal = MainMenuGoal;
                    break;
                case MenuState.SettingsMenu:
                    goal = SettingsMenuGoal;
                    break;
                case MenuState.PlayMenu:
                    goal = PlayMenuGoal;
                    break;
                case MenuState.TitleScreen:
                    goal = TitleGoal;
                    break;
            }

            waitRemaining = goal.InitialDelay;
            goal.SetRotation();
        }

        /// <summary>
        /// Method to linearly interpolate towards goal
        /// </summary>
        /// <returns>Goal State if at goal, otherwise current state</returns>
        private MenuState MoveToGoal() {
            if (waitRemaining > 0) {
                waitRemaining -= Time.deltaTime;
                return currentState;
            }
            dt = Mathf.SmoothDamp(dt, 1, ref velocity, goal.MoveDuration);

            var anchorMin = Vector2.Lerp(rectTransform.anchorMin, goal.Anchors.Min, dt);
            rectTransform.anchorMin = anchorMin;

            var anchorMax = Vector2.Lerp(rectTransform.anchorMax, goal.Anchors.Max, dt);
            rectTransform.anchorMax = anchorMax;

            var position = Vector3.Lerp(rectTransform.anchoredPosition, goal.Position, dt);
            rectTransform.anchoredPosition3D = position;

            var rotation = Quaternion.Lerp(rectTransform.rotation, goal._rotation, dt);
            rectTransform.rotation = rotation;

            var localScale = Vector3.Lerp(rectTransform.localScale, goal.Scale, dt);
            rectTransform.localScale = localScale;

            return Mathf.Approximately(dt, 1) ? goalState : currentState;
        }
    }

    [Serializable]
    public class Goal {
        public Anchors Anchors;
        public Vector3 Position, Rotation, Scale;
        public float MoveDuration, InitialDelay;
        [HideInInspector] public Quaternion _rotation;
        public void SetRotation() {
            _rotation = Quaternion.Euler(Rotation);
        }
    }

    [Serializable]
    public class Anchors {
        public Vector2 Min, Max;
    }
}

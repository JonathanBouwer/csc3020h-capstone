﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4{
    public class PreyController : MonoBehaviour{

        [HideInInspector] public GameNetworkManager Manager;
        [HideInInspector] public HuntBuilder HuntBuilder;
        [HideInInspector] public List<Vector2> GridPositions;
        [HideInInspector] public GameObject[,] Grid;
        [HideInInspector] public GameObject[] Predators;
        public int ID;
        public Vector2 CurrentPos;
        public int Turn;
        
        
        private void Start(){
            HuntBuilder = GameObject.Find("HuntBuilder").GetComponent<HuntBuilder>();
            GridPositions = HuntBuilder.GridPositions;
            Grid = HuntBuilder.Grid;
            Manager = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>();
            Predators = HuntBuilder.Predators;
        }
        
        private void Update(){
            transform.Rotate(Vector3.up * Time.deltaTime * 100);
            
            transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", Color.black);
            
            var colour = new Color(Mathf.PingPong(Time.time, 0.25f), Mathf.PingPong(Time.time, 0.25f), Mathf.PingPong(Time.time, 0.25f));
            if (!(Turn == 1 | Turn == 3)) return;
            transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", colour);
            if (!Manager.IsHost) return;
            StartCoroutine(PlanMove());
            if (!Trapped()) return;
            const short typeWin = (short)NetworkMessageType.PUZZLE_4_SOLVED;
            Manager.client.Send(typeWin, new EmptyMessage());
        }

        public IEnumerator PlanMove(){

            if(!(Manager.IsHost)) yield break;
            
            int currX = (int) CurrentPos.x;
            int currY = (int) CurrentPos.y;
            
            var Moves = new List<Vector2>();
            if (ValidateMove(currX + 1, currY)) 
                Moves.Add(new Vector2(currX + 1, currY));
            if (ValidateMove(currX - 1, currY)) 
                Moves.Add(new Vector2(currX - 1, currY));
            if (ValidateMove(currX , currY + 1)) 
                Moves.Add(new Vector2(currX, currY + 1));
            if (ValidateMove(currX, currY - 1)) 
                Moves.Add(new Vector2(currX, currY - 1));
            if(Moves.Count == 0) yield break;
            
            yield return null;
            
            var Weights = new List<double>();
            foreach (var move in Moves){
                double weight = 0;
                foreach (var predator in Predators){
                    var position = predator.GetComponent<PredatorController>().CurrentPos;
                    weight += Vector3.Distance(move, position);
                    yield return null;
                }
                Weights.Add(weight);
                yield return null;
            }

            int index = 0;
            double max = 0;
            for (var i = 0; i < Weights.Count; i++){
                var weight = Weights[i];
                print(weight);
                if (weight > max){
                    index = i;
                    max = weight;
                }
                yield return null;
                print(index);
            }

            var msg = new StringMessage(Moves[index].x + ":" + Moves[index].y);
            const short typePreyMoveRequest = (short)NetworkMessageType.PUZZLE_4_PREY_MOVE_REQUEST;
            Manager.client.Send(typePreyMoveRequest, msg);
        }
        
        public bool ValidateMove(int x, int y){
            if (!GridPositions.Contains(new Vector2(x, y))) return false;
            return Grid[x, y].transform.childCount == 0;
        }
        
        public void MovePrey(int x, int y){
            Debug.Log("Move "+ID+" to "+x+" "+y);
            var goal = transform.localPosition;
            transform.SetParent(Grid[x,y].transform, true);
            StartCoroutine(TransitionPrey(transform.localPosition, goal));
            CurrentPos = new Vector2(x, y);
        }

        private IEnumerator TransitionPrey(Vector3 start, Vector3 goal) {
            for (int i = 0; i < 15; i++) {
                transform.localPosition = Vector3.Lerp(start, goal, i / 15.0f);
                yield return null;
            }
            transform.localPosition = goal;
            transform.LookAt(HuntBuilder.Prey.transform, Vector3.up);
        }

        /// <summary>
        /// Method to idnetify if the Prey is trapped or not
        /// </summary>
        /// <returns>The Prey trapped status, true for trapped and false for not trapped</returns>
        public bool Trapped(){
            /*var up = new Vector2(CurrentPos.x, CurrentPos.y+1);
            var down = new Vector2(CurrentPos.x, CurrentPos.y-1);
            var left = new Vector2(CurrentPos.x-1, CurrentPos.y);
            var right = new Vector2(CurrentPos.x+1, CurrentPos.y);

            if (GridPositions.Contains(up) & Grid[(int) up.x, (int) up.y].transform.childCount == 0) return false;
            if (GridPositions.Contains(down) & Grid[(int) down.x, (int) down.y].transform.childCount == 0) return false;
            if (GridPositions.Contains(left) & Grid[(int) left.x, (int) left.y].transform.childCount == 0) return false;
            return !(GridPositions.Contains(right) & Grid[(int) right.x, (int) right.y].transform.childCount == 0);*/
            int currX = (int) CurrentPos.x;
            int currY = (int) CurrentPos.y;
            
            var Moves = new List<Vector2>();
            if (ValidateMove(currX + 1, currY)) 
                return false;
            if (ValidateMove(currX - 1, currY))
                return false;
            if (ValidateMove(currX, currY + 1))
                return false;
            if (ValidateMove(currX, currY - 1))
                return false;
            return true;
        }
    }
}
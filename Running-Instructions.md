# Running Instructions #

## Multiplayer ##

### On 1 PC ###
This assumes the unity editor will be used as 1 instance

1. Ensure the single player variable is **unchecked** in the network manager.
2. Click 'File > Build and Run' or 'Ctrl+B'.
3. Change to the menu Scene in the editor and start.
4. On one instance of the game click host.
5. On the other type 'localhost' and connect.

### On multiple PCs ###
This assumes that 2 builds of the game have been distributed

1. Get the IP address of the first PC (*Instance 1*).
2. Portforward the games port (default 7777) on the shared router ([Instructions](http://www.noip.com/support/knowledgebase/general-port-forwarding-guide/)).
3. Start the game on both PCs.
4. On *Instance 1* of the game click host.
5. On *Instance 2* type the IP and connect.

## Singleplayer ##

1. Ensure the single player variable is checked in the network manager
2. Then simply start in the menu scene and press host

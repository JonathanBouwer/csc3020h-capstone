﻿using System;
using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4 {
    [Serializable]
    public class SerializedPuzzle4Data : SerializedSceneItem {
        public string Name;
        public Vector3 Position, Scale;
        public string[] Extra;
    }

    class ScenePuzzle4Mapper : SceneItemMapper<SerializedPuzzle4Data> {
        public Puzzle4Builder Builder;

        /// <summary>
        /// Constructor for puzzle mapper
        /// </summary>
        /// <param name="builder">Builder object for the mapper to use prefabs of</param>
		public ScenePuzzle4Mapper(Puzzle4Builder builder) {
            Builder = builder;
        }
        
        /// <summary>
        /// Specialized method to map items to SerializedPuzzle4Data
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <returns>Mapped item in SerializedPuzzle4Data format</returns>
        public override SerializedPuzzle4Data MapItem(Transform item) {
            var result = new SerializedPuzzle4Data {
                Position = item.position,
                Scale = item.localScale,
                Name = item.name
            };

            if (item.name == "HuntWall") {
                result.Extra = new string[1];
                result.Extra[0] = "" + item.localRotation.y;
            }

            return result;
        }
        
        /// <summary>
        /// Specialized method to unmap SerializedPuzzle4Data items
        /// </summary>
        /// <param name="item">Item to unmap</param>
        /// <returns>Game object instantiated by unmapping</returns>
        public override GameObject UnmapItem(SerializedPuzzle4Data item) {
            GameObject result;
            switch (item.Name) {
                case "HuntWall":
                    result = Object.Instantiate(Builder.MazeWall);
                    if (item.Extra[0] != "0") {
                        result.transform.Rotate(0, 90, 0);
                    }
                    break;
                case "HuntFloor":
                    result = Object.Instantiate(Builder.MazeFloor);
                    break;
                case "LeavePlate":
                    result = Object.Instantiate(Builder.LeavePlate);
                    break;
                case "Block":
                    result = Object.Instantiate(Builder.Block);
                    break;
                default:
                    result = new GameObject();
                    break;
            }
            result.name = item.Name;
            result.transform.position = item.Position;
            result.transform.localScale = item.Scale;
            return result;
        }
    }
}

﻿using UnityEngine;

// Scripts from "Procedural Room Dungeon Generator-MIDSIN" on Unity Asset Store- scripts slightly modified
namespace Assets.Scripts.Dungeon_Generator_Scripts {
    public class Tile : MonoBehaviour {
        [HideInInspector] public IntVector2 Coordinates;
    }
}

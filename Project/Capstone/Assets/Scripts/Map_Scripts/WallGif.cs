﻿using UnityEngine;

namespace Assets.Scripts.Map_Scripts {
    public class WallGif : MonoBehaviour {
        public Texture[] Frames;
        public float FramesPerSecondMin;
        public float FramesPerSecondMax;

        private Renderer componentRenderer;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            componentRenderer = GetComponentInParent<Renderer>();
            ChangeFrame();
            InvokeRepeating("ChangeFrame", Random.Range(0f, 4f), Random.Range(FramesPerSecondMin, FramesPerSecondMax));
        }

        /// <summary>
        /// Method called repeatedly to change renderer's texture
        /// </summary>
        void ChangeFrame() {
            int index = Random.Range(0, Frames.Length - 1);
            componentRenderer.material.mainTexture = Frames[index];
            componentRenderer.material.mainTextureScale = new Vector2(-1, -1);
        }
    }
}

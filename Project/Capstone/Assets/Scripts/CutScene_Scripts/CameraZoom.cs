﻿using UnityEngine;

public class CameraZoom : MonoBehaviour {
	
	public Vector3 moveTowards;
	public float speed;
	void Update () {
		transform.localPosition = Vector3.MoveTowards (transform.localPosition, moveTowards, speed * Time.deltaTime);

	}
}

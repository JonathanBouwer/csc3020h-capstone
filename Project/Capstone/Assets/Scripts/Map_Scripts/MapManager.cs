﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Map_Scripts {
    public class MapManager : MonoBehaviour {
        private const short KeyTestType = (short) NetworkMessageType.DO_KEY_TEST;
        private const short KeySucessType = (short) NetworkMessageType.KEY_TEST_SUCESS;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start () {
            NetworkServer.RegisterHandler(KeyTestType, StartKeyAnimation);
            NetworkServer.RegisterHandler(KeySucessType, KeySucess);
        }

        /// <summary>
        /// Method to tell clients to show key test animation
        /// </summary>
        /// <param name="message"></param>
        public void StartKeyAnimation(NetworkMessage message) {
            NetworkServer.SendToAll(KeyTestType, new EmptyMessage());
        }

        /// <summary>
        /// Method to tell clients to remove final room doors on key sucess
        /// </summary>
        /// <param name="message"></param>
        public void KeySucess(NetworkMessage message) {
            NetworkServer.SendToAll(KeySucessType, new EmptyMessage());
        }
    }
}

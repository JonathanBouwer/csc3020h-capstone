﻿using System;
using Assets.Scripts.UI_Scripts;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Player_Scripts {
    public class PlayerMovement : NetworkBehaviour {
        public float Speed, RunMultiplyer, TurnSpeed;
        public Animator PlayerAnimator;

        private PlayerChat chat;
        private CameraControl cam;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            chat = GameObject.Find("Chat").GetComponent<PlayerChat>();
            cam = transform.FindChild("Camera").GetComponent<CameraControl>();
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will move the player if they are a local player and are also not typing and are pressing an axis key.
        /// </remarks>
        void FixedUpdate (){
            if (!isLocalPlayer) return;
            if (chat.InputField.isFocused) return;
            switch (cam.GetCameraState()) {
                case CameraControl.State.FirstPerson:
                    DoFirstPersonMovement();
                    break;
                case CameraControl.State.ThirdPerson:
                    goalDirection = North;
                    DoThirdPersonMovement();
                    break;
                case CameraControl.State.TopDown:
                    DoTopDownMovement();
                    break;
                default:
                    transform.forward =
                        Vector3.RotateTowards(transform.forward, goalDirection, 0.1f * TurnSpeed, 0);
                    PlayerAnimator.SetFloat("Speed", 0);
                    PlayerAnimator.SetFloat("Turn", 0);
                    break;
            }
        }

        /// <summary>
        /// Helper method to perform movement in first person,
        /// Not yet implemented as it is not used
        /// </summary>
        private void DoFirstPersonMovement() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Helper method for third person movement
        /// </summary>
        private void DoThirdPersonMovement() {
            var verticalMovement = Input.GetAxis("Vertical");
            if (Input.GetKey(KeyCode.LeftShift)) {
                verticalMovement *= RunMultiplyer;
            }
            var forward = 0.0f;
            var turn = Input.GetAxis("Horizontal") * TurnSpeed;
            if (verticalMovement >= 0) {
                forward = verticalMovement * Time.deltaTime * Speed * 0.75f;
            } else {
                turn = verticalMovement;
            }
            PlayerAnimator.SetFloat("Speed", verticalMovement / 2.0f, 0.1f, Time.deltaTime);
            
            PlayerAnimator.SetFloat("Turn", turn / 1.5f, 0.1f, Time.deltaTime);
            transform.Rotate(Vector3.up, turn);
            transform.position += forward * transform.forward;
        }

        private static readonly Vector3 North = new Vector3( 0,0, 1);
        private static readonly Vector3 East  = new Vector3( 1,0, 0);
        private static readonly Vector3 South = new Vector3( 0,0,-1);
        private static readonly Vector3 West  = new Vector3(-1,0,0);
        private Vector3 goalDirection = North;
        /// <summary>
        /// Helper method for top down movement
        /// </summary>
        private void DoTopDownMovement() {
            var verticalMovement = Input.GetAxis("Vertical");
            var horizontalMovement = Input.GetAxis("Horizontal");
            var actualMovement = 0.0f;
            if (verticalMovement > 0.2f) {
                actualMovement = verticalMovement;
                goalDirection = North;
            } else if (verticalMovement < -0.2f) {
                actualMovement = verticalMovement;
                goalDirection = South;
            } else if (horizontalMovement > 0.2f) {
                actualMovement = horizontalMovement;
                goalDirection = East;
            } else if (horizontalMovement < -0.2f) {
                actualMovement = horizontalMovement;
                goalDirection = West;
            }
            if (Input.GetKey(KeyCode.LeftShift)) {
                actualMovement *= RunMultiplyer;
            }
            actualMovement = Mathf.Abs(actualMovement);
            PlayerAnimator.SetFloat("Speed", actualMovement / 2.0f, 0.1f, Time.deltaTime);
            PlayerAnimator.SetFloat("Turn", 0);

            var timeChange = Time.deltaTime * Speed;
            var forward = actualMovement * timeChange;
            transform.forward = 
                Vector3.RotateTowards(transform.forward, goalDirection, 0.1f * TurnSpeed, 0);
            transform.position += forward * transform.forward;
        }
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class SceneTransitioner : MonoBehaviour {
        public GameNetworkManager Manager;

        private Image fadeImage;
        private bool opaque;

        /// <summary> 
        /// Start method to initialize class variables right before first frame.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.STart.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            fadeImage = GetComponent<Image>();
            opaque = false;
        }

        /// <summary>
        /// Method to setup all handlers for scene transtions
        /// </summary>
        public void RegisterHandler() {
            const short type = (short)NetworkMessageType.SCENE_TRANSITION_START;
            Manager.client.RegisterHandler(type, StartTransition);
            const short type2 = (short)NetworkMessageType.SCENE_TRANSITION_CLEAR;
            Manager.client.RegisterHandler(type2, ClearTransition);
        }

        /// <summary>
        /// Network message handler for clear events
        /// </summary>
        private void ClearTransition(NetworkMessage netmsg) {
            fadeImage.color = new Color(0, 0, 0, 0);
        }

        /// <summary>
        /// Network message handler for transition events
        /// </summary>
        private void StartTransition(NetworkMessage netmsg) {
            var value = netmsg.ReadMessage<IntegerMessage>().value;
            StartCoroutine(Transition(value));
        }

        /// <summary>
        /// Coroutine to show transition animation based on transition state.
        /// </summary>
        /// <param name="value">Value sent over network to determine transition (less than 0 is plain transition)</param>
        private IEnumerator Transition(int value) {
            const float size = 15.0f;
            if (opaque) {
                yield return StartCoroutine(FadeOut(size));
                Time.timeScale = 1;
            } else {
                Time.timeScale = 0;
                yield return StartCoroutine(FadeIn(size, value < 0));
                const short type = (short)NetworkMessageType.SCENE_TRANSITION_END;
                Manager.client.Send(type, new EmptyMessage());
            }
            opaque = !opaque;
        }

        /// <summary>
        /// Method to show a fade in animation RGBA(1,1,1,0) -> RGBA(1,1,1,1) -> RGBA(0,0,0,1)
        /// </summary>
        /// <param name="size">Number of iterations for a step</param>
        /// <param name="plainTranstion">Whether to do plain transition</param>
        private IEnumerator FadeIn(float size, bool plainTranstion) {
            if (plainTranstion) {
                for (var i = 0; i <= size; i++) {
                    fadeImage.color = new Color(0, 0, 0, i / size);
                    yield return null;
                }
                yield break;
            }
            for (var i = 0; i < size; i++) {
                fadeImage.color = new Color(1, 1, 1, i / size);
                yield return null;
            }
            for (var i = size; i > 0; i--) {
                fadeImage.color = new Color(i / size, i / size, i / size, 1);
                yield return null;
            }
            fadeImage.color = Color.black;
        }

        /// <summary>
        /// Method to show a fade out animation RGBA(0,0,0,1) -> RGBA(0,0,0,0)
        /// </summary>
        /// <param name="size">Number of iterations for a step</param>
        private IEnumerator FadeOut(float size) {
            for (var i = size; i > 0; i--) {
                fadeImage.color = new Color(0, 0, 0, i / size);
                yield return null;
            }
            fadeImage.color = new Color(0, 0, 0, 0);
        }
    }
}

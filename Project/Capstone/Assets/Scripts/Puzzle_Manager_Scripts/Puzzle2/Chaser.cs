﻿using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    public class Chaser : MonoBehaviour {
        public float Speed;
        public GameObject StartPosition;

        private enum State {
            Off,
            Idle,
            Chasing
        }
        
        private GameObject target;
        private Vector3 myLastPosition, lastPosition, currentPosition, startPosition;
        private State state;
        private Animator animator;
        private bool poweredDown, poweringDown;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            animator = GetComponentInChildren<Animator>();
        }

        /// <summary> 
        /// Method called every physicsupdate to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.FixedUpdate.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will simulate the chaser using a FSM
        /// </remarks>
        void FixedUpdate() {
            if (poweredDown) return;
            if (target != null) {
                Simulate();
            } else {
                WaitForPlayerSpawn();
            }
        }

        /// <summary>
        /// Method used to simulate chaser, based on state of chaser
        /// </summary>
        private void Simulate() {
            if (poweringDown) {
                if (!MoveToStart()) return;
                animator.SetBool("PowerOff", true);
                poweredDown = true;
                return;
            }
            var targetDistance = Vector3.Distance(target.transform.position, transform.position);
            switch (state) {
                case State.Off:
                    if (targetDistance < 30) {
                        state = State.Idle;
                        animator.SetBool("PowerOn", true);
                    }
                    break;
                case State.Idle:
                    if (targetDistance < 40) {
                        state = State.Chasing;
                    } else {
                        MoveToStart();
                    }
                    break;
                case State.Chasing:
                    if (targetDistance > 40) {
                        state = State.Idle;
                    } else {
                        InterceptTarget();
                    }
                    break;
            }
        }

        /// <summary>
        /// Public method called when puzzle is already won
        /// </summary>
        public void PowerDown() {
            poweringDown = true;
        }

        /// <summary>
        /// Annoyance of unity, must wait fo player to spawn before we can set class variables
        /// </summary>
        private void WaitForPlayerSpawn() {
            target = GameObject.Find("Player1(Clone)");
            if (target == null) {
                target = GameObject.Find("Player1");
                if (target == null) return;
            }
            startPosition = StartPosition.transform.position;
            currentPosition = target.transform.position;
            currentPosition.y = transform.position.y;
            startPosition.y = currentPosition.y;
            lastPosition = currentPosition;
            myLastPosition = transform.position;
        }

        /// <summary>
        /// Method to move chaser to start pos
        /// </summary>
        /// <returns>whether at start</returns>
        private bool MoveToStart() {
            if (Vector3.Distance(transform.position, startPosition) < 1.5) {
                animator.SetBool("Chasing", false);
                return true;
            }
            animator.SetBool("Chasing", true);
            transform.forward = Vector3.ProjectOnPlane(startPosition - transform.position, Vector3.up).normalized;
            transform.position += Speed * Time.fixedDeltaTime * transform.forward;
            return false;
        }

        /// <summary>
        /// Method to move to intercept with target
        /// </summary>
        private void InterceptTarget() {
            currentPosition = target.transform.position;
            currentPosition.y = transform.position.y;
            if (Vector3.Distance(transform.position, currentPosition) > 0.01) {
                var interceptPosition = CalculateIntercept();
                if (Vector3.Distance(transform.position, interceptPosition) > 1.5) {
                    transform.forward = (interceptPosition - transform.position).normalized;
                    myLastPosition = transform.position;
                    transform.position += Speed * Time.fixedDeltaTime * transform.forward;
                    animator.SetBool("Chasing", true);
                } else {
                    animator.SetBool("Chasing", false);
                }
            }
            lastPosition = currentPosition;
        }

        /// <summary>
        /// Method to calculate intercept with target, based on velocities and positions
        /// </summary>
        /// <returns>Intercept position as Vector3</returns>
        private Vector3 CalculateIntercept() {
            var myVelocity = Vector3.Distance(myLastPosition, transform.position) / Time.fixedDeltaTime;
            var targetVelocity = Vector3.Distance(lastPosition, currentPosition) / Time.fixedDeltaTime;
            var deltaVelocity = targetVelocity - myVelocity;
            var distance = Vector3.Distance(transform.position, currentPosition);
            var closingTime = distance / deltaVelocity;
            return currentPosition + (currentPosition - lastPosition) * closingTime;
        }
    }
}

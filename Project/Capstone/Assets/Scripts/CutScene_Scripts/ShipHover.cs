﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHover : MonoBehaviour {

	/// <summary>
	/// Simple float up and down method
	/// </summary>
	void Update () {
		var tmp = transform.localPosition;
		tmp.y = Mathf.SmoothStep(5f, 5.7f, Mathf.PingPong(Time.time / 2, 1.0f));
		transform.localPosition = tmp;
	}
}
	


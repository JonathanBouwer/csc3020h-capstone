﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle4{
    public class PredatorController : MonoBehaviour{
        
        [HideInInspector] public GameNetworkManager Manager;
        [HideInInspector] public HuntBuilder HuntBuilder;
        [HideInInspector] public List<Vector2> GridPositions;
        [HideInInspector] public GameObject[,] Grid;
        public int ID;
        public Vector2 CurrentPos;
        public int Turn;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        private void Start(){
            HuntBuilder = GameObject.Find("HuntBuilder").GetComponent<HuntBuilder>();
            GridPositions = HuntBuilder.GridPositions;
            Grid = HuntBuilder.Grid;

            Manager = GameObject.Find("NetworkManager").GetComponent<GameNetworkManager>();
            
            transform.LookAt(HuntBuilder.Prey.transform, Vector3.up);

            Turn = 0;
        }

        private void Update(){
            transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", Color.black);
            var colour = new Color(Mathf.PingPong(Time.time, 0.25f), Mathf.PingPong(Time.time, 0.25f), Mathf.PingPong(Time.time, 0.25f));
            
            if (Turn == 0){
                if (ID == 0) transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", colour);
                if (ID == 1) transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", colour);
            }
            if (Turn == 2){
                if (ID == 2) transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", colour);
                if (ID == 3) transform.GetComponentsInChildren<Renderer>()[0].material.SetColor("_EmissionColor", colour);
            }

        }

        /// <summary>
        /// Validates the move that is about to be made by the Predator
        /// </summary>
        /// <param name="x">The X position the Predator will move to</param>
        /// <param name="y">The Y position the Predator will move to</param>
        /// <returns>True for a valid move, False for an invalid move</returns>
        public bool ValidateMove(int x, int y){
            if (!GridPositions.Contains(new Vector2(x, y))) return false;
            return Grid[x, y].transform.childCount == 0;
        }

        /// <summary>
        /// Moves the Predator object in the game world
        /// </summary>
        /// <param name="x">The X position in the grid the Predator will move to</param>
        /// <param name="y">The Y position in the grid the Predator will move to</param>
        public void MovePredator(int x, int y) {
            var goal = transform.localPosition;
            transform.SetParent(Grid[x,y].transform, true);
            StartCoroutine(TransitionPredator(transform.localPosition, goal));
            CurrentPos = new Vector2(x, y);
        }

        private IEnumerator TransitionPredator(Vector3 start, Vector3 goal) {
            for (int i = 0; i < 15; i++) {
                transform.localPosition = Vector3.Lerp(start, goal, i / 15.0f);
                yield return null;
            }
            transform.localPosition = goal;
            transform.LookAt(HuntBuilder.Prey.transform, Vector3.up);
        }

        /// <summary>
        /// Requests the movement of the Predator object
        /// </summary>
        /// <param name="x">The increment in the X position the Predator shall move to</param>
        /// <param name="y">The increment in the Y position the Predator shall move to</param>
        public void RequestMove(int x, int y) {
            int nextX = (int) CurrentPos.x + x;
            int nextY = (int) CurrentPos.y + y;
            if (!ValidateMove(nextX, nextY)) return;
            var message = new StringMessage(ID + "-" + nextX + ":" + nextY);
            const short typePredMoveRequest = (short) NetworkMessageType.PUZZLE_4_PREDATOR_MOVE_REQUEST;
            Manager.client.Send(typePredMoveRequest, message);
        }
    }
}
﻿using Assets.Scripts.Dungeon_Generator_Scripts;
using Assets.Scripts.Serialization_Scripts;

namespace Assets.Scripts.Map_Scripts {
    public class GameManager : SceneManager {
        public RoomMapManager MapManager;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            MapManager.Manager = this;
            MapManager.LevelManager = GameSettings.LevelManager;
            var mapper = new SceneMapMapper(MapManager.MapPrefab);
            MapManager.Serializer = new SceneSerializer<SerializedMapData>(this, mapper);
            MapManager.GenerateMap(NetworkManager.NetworkSeed);
        }
    }
}

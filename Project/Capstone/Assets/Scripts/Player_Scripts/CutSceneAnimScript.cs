﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Player_Scripts {
	public class CutSceneAnimScript : MonoBehaviour {
		public float Speed;
		public Vector3 MoveToPosition;

		/// <summary> 
		/// Start method to initialize class variables.
		/// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
		/// </summary>

		void Update(){
			transform.localPosition = Vector3.MoveTowards (transform.localPosition, MoveToPosition, Speed * Time.deltaTime);
			transform.Rotate (Speed *0.5f* Time.deltaTime,Speed * Time.deltaTime ,0,Space.World);
		}

	
}

}

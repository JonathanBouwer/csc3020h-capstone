﻿using UnityEngine;
using Assets.Scripts.Player_Scripts;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Tutorial {
	public class PadController : MonoBehaviour {

		public TutorialManager manager;
		public int index;
	
		public void SetMaterial(Material mat){
			GetComponentInParent<Renderer> ().material = mat;
		}
		public void OnTriggerEnter(Collider other) {
			PlayerSetup player = other.gameObject.GetComponent<PlayerSetup> ();
			if (player != null) {
				bool local=player.isLocalPlayer;
				if (local) {
					manager.Step (index);
				}
			}
		}


	}

	
}

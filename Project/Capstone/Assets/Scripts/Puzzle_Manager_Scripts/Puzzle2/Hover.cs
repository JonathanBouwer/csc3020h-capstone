﻿using UnityEngine;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle2 {
    public class Hover : MonoBehaviour {
        /// <summary>
        /// Simple float up and down method
        /// </summary>
        void Update () {
            var tmp = transform.localPosition;
            tmp.y = Mathf.SmoothStep(0.45f, 0.65f, Mathf.PingPong(Time.time / 2, 1.0f));
            transform.localPosition = tmp;
        }
    }
}

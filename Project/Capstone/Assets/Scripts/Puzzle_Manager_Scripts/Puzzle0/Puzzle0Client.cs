﻿using Assets.Scripts.Serialization_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle0 {
    public class Puzzle0Client : SceneManager {
        public PuzzleBuilder Builder;

        private bool rewardingKey;
        private GameObject[] sinkingObjects;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method also build the world
        /// </remarks>
        void Start() {
            const short type2 = (short)NetworkMessageType.PUZZLE_0_DESTROY_PLATE_PAIR;
            NetworkManager.client.RegisterHandler(type2, DestroyPlate);
            const short type3 = (short)NetworkMessageType.PUZZLE_0_SOLVED;
            NetworkManager.client.RegisterHandler(type3, RewardKey);
            rewardingKey = false;

            Builder.LevelManager = GameSettings.LevelManager;
            Builder.Manager = this;
            var mapper = new ScenePuzzle0Mapper(Builder);
            Builder.Serializer = new SceneSerializer<SerializedPuzzle0Data>(this, mapper);
            Builder.BuildMap(NetworkManager.NetworkSeed);
            Builder.SetClueIgnore(NetworkManager.IsHost ? "Player1Ignore" : "Player2Ignore");
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// Manages actual puzzle play during reward phase
        /// </remarks>
        void Update() {
            if (!rewardingKey) return;
            foreach (var sinking in sinkingObjects) {
                sinking.transform.localPosition = sinking.transform.localPosition + 0.1f * Vector3.down;
            }
            if (sinkingObjects[0].transform.localPosition.y < -12) {
                rewardingKey = false;
                StartCoroutine(Builder.SaveEnvironment());
            }
        }

        /// <summary>
        /// Network message handler for destroy plate events
        /// </summary>
        /// <param name="message">Network message sent</param>
        public void DestroyPlate(NetworkMessage message) {
            var data = message.ReadMessage<StringMessage>().value;
            Destroy(GameObject.Find(data));
            var index = int.Parse(data.Substring(4));
            var decoration = Builder.RoomBuilder.Decorations[index].GetComponent<MeshRenderer>();
            decoration.material = Builder.RoomBuilder.AlternateDecorationMaterial;
        }

        /// <summary>
        /// Network message handler for puzzle solved events
        /// </summary>
        /// <param name="message">Network message sent</param>
        private void RewardKey(NetworkMessage message) {
            rewardingKey = true;
            GameHUD.AddKey(1);
            sinkingObjects = new GameObject[3];
            sinkingObjects[0] = GameObject.Find("Maze");
            sinkingObjects[1] = GameObject.Find("Clue");
            sinkingObjects[2] = GameObject.Find("FakeWalls");
        }
    }
}
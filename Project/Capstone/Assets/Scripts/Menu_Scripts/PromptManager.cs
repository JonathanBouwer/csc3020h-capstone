﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class PromptManager : MenuUIManager {
        private TextMeshProUGUI text;
        private float outlineWidth;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            text = GetComponent<TextMeshProUGUI>();
            outlineWidth = 0;
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// Pulse the prompt's outline
        /// </remarks>
        void Update() {
            outlineWidth = Mathf.SmoothStep(0.25f, 0.4f, Mathf.PingPong(Time.time / 1.3f, 1.0f));
            text.fontMaterial.SetFloat(ShaderUtilities.ID_OutlineWidth, outlineWidth);
            text.UpdateMeshPadding();
        }
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Menu_Scripts {
    public class SkyboxRotator : MonoBehaviour {
        public float Speed;
        private Vector3 change;

        /// <summary> 
        /// Start method to initialize class variables.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">Unity Documentation</see> for more info
        /// </summary>
        void Start() {
            change = new Vector3(0, 0.8f, 0.2f);
        }

        /// <summary> 
        /// Method called every frame to update the attached game objects.
        /// See <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">Unity Documentation</see> for more info
        /// </summary>
        /// <remarks>
        /// This method will spin the skybox like stars
        /// </remarks>
        void Update() {
            transform.localEulerAngles = transform.localEulerAngles + Speed * change;
        }
    }
}

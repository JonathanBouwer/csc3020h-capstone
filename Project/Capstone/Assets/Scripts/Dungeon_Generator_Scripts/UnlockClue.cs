﻿using Assets.Scripts.Puzzle_Manager_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts.Dungeon_Generator_Scripts {
    class UnlockClue : ClueController {
        private bool withinRadius;
        private NetworkManager networkManager;
        private const short Type = (short) NetworkMessageType.DO_KEY_TEST;

        public new void Start(){
            base.Start();
            withinRadius = false;
            networkManager = NetworkManager.singleton;
        }

        void Update() {
            if (!withinRadius) return;
            if (Input.GetKey(KeyCode.E)) {
                networkManager.client.Send(Type, new EmptyMessage());
            }
        }

        public new void OnTriggerEnter(Collider collider) {
            base.OnTriggerEnter(collider);
            withinRadius = true;
        }

        public new void OnTriggerExit(Collider collider) {
            base.OnTriggerExit(collider);
            withinRadius = false;
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Puzzle_Manager_Scripts.Puzzle3 {
    public class MineBuilder : MonoBehaviour {
        [HideInInspector] public GameObject Block;
        [HideInInspector] public int Width, Breadth, NumMines;
        [HideInInspector] public GameObject MineObject;
        [HideInInspector] public HashSet<int> MinesId;
        [HideInInspector] public int[,] BlocksArray;

        private int blockSize = 5;

        /// <summary>
        /// Builds grid world and sets up values for host
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator BuildHost() {
            MineObject = new GameObject("Puzzle");
            var _blocks = new GameObject("MineBlocks");
            BlocksArray = new int[Breadth, Width];
            MinesId = new HashSet<int>();
            GenerateBlockArray();
            int ids = 0;
            for (var i = 0; i < Breadth; i++) {
                for (var j = 0; j < Width; j++) {
                    GameObject block = CreateBrick(_blocks, Block, new Vector3(j * blockSize, 0.2f, i * blockSize));
                    block.GetComponent<BrickController>().Value = BlocksArray[i, j];
                    block.GetComponent<BrickController>().Id = ids;
                    block.name = "Block";
                    if (BlocksArray[i, j] == 9) {
                        MinesId.Add(ids);
                    }
                    ids++;
                }
            }


            _blocks.transform.parent = MineObject.transform;
            yield return null;
        }

        /// <summary>
        /// Builds grid world for client, does not set up values as values are sent from host as messages
        /// </summary>
        /// <remarks>
        /// This may be called from within a <see cref="https://docs.unity3d.com/ScriptReference/MonoBehaviour.StartCoroutine.html">StartCoroutine()</see> funciton call
        /// </remarks>
        public IEnumerator BuildClient() {
            MineObject = new GameObject("Puzzle");
            var _blocks = new GameObject("MineBlocks");
            int ids = 0;
            for (var i = 0; i < Breadth; i++) {
                for (var j = 0; j < Width; j++) {
                    GameObject block = CreateBrick(_blocks, Block, new Vector3(j * blockSize, 0.2f, i * blockSize));
                    block.GetComponent<BrickController>().Value = -1;
                    block.GetComponent<BrickController>().Id = ids;
                    block.name = "Block";
                    ids++;
                }
            }

            _blocks.transform.parent = MineObject.transform;
            yield return null;
        }

        /// <summary>
        /// Generates grid values for host
        /// </summary>
        public void GenerateBlockArray() {
            int countMineAssigned = 0;
            while (countMineAssigned < NumMines) {
                int b = Mathf.RoundToInt(Random.Range(0, Breadth - 1));
                int w = Mathf.RoundToInt(Random.Range(0, Width - 1));
                if (BlocksArray[b, w] != 9) {
                    countMineAssigned++;
                    BlocksArray[b, w] = 9;
                    Neighbours(b, w);
                }
            }
        }
        
        /// <summary>
        /// Helper method to instantiate bricks with position.
        /// </summary>
        /// <param name="parent">Bricks parent</param>
        /// <param name="brick">Brick prefab</param>
        /// <param name="localPosition">relative position to parent</param>
        /// <returns>Brick Created</returns>
        private GameObject CreateBrick(GameObject parent, GameObject brick, Vector3 localPosition) {
            var block = Instantiate(brick, parent.transform);
            block.transform.localPosition = localPosition;
            return block;
        }

        /// <summary> 
        /// If a position is marked as a mine, adds a value to all neighbours
        /// </summary>
        /// <param name="b">Y coordinate of mine</param>
        /// <param name="w">X Coordinate of mine</param>
        public void Neighbours(int b, int w) {
            for (int neighbourBreadth = b - 1; neighbourBreadth < b + 2; neighbourBreadth++) {
                if (neighbourBreadth >= Breadth || neighbourBreadth < 0) continue;
                for (int neighbourWidth = w - 1; neighbourWidth < w + 2; neighbourWidth++) {
                    if (neighbourWidth >= Width || neighbourWidth < 0) continue;
                    if (BlocksArray[neighbourBreadth, neighbourWidth] < 9) {
                        BlocksArray[neighbourBreadth, neighbourWidth] += 1;
                    }
                }//end width
            }//end breath
        }
    }
}




﻿using UnityEngine;
using TMPro;

public class TutorialUI : MonoBehaviour {

	public string[] TextArray;
	private int count;
	public TextMeshProUGUI TextContainer;
	public TextAsset FullText;
	public GameObject canvas;

	
	void Start(){
		TextArray = FullText.text.Split('*');
		count = 0;
		TextContainer.SetText(TextArray[count]);

	}
	public void NextButtonPushed() {
		if (count < TextArray.Length-2) {
			count++;
			TextContainer.SetText (TextArray [count]);
		} else {
			canvas.SetActive (false);
			count++;
		}
			
	}
	public void WonUI() {
		canvas.SetActive (true);
		count = TextArray.Length - 1;
		TextContainer.SetText (TextArray [count]);
	}

}

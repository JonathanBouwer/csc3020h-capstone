# Games Capstone #

This repository will act as a place to store all code relating to our games capstone.

## Sections ##

* Unity Project
* GDD
* Website

###  Unity Project ###

This will be the place where the source code lives

Please refer to Running-Instructions.md if you have any difficulties running the game.

### GDD ###

The latex document with our GDD, please start off by editing [this Google Doc](https://docs.google.com/document/d/1IXQngnJJ_DvTO6o2UiYbqMdCJ1C-AkMFXITpNh0KRHc/edit?usp=sharing)

Also edit the [pitch presentation](https://docs.google.com/presentation/d/1Nhnv9EcQxnmoOMwGGLP1PjWeKm8OkkbNO51qGKb5uBI/edit?usp=sharing)

### Website ###

The website for out project

## Git Best Practices ##

Please do your best to:

* Branch off of master when developing a new branch.
* Follow the naming pactices for branches: 
    * 'feature/' for a new feature
    * 'prototype/' for anything which is not intended to be merged
    * 'fix/' for any fixes (and almost anything else)
* When your branch is complete create a pull request and set others as reviewers.
* As a reviewer make comments on the code and a comment when you are done reiewing.
* If you are happy as a reveiwer aprove the request for the author to merged